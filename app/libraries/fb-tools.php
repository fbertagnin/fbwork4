<?php
//=====================================================================
// FBWork3 PHP Developement Framework
// Version            : 3
// Première version   : 30 mars 2006
// Dernière version   : août 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2011, est à la version 3 : FBWork3.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le
// générateur de templates VTemplate.
// FBWork3 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Outils et fonctions diverses.
//
//=====================================================================
define ('FBTOOLS_VERSION', "2.0");
define('MIN_TRACE_LEVEL', 3);



// Conversion d'une chaine de caractère pour une recherche floue
// les caractères spéciaux sont convertis en caractères normaux
// selon une convention qui facilite la recherche
function convert_string_pour_recherche($t)
{
	$ret = $t;

	$l1 = 'à á â ä ç è é ê ë ì í î ï ò ó ô ö ù ú û ü ý ÿ À Á Â Ä Ç È É Ê Ë Ì Í Î Ï Ò Ó Ô Ö Ù Ú Û Ü Ý A E I O U';
	$l2 = 'a a a a c e e e e i i i i o o o o u u u u y y a a a a c e e e e i i i i o o o o u u u u y a e i o u';
	$ret = str_replace(explode(' ', $l1), explode(' ', $l2), $ret) ;

	$ret = strtolower($ret);

	$l3 = "' \" -- - ... _ / | , . ! ? :";
	$ret = str_replace(explode(' ', $l3), ' ', $ret);


	return $ret;
}

// money_format n'est pas disponible sous windows
// http://www.developpez.net/forums/d1129277/php/langage/fonctions/call-to-undefined-function-money_format/
// http://www.php.net/manual/fr/function.money-format.php#89060
if (!function_exists('money_format'))
{
	function money_format($format, $number)
	{
		$regex  = '/%((?:[\^!\-]|\+|\(|\=.)*)([0-9]+)?'.
				  '(?:#([0-9]+))?(?:\.([0-9]+))?([in%])/';
		if (setlocale(LC_MONETARY, 0) == 'C') {
			setlocale(LC_MONETARY, '');
		}
		$locale = localeconv();
		preg_match_all($regex, $format, $matches, PREG_SET_ORDER);
		foreach ($matches as $fmatch) {
			$value = floatval($number);
			$flags = array(
				'fillchar'  => preg_match('/\=(.)/', $fmatch[1], $match) ?
							   $match[1] : ' ',
				'nogroup'   => preg_match('/\^/', $fmatch[1]) > 0,
				'usesignal' => preg_match('/\+|\(/', $fmatch[1], $match) ?
							   $match[0] : '+',
				'nosimbol'  => preg_match('/\!/', $fmatch[1]) > 0,
				'isleft'    => preg_match('/\-/', $fmatch[1]) > 0
			);
			$width      = trim($fmatch[2]) ? (int)$fmatch[2] : 0;
			$left       = trim($fmatch[3]) ? (int)$fmatch[3] : 0;
			$right      = trim($fmatch[4]) ? (int)$fmatch[4] : $locale['int_frac_digits'];
			$conversion = $fmatch[5];

			$positive = true;
			if ($value < 0) {
				$positive = false;
				$value  *= -1;
			}
			$letter = $positive ? 'p' : 'n';

			$prefix = $suffix = $cprefix = $csuffix = $signal = '';

			$signal = $positive ? $locale['positive_sign'] : $locale['negative_sign'];
			switch (true) {
				case $locale["{$letter}_sign_posn"] == 1 && $flags['usesignal'] == '+':
					$prefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 2 && $flags['usesignal'] == '+':
					$suffix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 3 && $flags['usesignal'] == '+':
					$cprefix = $signal;
					break;
				case $locale["{$letter}_sign_posn"] == 4 && $flags['usesignal'] == '+':
					$csuffix = $signal;
					break;
				case $flags['usesignal'] == '(':
				case $locale["{$letter}_sign_posn"] == 0:
					$prefix = '(';
					$suffix = ')';
					break;
			}
			if (!$flags['nosimbol']) {
				$currency = $cprefix .
							($conversion == 'i' ? $locale['int_curr_symbol'] : $locale['currency_symbol']) .
							$csuffix;
			} else {
				$currency = '';
			}
			$space  = $locale["{$letter}_sep_by_space"] ? ' ' : '';

			$value = number_format($value, $right, $locale['mon_decimal_point'],
					 $flags['nogroup'] ? '' : $locale['mon_thousands_sep']);
			$value = @explode($locale['mon_decimal_point'], $value);

			$n = strlen($prefix) + strlen($currency) + strlen($value[0]);
			if ($left > 0 && $left > $n) {
				$value[0] = str_repeat($flags['fillchar'], $left - $n) . $value[0];
			}
			$value = implode($locale['mon_decimal_point'], $value);
			if ($locale["{$letter}_cs_precedes"]) {
				$value = $prefix . $currency . $space . $value . $suffix;
			} else {
				$value = $prefix . $value . $space . $currency . $suffix;
			}
			if ($width > 0) {
				$value = str_pad($value, $width, $flags['fillchar'], $flags['isleft'] ?
						 STR_PAD_RIGHT : STR_PAD_LEFT);
			}

			$format = str_replace($fmatch[0], $value, $format);
		}
		return $format;
	}
}




function generate_image_google_maps($googlex, $googley, $file, $w=600, $h=400)
{
	// on garde comme doc, pas utilisé pour l'instant
	$id_auteur=Atomik::get('session/'.IDAPPLI.'/user/id')+0;
	$url="http://maps.google.com/staticmap";
	$url .= "?center=43.564269,1.45292";
	$url .= "&zoom=15";
	$url .= "&size=600x400";
	$url .= "&markers=43.57691664771851,1.402451992034912,bluem&key=clé";

	$filename=Atomik::get('atomik/apppath')."/tmp/".$id_auteur."mamap.gif";
	if (!$handle = fopen($url, 'r'))
	{
		echo "Impossible d'ouvrir le fichier ($filename)";
		exit;
	}


	$contents = stream_get_contents($handle);
	fclose($handle);

	$handle2=fopen($filename, 'w');
	// Ecrivons quelque chose dans notre fichier.
	if (fwrite($handle2, $contents) === FALSE)
	{
		echo "Impossible d'écrire dans le fichier ($filename)";
		exit;
	}

	echo "L'écriture de () dans le fichier ($filename) a réussi";

	fclose($handle2);

	$filename=Atomik::get('atomik/appurl')."/tmp/".$id_auteur."mamap.gif";
	print("<img src=\"$filename\" />");
}

function get_human_string_droits($droit)
{
	if ($droit == 'lec') return "Lecture";
	if ($droit == 'ecr') return "Ecriture";
	return "Sans droit";
}

function get_human_date_from_elements($jour, $mois, $annee)
{
	$j = 0; $m = 0; $a = 0;
	$j = $jour*1;
	$m = $mois*1;
	$a = $annee*1;
	if ($j > 31) $j = 0;
	if ($m > 12) $m = 0;
	if ($a > 2200) $a = 0;
	if ($a > 0 && $a < 100) $a += 2000;
	if ($j == 0 && $m == 0 && $a == 0) return "";
	if ($j == 0 && $m == 0) return "Année $a";
	if ($j == 0) return french_month_name($m) . " $a";
	return  "$j ".french_month_name($m)." $a";
}


function microtime_float()
{
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


function enregistrer_trace($tracefile, $message, $file='', $ligne='')
{
	$times = microtime_float(microtime(true));
	$timeh = date("H:i:s");
	$fp = fopen($tracefile, "a");
	if ($fp)
	{
		fwrite($fp, "t: $timeh $times |f: $file |l: $ligne |m: $message \n");
		fclose($fp);
	}
}


function encodehtml($text)
{
	// Traitement des caractères spéciaux (accents etc) mais laisse telles quelles les balises html
	if (is_array($text))
	{
		$ret = array();
		foreach($text as $k => $v)
		{
			$s = $v;
			if (is_string($s)) $s = encodestringhtml($s);
			$ret[$k] = $s;
		}
		return $ret;
	}
	else
	{
		$ret = $text;
		$ret = encodestringhtml($ret);
		return $ret;
	}
}

// Transforme un texte avec des sauts de ligne (\n) en un ensembpe
// de paragraphes html.
function nl2p ($text)
{
	$ret = "";
	$buff = explode("\n", $text);
	if (is_array($buff))
	{
		foreach ($buff as $p)
		{
			$ret .= "<p>$p</p>\n";
		}
	}
	else
	{
		$ret .= "<p>$text</p>\n";
	}
	return $ret;
}



function txt2html ($text)
{
	$infos = $text;
	$infos = encodestringhtml($infos);
	// $infos = htmlentities($infos);
	$infos = nl2p($infos);
	return $infos;
}

function encodestringhtml($text)
{
	$ret = $text;
	// $ret = utf8_encode($ret);
	// $ret = htmlentities($ret, ENT_QUOTES, 'ISO-8859-15', true);
	// $ret = htmlspecialchars($ret, ENT_QUOTES, 'ISO-8859-15', true);
	$ret = str_replace(explode(' ', 'à á â ä ç è é ê ë ì í î ï ò ó ô ö ù ú û ü ý ÿ À Á Â Ä Ç È É Ê Ë Ì Í Î Ï Ò Ó Ô Ö Ù Ú Û Ü Ý'), explode(' ', '&agrave; &aacute; &acirc; &auml; &ccedil; &egrave; &eacute; &ecirc; &euml; &igrave; &iacute; &icirc; &iuml; &ograve; &oacute; &ocirc; &ouml; &ugrave; &uacute; &ucirc; &uuml; &yacute; &yuml; &Agrave; &Aacute; &Acirc; &Auml; &Ccedil; &Egrave; &Eacute; &Ecirc; &Euml; &Igrave; &Iacute; &Icirc; &Iuml; &Ograve; &Oacute; &Ocirc; &Ouml; &Ugrave; &Uacute; &Ucirc; &Uuml; &Yacute;'), $ret) ;
	return $ret;
}

function get_enum_mysql($DB, $table, $field)
{
	// $DB est une connexion déjà établie avec Zend Framework
	$sql = "DESCRIBE $table $field ";
	$enum = $DB->fetchRow($sql);
	$enum = explode("','",substr($enum['Type'],6,strlen($enum['Type'])-8));
	return $enum;
}

function printr($text, $titre='', $chemin='', $profond=0)
{
	if ($profond==0)
	{
		echo "<pre>";
		if(is_array($text))
		{
			echo "ARRAY $titre :\n";
			printr($text, $titre, $chemin, 1);
		}
		elseif(is_object($text))
		{
			echo "OBJECT $titre :\n";
			printr($text, $titre, $chemin, 1);
		}
		else
		{
			if ($titre != '')
			{
				echo "$titre = $text\n";
			}
			else
			{
				echo "$text\n";
			}
		}
		echo "</pre>";
		// echo "<br />";
	}
	else
	{
		if(is_array($text))
		{
			foreach ($text as $k => $v)
			{
				printr($v, $titre, $chemin."[$k]",$profond+1);
			}
		}
		elseif(is_object($text))
		{
			foreach ($text as $k => $v)
			{
				printr($v, $titre, $chemin."-&gt;$k",$profond+1);
			}
		}
		else
		{
			echo "  $chemin = $text\n";
		}
	}
}

function print_debug($text, $titre='')
{
	if (Atomik::get('atomik/debug'))
	{
		printr($text, $titre);
	}
}

function print_debug2($text, $file=__FILE__, $line=__LINE__)
{
	$rep = "";
	$rep .= "[ ".date('H:i:s')." ] ";
	$rep .= print_r($text,1);
	$rep .= " [ $file line:$line ]";
	$rep .= "\n";
	echo "<pre>";
	echo $rep;
	echo "</pre>";
}


function print_trace($text, $level=3)
{
	$file = "";
	$line = "";
	$pile = array();
	$pile = debug_backtrace();
	$file = $pile[0]['file'];
	$line = $pile[0]['line'];

	$rep = "";
	if ($level <= MIN_TRACE_LEVEL)
	{
		$rep .= "[ ".date('H:i:s')." ] ";
		// $rep .= print_r($pile,1);
		$rep .= print_r($text,1);
		$rep .= " [ $file line:$line ]";
		$rep .= "\n";
	}
	$filedest = Atomik::get('atomik/apppath')."/log/trace.txt";
	$fp = fopen($filedest, "a");
	fwrite($fp, $rep);
	fclose($fp);
}

function scan_directory($Directory)
{
	$ret = array();
	if (!is_dir($Directory)) return $ret;
	$MyDirectory = opendir($Directory);
	while($Entry = @readdir($MyDirectory))
	{
		if(is_dir($Directory.'/'.$Entry)&& $Entry != '.' && $Entry != '..')
		{
			scan_directory($Directory.'/'.$Entry);
		}
		else
		{
			if ((substr($Entry, 0,4) == 'doc_') && (substr($Entry, strlen($Entry)-4, 4) == 'html'))
			{
				// printr("scan_directory $Directory $Entry");
				$nom = substr($Entry,4,strlen($Entry)-9);
				if ($nom != "") $ret[$nom] = $Entry;
			}
        }
	}
	closedir($MyDirectory);
	return $ret;
}

function convert_mysql_format_to_date ($mysqldate)
{
	$a1 = explode (" ", $mysqldate." ");
	$arr = explode ("-", $a1[0]);
	$date = trim("$arr[2]/$arr[1]/$arr[0] $a1[1]");
	return $date;
}

function convert_date_us_to_timestamp($date)
{
    $d = explode (" ", $date);
    if (!isset($d[1]))
    {
			$d[1] = "00:00:00";
	}
	$d[1] .= ":00";
    $ladate = explode("-", $d[0]);
	$lheure = explode(":", $d[1]);
	if (!isset($ladate[2])) return 0;
	if ($lheure[0] > 24) return 0;
	if ($lheure[1] > 60) return 0;
	if ($lheure[2] > 60) return 0;
	if ($ladate[2] > 31) return 0;
	if ($ladate[1] > 12) return 0;
	if ($ladate[0] < 1900) return 0;
	if ($ladate[0] > 2999) return 0;
    $rep = mktime ($lheure[0], $lheure[1], $lheure[2], $ladate[1], $ladate[2], $ladate[0]);
    return $rep;
}

function convert_date_eu_to_mysql($date)
{
	// printr($date, "convert_date_eu_to_mysql");
    $lheure = "";
    $d = explode (" ", $date);
    if (!isset($d[0])) return "";
    $ladate = explode("/", $d[0]);
    if (isset($d[1])) $lheure = " ".$d[1];
    if (!isset($ladate[2])) return "";
    $rep = "$ladate[2]-$ladate[1]-$ladate[0]".$lheure;
    return $rep;
}

function convert_date_eu_to_timestamp($date)
{
    $lheure = array();
    $lheure[0] = 0;
    $lheure[1] = 0;
    $lheure[2] = 0;
    $d = explode (" ", $date);
    $ladate = explode("/", $d[0]);
    if (isset($d[1])) $lheure = explode(":", $d[1]);
    $rep = mktime ($lheure[0], $lheure[1], $lheure[2], $ladate[1], $ladate[0], $ladate[2]);
    return $rep;
}

function convert_date_timestamp_to_eu ($date)
{
    return date("d/m/Y H:i:s", $date);
}

function number_days_in_month ($mois, $annee="")
{
  if ($mois == 1 || $mois == "1" || $mois == "01") return 31;
  if ($mois == 3 || $mois == "3" || $mois == "03") return 31;
  if ($mois == 4 || $mois == "4" || $mois == "04") return 30;
  if ($mois == 5 || $mois == "5" || $mois == "05") return 31;
  if ($mois == 6 || $mois == "6" || $mois == "06") return 30;
  if ($mois == 7 || $mois == "7" || $mois == "07") return 31;
  if ($mois == 8 || $mois == "8" || $mois == "08") return 31;
  if ($mois == 9 || $mois == "9" || $mois == "09") return 30;
  if ($mois == 10 || $mois == "10") return 31;
  if ($mois == 11 || $mois == "11") return 30;
  if ($mois == 12 || $mois == "12") return 31;
  if ($mois == 2 || $mois == "2" || $mois == "02")
  {
    $a = $annee;
    if ($a == "" || $a == 0)
    {
      $a = date("Y");
    }
    if (est_bisestile($a)) return 29;
    return 28;
  }
  return 0;
}

function is_bisestile ($annee)
{
  if (checkdate(2,29,$annee)) return true;
  return false;
}

function test_number_days_in_month ()
{
  for ($i=2000; $i<=2010; $i++)
  {
    echo "janvier $i : ".nombre_jours_dans_mois(1,$i)."<br />";
    echo "février $i : ".nombre_jours_dans_mois("02","$i")."<br />";
    echo "mars $i : ".nombre_jours_dans_mois("3",$i)."<br />";
  }
}

function is_weekend ($j, $m, $a)
{
  $dt = convert_date_eu_to_timestamp("$j/$m/$a 01:01:01");
  $js = date("w", $dt);
  if ($js == 0 || $js == 6) return true;
  return false;
}

function is_today ($j, $m, $a)
{
  $dt = convert_date_eu_to_timestamp("$j/$m/$a");
  $au = mktime(0,0,0);
  if ($dt == $au)
  {
    return true;
  }
  return false;
}

function date_heure_sans_secondes($date)
{
	$aa =explode(":", $date);
	if (count($aa == 3))
	{
		return $aa[0].":".$aa[1];
	}
	return $date;
}

function aujourdhui_french_long()
{
	$Jour = array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi","Samedi");
	$Mois = array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre");
	$datefr = $Jour[date("w")]." ".date("d")." ".$Mois[date("n")-1]." ".date("Y");
	return $datefr;
}

function day_of_week ($j, $m, $a)
{
  $dt = convert_date_eu_to_timestamp("$j/$m/$a 01:01:01");
  $js = date("w", $dt);
  // on considère lundi comme premier jour de la semaine
  if ($js == 0) return 6;
  if ($js == 1) return 0;
  if ($js == 2) return 1;
  if ($js == 3) return 2;
  if ($js == 4) return 3;
  if ($js == 5) return 4;
  if ($js == 6) return 5;
  return -1;
}

function french_day_of_week ($date)
{
  $dt = convert_date_eu_to_timestamp($date);
  $js = date("w", $dt);
  if ($js == 0) return "Dimanche";
  if ($js == 1) return "Lundi";
  if ($js == 2) return "Mardi";
  if ($js == 3) return "Mercredi";
  if ($js == 4) return "Jeudi";
  if ($js == 5) return "Vendredi";
  if ($js == 6) return "Samedi";
  return "???";
}

function french_day_of_week_short ($date)
{
  $dt = convert_date_eu_to_timestamp($date);
  $js = date("w", $dt);
  if ($js == 0) return "Dim";
  if ($js == 1) return "Lun";
  if ($js == 2) return "Mar";
  if ($js == 3) return "Mer";
  if ($js == 4) return "Jeu";
  if ($js == 5) return "Ven";
  if ($js == 6) return "Sam";
  return "???";
}

function french_month_name ($mois)
{
  $m = $mois + 0;
  if ($m == 1) return "Janvier";
  if ($m == 2) return "Février";
  if ($m == 3) return "Mars";
  if ($m == 4) return "Avril";
  if ($m == 5) return "Mai";
  if ($m == 6) return "Juin";
  if ($m == 7) return "Juillet";
  if ($m == 8) return "Août";
  if ($m == 9) return "Septembre";
  if ($m == 10) return "Octobre";
  if ($m == 11) return "Novembre";
  if ($m == 12) return "Décembre";
}


function hours_minutes ($time)
{
  $rep = $time;
  $t = explode (":", $time);
  if (count($t) >= 2)
  {
    $rep = "$t[0]:$t[1]";
  }
  return $rep;
}

function day_of_date ($date)
{
  $j = 0;
  $da = explode("/", $date);
  if (isset($da[0]))
  {
    $j = $da[0];
  }
  return $j;
}

function month_of_date ($date)
{
  $m = 0;
  $da = explode("/", $date);
  if (isset($da[1]))
  {
    $m = $da[1];
  }
  return $m;
}

function uid_string ()
{
  $c = uniqid(rand(1,999));
  return $c;
}

function uid ()
{
  $c = uniqid(rand(1,999));
  $c = hexdec($c);
  return $c;
}

function day_of_month ($date_eu)
{
  $rep = $date_eu;
  $t = explode ("/", $date_eu);
  if (count($t) >= 2)
  {
    $rep = "$t[0]/$t[1]";
  }
  return $rep;
}

function convert_array_to_string($arr, $niv=0)
{
	// fonction recoursive, parcours de tout le tableau
	$rep = "";
	$ident = "   ";
	if (is_array($arr)) {
	  foreach ($arr as $key => $value)
	  {
		if (is_array($value)) {
			$rep .= str_repeat($ident, $niv);
			$rep .= "[ $key ]\n";
			$rep .= convert_array_to_string($value, $niv+1);
		}
		else
		{
		  $rep .= str_repeat($ident, $niv);
		  $rep .= "$key = $value\n";
		}
	  }
	}
	else
	{
	  $rep .= str_repeat($ident, $niv);
	  $rep .= "[ $arr ] --> ce n'est pas un array\n";
	}
	return $rep;
}

/* Redimensionnement d'une image pour créer une vignette
 * La vignette est sauvegardée au format JPG
 * L'image est reduite et recadrée au mieux
 * */
function redimensionner_image($filein, $fileout, $width, $height)
{
	// Déterminer l'extension à partir du nom de fichier
	$extension = substr( $filein, -3 );
	// Afin de simplifier les comparaisons, on met tout en minuscule
	$extension = strtolower( $extension );

	switch ( $extension ) {

		case "jpg":
		case "jpeg": //pour le cas où l'extension est "jpeg"
			$img_src_resource = imagecreatefromjpeg( $filein);
			break;

		case "gif":
			$img_src_resource = imagecreatefromgif( $filein);
			break;

		case "png":
			$img_src_resource = imagecreatefrompng( $filein);
			break;
		default:
			$ret = "L'image n'est pas dans un format reconnu. Extensions autorisées : jpg/jpeg, gif, png";
			return $ret;
			break;
	}

	$img_src_width = imagesx( $img_src_resource );
	$img_src_height = imagesy( $img_src_resource );


	$ratio = intval($width/$height);
	$rationimg = intval($img_src_width/$img_src_height);
	if ($rationimg > $ratio)
	{
		// L'image est trop large
		// Elle doit être reduite dans le sens de la hauteur, puis recadrée
		$newh = $height;
		$neww = intval($img_src_width*($height/$img_src_height));
		$startx = intval(($neww-$width)/2);
		$starty = 0;
	}
	else
	{
		// L'image est trop haute
		// Elle doit être reduite dans le sens de la largeur, puis recadrée
		$neww = $width;
		$newh = intval($img_src_height*($width/$img_src_width));
		$startx = 0;
		$starty = intval(($newh-$height)/2);
	}
	// Création d'une image témporaire, avec une dimension qu entre dans la taille de la vignette
	// et l'autre dimension qui dépasse
	$img_tmp_resource = imagecreatetruecolor( $neww, $newh);
	imagecopyresampled($img_tmp_resource, $img_src_resource, 0, 0, 0, 0, $neww, $newh, $img_src_width, $img_src_height);



	$img_dst_resource = imagecreatetruecolor( $width, $height);
	imagecopy($img_dst_resource, $img_tmp_resource, 0, 0, $startx, $starty, $neww, $newh);

	$handle = fopen( $fileout, "w" );
	if ( !$handle ) {
		$ret = "Impossible d'écrire l'image. Vérifiez le chemin, et les droits du serveur.";
		return $ret;
	}
	fclose( $handle );
	imagejpeg( $img_dst_resource, $fileout);
	return "";
}

function get_directory($Directory, $ret=null)
{
	if (is_null($ret)) $ret = array();
	$MyDirectory = opendir($Directory);
	if (!$MyDirectory) return false;
	while($Entry = @readdir($MyDirectory))
	{
		if($Entry == '.' || $Entry == '..')
		{}
		elseif(is_dir($Directory.'/'.$Entry))
		{
			$ret[$Directory] = get_directory($Directory.'/'.$Entry);
		}
		else
		{
			$ret[$Entry] = $Entry;
		}
	}
	closedir($MyDirectory);
	return $ret;
}

function fix_magic_quotes_gpc ($var=null) {
		// Voir http://forums.ovh.net/printthread.php?threadid=7372
        // si $var n'est pas spécifié, on corrige toutes les variables superglobales
        if ( $var === null ) {
                if ( !get_magic_quotes_gpc () ) return true ; // si les magic_quotes sont activées

                $to_change = array ('_GET', '_POST', '_COOKIE') ;
                foreach ( $to_change as $var )
                {
					$GLOBALS[$var] = fix_magic_quotes_gpc ($GLOBALS[$var]) ;
				}

                return TRUE;
        }

        // si $var est un tableau, appel récursif pour corriger chaque élément
        if ( is_array ($var) ) {
                foreach ( $var as $key => $val ) $var[$key] = fix_magic_quotes_gpc ($val) ;
                return $var;
        }

        // si $var est une chaine on utilise la fonction stripslashes,
        if ( is_string($var) ) return stripslashes($var) ;

        // sinon rien
        return $var;
}

function is_mobile()
{
	// voir http://stackoverflow.com/questions/5122566/simple-smart-phone-detection
	$user_agent = $_SERVER['HTTP_USER_AGENT']; // get the user agent value - this should be cleaned to ensure no nefarious input gets executed
	$accept     = $_SERVER['HTTP_ACCEPT']; // get the content accept value - this should be cleaned to ensure no nefarious input gets executed
	return false
		|| (preg_match('/ipad/i',$user_agent))
		|| (preg_match('/ipod/i',$user_agent)||preg_match('/iphone/i',$user_agent))
		|| (preg_match('/android/i',$user_agent))
		|| (preg_match('/opera mini/i',$user_agent))
		|| (preg_match('/blackberry/i',$user_agent))
		|| (preg_match('/(pre\/|palm os|palm|hiptop|avantgo|plucker|xiino|blazer|elaine)/i',$user_agent))
		|| (preg_match('/(iris|3g_t|windows ce|opera mobi|windows ce; smartphone;|windows ce; iemobile)/i',$user_agent))
		|| (preg_match('/(mini 9.5|vx1000|lge |m800|e860|u940|ux840|compal|wireless| mobi|ahong|lg380|lgku|lgu900|lg210|lg47|lg920|lg840|lg370|sam-r|mg50|s55|g83|t66|vx400|mk99|d615|d763|el370|sl900|mp500|samu3|samu4|vx10|xda_|samu5|samu6|samu7|samu9|a615|b832|m881|s920|n210|s700|c-810|_h797|mob-x|sk16d|848b|mowser|s580|r800|471x|v120|rim8|c500foma:|160x|x160|480x|x640|t503|w839|i250|sprint|w398samr810|m5252|c7100|mt126|x225|s5330|s820|htil-g1|fly v71|s302|-x113|novarra|k610i|-three|8325rc|8352rc|sanyo|vx54|c888|nx250|n120|mtk |c5588|s710|t880|c5005|i;458x|p404i|s210|c5100|teleca|s940|c500|s590|foma|samsu|vx8|vx9|a1000|_mms|myx|a700|gu1100|bc831|e300|ems100|me701|me702m-three|sd588|s800|8325rc|ac831|mw200|brew |d88|htc\/|htc_touch|355x|m50|km100|d736|p-9521|telco|sl74|ktouch|m4u\/|me702|8325rc|kddi|phone|lg |sonyericsson|samsung|240x|x320|vx10|nokia|sony cmd|motorola|up.browser|up.link|mmp|symbian|smartphone|midp|wap|vodafone|o2|pocket|kindle|mobile|psp|treo)/i',$user_agent))
		|| ((strpos($accept,'text/vnd.wap.wml')>0)||(strpos($accept,'application/vnd.wap.xhtml+xml')>0))
		|| (isset($_SERVER['HTTP_X_WAP_PROFILE'])||isset($_SERVER['HTTP_PROFILE']))
		|| (in_array(strtolower(substr($user_agent,0,4)),array('1207'=>'1207','3gso'=>'3gso','4thp'=>'4thp','501i'=>'501i','502i'=>'502i','503i'=>'503i','504i'=>'504i','505i'=>'505i','506i'=>'506i','6310'=>'6310','6590'=>'6590','770s'=>'770s','802s'=>'802s','a wa'=>'a wa','acer'=>'acer','acs-'=>'acs-','airn'=>'airn','alav'=>'alav','asus'=>'asus','attw'=>'attw','au-m'=>'au-m','aur '=>'aur ','aus '=>'aus ','abac'=>'abac','acoo'=>'acoo','aiko'=>'aiko','alco'=>'alco','alca'=>'alca','amoi'=>'amoi','anex'=>'anex','anny'=>'anny','anyw'=>'anyw','aptu'=>'aptu','arch'=>'arch','argo'=>'argo','bell'=>'bell','bird'=>'bird','bw-n'=>'bw-n','bw-u'=>'bw-u','beck'=>'beck','benq'=>'benq','bilb'=>'bilb','blac'=>'blac','c55/'=>'c55/','cdm-'=>'cdm-','chtm'=>'chtm','capi'=>'capi','cond'=>'cond','craw'=>'craw','dall'=>'dall','dbte'=>'dbte','dc-s'=>'dc-s','dica'=>'dica','ds-d'=>'ds-d','ds12'=>'ds12','dait'=>'dait','devi'=>'devi','dmob'=>'dmob','doco'=>'doco','dopo'=>'dopo','el49'=>'el49','erk0'=>'erk0','esl8'=>'esl8','ez40'=>'ez40','ez60'=>'ez60','ez70'=>'ez70','ezos'=>'ezos','ezze'=>'ezze','elai'=>'elai','emul'=>'emul','eric'=>'eric','ezwa'=>'ezwa','fake'=>'fake','fly-'=>'fly-','fly_'=>'fly_','g-mo'=>'g-mo','g1 u'=>'g1 u','g560'=>'g560','gf-5'=>'gf-5','grun'=>'grun','gene'=>'gene','go.w'=>'go.w','good'=>'good','grad'=>'grad','hcit'=>'hcit','hd-m'=>'hd-m','hd-p'=>'hd-p','hd-t'=>'hd-t','hei-'=>'hei-','hp i'=>'hp i','hpip'=>'hpip','hs-c'=>'hs-c','htc '=>'htc ','htc-'=>'htc-','htca'=>'htca','htcg'=>'htcg','htcp'=>'htcp','htcs'=>'htcs','htct'=>'htct','htc_'=>'htc_','haie'=>'haie','hita'=>'hita','huaw'=>'huaw','hutc'=>'hutc','i-20'=>'i-20','i-go'=>'i-go','i-ma'=>'i-ma','i230'=>'i230','iac'=>'iac','iac-'=>'iac-','iac/'=>'iac/','ig01'=>'ig01','im1k'=>'im1k','inno'=>'inno','iris'=>'iris','jata'=>'jata','java'=>'java','kddi'=>'kddi','kgt'=>'kgt','kgt/'=>'kgt/','kpt '=>'kpt ','kwc-'=>'kwc-','klon'=>'klon','lexi'=>'lexi','lg g'=>'lg g','lg-a'=>'lg-a','lg-b'=>'lg-b','lg-c'=>'lg-c','lg-d'=>'lg-d','lg-f'=>'lg-f','lg-g'=>'lg-g','lg-k'=>'lg-k','lg-l'=>'lg-l','lg-m'=>'lg-m','lg-o'=>'lg-o','lg-p'=>'lg-p','lg-s'=>'lg-s','lg-t'=>'lg-t','lg-u'=>'lg-u','lg-w'=>'lg-w','lg/k'=>'lg/k','lg/l'=>'lg/l','lg/u'=>'lg/u','lg50'=>'lg50','lg54'=>'lg54','lge-'=>'lge-','lge/'=>'lge/','lynx'=>'lynx','leno'=>'leno','m1-w'=>'m1-w','m3ga'=>'m3ga','m50/'=>'m50/','maui'=>'maui','mc01'=>'mc01','mc21'=>'mc21','mcca'=>'mcca','medi'=>'medi','meri'=>'meri','mio8'=>'mio8','mioa'=>'mioa','mo01'=>'mo01','mo02'=>'mo02','mode'=>'mode','modo'=>'modo','mot '=>'mot ','mot-'=>'mot-','mt50'=>'mt50','mtp1'=>'mtp1','mtv '=>'mtv ','mate'=>'mate','maxo'=>'maxo','merc'=>'merc','mits'=>'mits','mobi'=>'mobi','motv'=>'motv','mozz'=>'mozz','n100'=>'n100','n101'=>'n101','n102'=>'n102','n202'=>'n202','n203'=>'n203','n300'=>'n300','n302'=>'n302','n500'=>'n500','n502'=>'n502','n505'=>'n505','n700'=>'n700','n701'=>'n701','n710'=>'n710','nec-'=>'nec-','nem-'=>'nem-','newg'=>'newg','neon'=>'neon','netf'=>'netf','noki'=>'noki','nzph'=>'nzph','o2 x'=>'o2 x','o2-x'=>'o2-x','opwv'=>'opwv','owg1'=>'owg1','opti'=>'opti','oran'=>'oran','p800'=>'p800','pand'=>'pand','pg-1'=>'pg-1','pg-2'=>'pg-2','pg-3'=>'pg-3','pg-6'=>'pg-6','pg-8'=>'pg-8','pg-c'=>'pg-c','pg13'=>'pg13','phil'=>'phil','pn-2'=>'pn-2','pt-g'=>'pt-g','palm'=>'palm','pana'=>'pana','pire'=>'pire','pock'=>'pock','pose'=>'pose','psio'=>'psio','qa-a'=>'qa-a','qc-2'=>'qc-2','qc-3'=>'qc-3','qc-5'=>'qc-5','qc-7'=>'qc-7','qc07'=>'qc07','qc12'=>'qc12','qc21'=>'qc21','qc32'=>'qc32','qc60'=>'qc60','qci-'=>'qci-','qwap'=>'qwap','qtek'=>'qtek','r380'=>'r380','r600'=>'r600','raks'=>'raks','rim9'=>'rim9','rove'=>'rove','s55/'=>'s55/','sage'=>'sage','sams'=>'sams','sc01'=>'sc01','sch-'=>'sch-','scp-'=>'scp-','sdk/'=>'sdk/','se47'=>'se47','sec-'=>'sec-','sec0'=>'sec0','sec1'=>'sec1','semc'=>'semc','sgh-'=>'sgh-','shar'=>'shar','sie-'=>'sie-','sk-0'=>'sk-0','sl45'=>'sl45','slid'=>'slid','smb3'=>'smb3','smt5'=>'smt5','sp01'=>'sp01','sph-'=>'sph-','spv '=>'spv ','spv-'=>'spv-','sy01'=>'sy01','samm'=>'samm','sany'=>'sany','sava'=>'sava','scoo'=>'scoo','send'=>'send','siem'=>'siem','smar'=>'smar','smit'=>'smit','soft'=>'soft','sony'=>'sony','t-mo'=>'t-mo','t218'=>'t218','t250'=>'t250','t600'=>'t600','t610'=>'t610','t618'=>'t618','tcl-'=>'tcl-','tdg-'=>'tdg-','telm'=>'telm','tim-'=>'tim-','ts70'=>'ts70','tsm-'=>'tsm-','tsm3'=>'tsm3','tsm5'=>'tsm5','tx-9'=>'tx-9','tagt'=>'tagt','talk'=>'talk','teli'=>'teli','topl'=>'topl','hiba'=>'hiba','up.b'=>'up.b','upg1'=>'upg1','utst'=>'utst','v400'=>'v400','v750'=>'v750','veri'=>'veri','vk-v'=>'vk-v','vk40'=>'vk40','vk50'=>'vk50','vk52'=>'vk52','vk53'=>'vk53','vm40'=>'vm40','vx98'=>'vx98','virg'=>'virg','vite'=>'vite','voda'=>'voda','vulc'=>'vulc','w3c '=>'w3c ','w3c-'=>'w3c-','wapj'=>'wapj','wapp'=>'wapp','wapu'=>'wapu','wapm'=>'wapm','wig '=>'wig ','wapi'=>'wapi','wapr'=>'wapr','wapv'=>'wapv','wapy'=>'wapy','wapa'=>'wapa','waps'=>'waps','wapt'=>'wapt','winc'=>'winc','winw'=>'winw','wonu'=>'wonu','x700'=>'x700','xda2'=>'xda2','xdag'=>'xdag','yas-'=>'yas-','your'=>'your','zte-'=>'zte-','zeto'=>'zeto','acs-'=>'acs-','alav'=>'alav','alca'=>'alca','amoi'=>'amoi','aste'=>'aste','audi'=>'audi','avan'=>'avan','benq'=>'benq','bird'=>'bird','blac'=>'blac','blaz'=>'blaz','brew'=>'brew','brvw'=>'brvw','bumb'=>'bumb','ccwa'=>'ccwa','cell'=>'cell','cldc'=>'cldc','cmd-'=>'cmd-','dang'=>'dang','doco'=>'doco','eml2'=>'eml2','eric'=>'eric','fetc'=>'fetc','hipt'=>'hipt','http'=>'http','ibro'=>'ibro','idea'=>'idea','ikom'=>'ikom','inno'=>'inno','ipaq'=>'ipaq','jbro'=>'jbro','jemu'=>'jemu','java'=>'java','jigs'=>'jigs','kddi'=>'kddi','keji'=>'keji','kyoc'=>'kyoc','kyok'=>'kyok','leno'=>'leno','lg-c'=>'lg-c','lg-d'=>'lg-d','lg-g'=>'lg-g','lge-'=>'lge-','libw'=>'libw','m-cr'=>'m-cr','maui'=>'maui','maxo'=>'maxo','midp'=>'midp','mits'=>'mits','mmef'=>'mmef','mobi'=>'mobi','mot-'=>'mot-','moto'=>'moto','mwbp'=>'mwbp','mywa'=>'mywa','nec-'=>'nec-','newt'=>'newt','nok6'=>'nok6','noki'=>'noki','o2im'=>'o2im','opwv'=>'opwv','palm'=>'palm','pana'=>'pana','pant'=>'pant','pdxg'=>'pdxg','phil'=>'phil','play'=>'play','pluc'=>'pluc','port'=>'port','prox'=>'prox','qtek'=>'qtek','qwap'=>'qwap','rozo'=>'rozo','sage'=>'sage','sama'=>'sama','sams'=>'sams','sany'=>'sany','sch-'=>'sch-','sec-'=>'sec-','send'=>'send','seri'=>'seri','sgh-'=>'sgh-','shar'=>'shar','sie-'=>'sie-','siem'=>'siem','smal'=>'smal','smar'=>'smar','sony'=>'sony','sph-'=>'sph-','symb'=>'symb','t-mo'=>'t-mo','teli'=>'teli','tim-'=>'tim-','tosh'=>'tosh','treo'=>'treo','tsm-'=>'tsm-','upg1'=>'upg1','upsi'=>'upsi','vk-v'=>'vk-v','voda'=>'voda','vx52'=>'vx52','vx53'=>'vx53','vx60'=>'vx60','vx61'=>'vx61','vx70'=>'vx70','vx80'=>'vx80','vx81'=>'vx81','vx83'=>'vx83','vx85'=>'vx85','wap-'=>'wap-','wapa'=>'wapa','wapi'=>'wapi','wapp'=>'wapp','wapr'=>'wapr','webc'=>'webc','whit'=>'whit','winw'=>'winw','wmlb'=>'wmlb','xda-'=>'xda-',)))
	;
}
