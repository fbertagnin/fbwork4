<?php
//=====================================================================
// FBWork3 PHP Developement Framework
// Version            : 3
// Premi�re version   : 30 mars 2006
// Derni�re version   : ao�t 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de d�veloppement PHP
// appel� FBWork, cr�� par Fabio Bertagnin comme Framework de base
// pour les d�veloppements et les outils de FBServices.
// FBWork, depuis septembre 2011, est � la version 3 : FBWork3.
// FBWork int�gre plusieurs biblioth�ques et fonctions Open Source,
// notamment le framework Atomik, le g�n�rateur de PDF TCPdf, le 
// g�n�rateur de templates VTemplate.
// FBWork3 est soumis � la licence GNU-LGPL v3, et peut donc �tre
// r�utilis� et distribu� selon les crit�res propres de cette licence.
// --------------------------------------------------------------------
//
// FBForm : Classe d'outils pour l'automatisation de la cr�ation
// et du traitement de formulaires HTML
//
//=====================================================================
class FBForms
{
	public $appurl;
	public $config;
	public $validators;
	public $erreurs;
	public $valeurs;
	public $hidefields = array();
	public $data = array();
	public function __construct($config, $appurl='@')
	{
		// l'URL de base peut �tre forc�e
		$this->config = $config;
		$this->appurl = $appurl;
		if ($appurl == '@') $this->appurl = Atomik::get('atomik/appurl');
		// printr($this->config);
		// printr($appurl, "APPURL");
	}
	
	public function set_data_list($nom, $list)
	{
		$this->data[$nom] = $list;
	}

	public function hide_field($valeur)
	{
		$this->hidefields[$valeur] = $valeur;
	}
	
	
	private function get_options_html($options)
	{
		$rep = '';
		if (is_string($options)) return $options;
		if (!is_object($options)) return $rep;
		foreach($options as $k => $v)
		{
			if (is_string($v))	$rep .= " $k=\"$v\"";
		}
		return $rep;
	}
	private function get_fieldsets($conf)
	{
		if (!is_object($conf)) 
		{
			$ret = array();
			$a = array();
			$a['html'] = '';
			$a['fieldset-id'] = '0';
			$ret[] = $a;
			return $ret;
		}
		foreach($conf as $k => $fieldset)
		{
			$a = array();
			$a['fieldset'] = $fieldset;
			$a['html'] = '';
			$a['fieldset-id'] = $k;
			$ret[] = $a;
		}
		return $ret;
	}
	// Retrouve l'index d'un fieldset selon son nom. 
	// Si aucun fieldset n'est trouv�, retourne 0;
	private function search_fieldset($fieldsets, $name)
	{
		$ret = 0;
		foreach($fieldsets as $k => $fieldset)
		{
			if ($name == $fieldset['fieldset-id']) return $k;
		}
		return $ret;
	}
	
	private function get_element_textarea_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$text = '';
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->value)) $text = $conf->value;
		if (isset($this->valeurs[$name])) $text = $this->valeurs[$name]; // prioritaire sur la conf
		if ($value != '') $text = $value; // prioritaire sur la conf et sur le process
		$ret .= '  <p>'."\n"; 
		$ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		$ret .= '    <textarea name="'.$name.'" '.$this->get_options_html($conf->options).'>'.$text.'</textarea>'."\n";
		
		$ret .= '  </p>'."\n";
		return $ret;
	}




	private function get_element_html_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$list = '';
		$text = '';
		$data = array();
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->value)) $text = $conf->value;
		if (isset($conf->data)) $list = $conf->data;
		if (isset($this->valeurs[$name])) $text = $this->valeurs[$name]; // prioritaire sur la conf
		if ($value != '') $text = $value; // prioritaire sur la conf et sur le process
		if (isset($this->data[$list])) $data = $this->data[$list];
		foreach($data as $params)
		{
			if (is_array($params))
			{
				// printr($params);
				$rech = $params['text'];
				$remp = $params['value'];
				// printr ($remp, $rech);
				$text = str_replace($rech, $remp, $text);
			}
		}


		$ret .= '  <p>'."\n"; 
		if ($label != '') $ret .= '<label class="form-label-'.$name.'">'.$label.'</label>'."\n";
		$ret .= $text;
		$ret .= '  </p>'."\n";
		return $ret;
	}

	private function get_element_input_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$text = '';
		$infos = '';
		$presaisie = '';
		$presaisieinfos = '';
		$type = 'text';
		if (isset($conf->type)) $type = $conf->type;
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->value)) $text = $conf->value;
		if (isset($conf->infos)) $infos = $conf->infos;
		if (isset($conf->presaisieinfos)) $presaisieinfos = $conf->presaisieinfos;
		if (isset($conf->presaisie)) $presaisie = $conf->presaisie;
		if (isset($this->valeurs[$name])) $text = $this->valeurs[$name]; // prioritaire sur la conf
		if ($value != '') $text = $value; // prioritaire sur la conf et sur le process

		$text = str_replace('"', '&quot;', $text);
		// printr($text, "TEXT $name");

		$class = '';
		if (isset($conf->class)) $class = $conf->class;
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  <p class="'.$class.'" >'."\n"; 
		}
		if ($label != '') $ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		$ret .= '    <input type="'.$type.'" id="input-'.$name.'" name="'.$name.'" '.$this->get_options_html($conf->options).' value="'.$text.'" />'."\n";
		if ($infos != '')
		{
			$ret .= '<a href="#" class="bulleaide avec-tooltip" title="'.$infos.'"></a>';
		}
		if ($presaisie != '') $ret .= $this->get_presaisie_code($presaisie, $name, $presaisieinfos);
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  </p>'."\n";
		}
		//printr(htmlentities($ret));
		return $ret;
	}
	
	private function get_presaisie_code($presaisie, $name, $infos)
	{
		$ret = "";
		$tab = explode(",", $presaisie);
		if (count($tab) == 0) return $ret;
		$ret .= '<a href="#" id="presaisie-a-'.$name.'" class="presaisie-fb-form-activate avec-tooltip" title="'.$infos.'"><span>&nbsp;</span></a>';
		$ret .= "<div id=\"presaisie-list-$name\" style=\"display:none\">";
		$ret .= "<ul>";
		foreach($tab as $t)
		{
			$ret .= "<li>";
			$ret .= "<a class=\"set-presaisie-fb-form-$name\" href=\"#\">";
			$ret .= $t;
			$ret .= "</a>";
			$ret .= "</li>";
		}
		$ret .= "</ul>";
		$ret .= "<a class=\"presaisie-list-fermer\" href=\"#\">fermer</a>";
		$ret .= "
			<script type=\"text/javascript\" charset=\"utf-8\">
			$('a.set-presaisie-fb-form-$name').click(function() {
				// alert($(this).html());
				$('input#input-$name').attr('value',$(this).html());
				$('div#presaisie-list-$name').css('display', 'none');
				// alert($(this).html());
				return false;
			});
			$('a#presaisie-a-$name').click(function() {
				pos = $(this).position();
				$('div#presaisie-list-$name').css('left', pos.left+10);
				$('div#presaisie-list-$name').css('top', pos.top+10);
				$('div#presaisie-list-$name').css('display', 'block');
				$('a.presaisie-a-$name').css('display', 'none');
			});
			$('a.presaisie-list-fermer').click(function() {
				$('div#presaisie-list-$name').css('display', 'none');
				$('a.presaisie-a-$name').css('display', 'block');
			});
			</script>
			<style>
				div#presaisie-list-$name {
					position : absolute;
					background : #F3FDCE;
					border : solid 1px #000;
					border-right : solid 4px #000;
					border-bottom : solid 4px #000;
					width : 200px;
					z-index : 100;
				}
				a.presaisie-list-fermer {
					padding-left : 100px;
				}
			</style>
		";
		$ret .= "</div>";
		return $ret;
	}
	
	private function get_element_file_html($conf, $name)
	{
		$ret = '';
		$label = '';
		$type = 'text';
		if (isset($conf->type)) $type = $conf->type;
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		$ret .= '  <p>'."\n"; 
		$ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		$ret .= '    <input type="'.$type.'" name="'.$name.'" '.$this->get_options_html($conf->options).' />'."\n";
		
		$ret .= '  </p>'."\n";
		return $ret;
	}

	private function get_fieldset_html($fieldset)
	{
		$rep = '';
		if (isset($fieldset['fieldset'])) $rep .= '<fieldset '.$this->get_options_html($fieldset['fieldset']->options).' >'."\n";
		if (isset($fieldset['fieldset']->name) && $fieldset['fieldset']->name != '') $rep .= '<legend>'.$fieldset['fieldset']->name.'</legend>'."\n";
		$rep .= $fieldset['html'];
		if (isset($fieldset['fieldset'])) $rep .= '</fieldset>'."\n";
		return $rep;
	}

	private function get_element_datesep_html($conf, $name, $value='')
	{
		//printr($conf, "CONF");
		// Date en trois champs texte (jour, mois, annee)
		$ret = '';
		$label = 'Date';
		$val_jour = '';
		$val_mois = '';
		$val_annee = '';
		$infos = '';
		$name_jour = 'jour';
		$name_mois = 'mois';
		$name_annee = 'annee';
		if (isset($conf->name_jour)) $name_jour = $conf->name_jour;
		if (isset($conf->name_mois)) $name_mois = $conf->name_mois;
		if (isset($conf->name_annee)) $name_annee = $conf->name_annee;
		if (isset($conf->label)) $label = $conf->label;

		if (isset($conf->value_jour)) $val_jour = $conf->value_jour;
		if (isset($conf->value_mois)) $val_mois = $conf->value_mois;
		if (isset($conf->value_annee)) $val_annee = $conf->value_annee;
		if (isset($conf->infos)) $infos = $conf->infos;
		if (isset($this->valeurs[$name_jour])) $val_jour = $this->valeurs[$name_jour]; // prioritaire sur la conf
		if (isset($this->valeurs[$name_mois])) $val_mois = $this->valeurs[$name_mois]; // prioritaire sur la conf
		if (isset($this->valeurs[$name_annee])) $val_annee = $this->valeurs[$name_annee]; // prioritaire sur la conf


		$class = '';
		if (isset($conf->class)) $class = $conf->class;
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  <p class="'.$class.'">'."\n"; 
		}
		if ($label != '') $ret .= '    <label class="form-label-'.$name_jour.'" for="'.$name_jour.'">'.$label.'</label>'."\n";
		$ret .= ' <input type="text" size="3" name="'.$name_jour.'" '.$this->get_options_html($conf->options).' value="'.$val_jour.'" />'."\n";
		$ret .= ' <input type="text" size="3" name="'.$name_mois.'" '.$this->get_options_html($conf->options).' value="'.$val_mois.'" />'."\n";
		$ret .= ' <input type="text" size="6" name="'.$name_annee.'" '.$this->get_options_html($conf->options).' value="'.$val_annee.'" />'."\n";
		if ($infos != '')
		{
			$ret .= '<a href="#" class="bulleaide" title="'.$infos.'"></a>';
		}
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  </p>'."\n";
		}
		//printr(htmlentities($ret));
		return $ret;
		
	}

	private function get_element_selectlist_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$infos = '';
		$select = '';
		$list = '';
		$data = array();
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->data)) $list = $conf->data;
		if (isset($conf->infos)) $infos = $conf->infos;
		if (isset($conf->value)) $select = $conf->value;
		if ($value != '') $select = $value; // prioritaire sur la conf
		if (isset($this->valeurs[$name])) 
		{
			$select = $this->valeurs[$name]; // prioritaire sur la conf
		}

		$class = '';
		if (isset($conf->class)) $class = $conf->class;
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  <p class="'.$class.'">'."\n"; 
		}
		if ($label != '') $ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		if (isset($this->data[$list])) $data = $this->data[$list];
		$ret .= '<select name="'.$name.'" '.$this->get_options_html($conf->options).'>'."\n";
		foreach($data as $opts)
		{
			// printr($opts, "OPTS $name $select");
			if (is_array($opts))
			{
				$opt = $opts['text'];
				$optval = $opts['value'];
			}
			else
			{
				$opt = $opts;
				$optval = $opts;
			}
			$selected = '';
			if ($select == $optval) $selected = ' selected="selected"';
			$ret .= "<option$selected value=\"$optval\">$opt</option>\n";
		}
		$ret .= "</select>\n";
		if ($infos != '')
		{
			$ret .= '<a href="#" class="bulleaide avec-tooltip" title="'.$infos.'"></a>';
		}
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  </p>'."\n";
		}
		// printr(htmlentities($ret));
		return $ret;
	}

	private function get_element_radiolist_html($conf, $name, $value='')
	{
		// printr($conf, "ONF");
		$ret = '';
		$label = '';
		$select = '';
		$list = '';
		$data = array();
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->data)) $list = $conf->data;
		if (isset($conf->value)) $select = $conf->value;
		if ($value != '') $select = $value; // prioritaire sur la conf
		if (isset($this->valeurs[$name])) 
		{
			$select = $this->valeurs[$name]; // prioritaire sur la conf
		}

		$class = '';
		if (isset($conf->class)) $class = $conf->class;
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  <p class="'.$class.'">'."\n"; 
		}
		if ($label != '') $ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		if (isset($this->data[$list])) $data = $this->data[$list];
		foreach($data as $opts)
		{
			// printr($opts, "OPTS $name $select");
			if (is_array($opts))
			{
				$opt = $opts['text'];
				$optval = $opts['value'];
			}
			else
			{
				$opt = $opts;
				$optval = $opts;
			}
			// printr("opt=$opt optval=$optval select=$select");
			$checked = '';
			if ($select == $optval) $checked = ' checked="checked"';
			$ret .= "<input type=\"radio\" name=\"$name\" $checked value=\"$optval\" ".$this->get_options_html($conf->options).">";
			$ret .= "<span class=\"info-bouton-radio\">$opt</span>\n";
		}
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  </p>'."\n";
		}
		// printr(htmlentities($ret));
		return $ret;
	}

	private function get_element_select_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$text = '';
		$type = 'text';
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->value)) $text = $conf->value;
		if ($value != '') $text = $value; // prioritaire sur la conf
		$ret .= '  <p>'."\n"; 
		$ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		$ret .= "<p>SELECT TODO !</p>";
		
		$ret .= '  </p>'."\n";
		return $ret;
	}
	
	private function get_element_checkbox_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$text = '';
		$infos = '';
		$notes = '';
		$type = 'checkbox';
		if (isset($conf->type)) $type = $conf->type;
		if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->value)) $text = $conf->value;
		if (isset($conf->infos)) $infos = $conf->infos;
		if (isset($conf->notes)) $notes = $conf->notes;
		if (isset($this->valeurs[$name])) $text = $this->valeurs[$name]; // prioritaire sur la conf
		if ($value != '') $text = $value; // prioritaire sur la conf et sur le process
		$text = strtolower($text);
		if ($text == 'oui' || $text == 'on' || $text == '1')
		{
			$checked='checked';
		}
		else
		{
			$checked = '';
		}
		// printr($conf, "CONF");
		$class = '';
		if (isset($conf->class)) $class = $conf->class;
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  <p class="'.$class.'">'."\n"; 
		}
		if ($label != '') $ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		$ret .= '    <input type="'.$type.'" name="'.$name.'" '.$this->get_options_html($conf->options).' '.$checked.' value="oui" />'."\n";
		if ($infos != '')
		{
			$ret .= '<a href="#" class="bulleaide" title="'.$infos.'"></a>';
		}		
		if ($notes != '')
		{
			$ret .= "<br class=\"clear\" /><br /><span style=\"display:block;\">$notes</span>";
		}		
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  </p>'."\n";
		}
		return $ret;
	}

	private function get_element_radio_html($conf, $name, $value='')
	{
		$ret = '';
		$label = '';
		$text = '';
		$type = 'radio';
		$selected = '';
		if (isset($conf->type)) $type = $conf->type;
		// if (isset($conf->name)) $name = $conf->name;
		if (isset($conf->label)) $label = $conf->label;
		if (isset($conf->value)) $text = $conf->value;
		if (isset($this->valeurs[$name])) $text = $this->valeurs[$name]; // prioritaire sur la conf
		if ($value != '') $text = $value; // prioritaire 
		$text = strtolower($text);
		if ($text == 'oui' || $text == 'on' || $text == '1')
		{
			$selected='checked="checked"';
		}
		$class = "";
		if (isset($conf->class)) $class = $conf->class;
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  <p class="'.$class.'">'."\n"; 
		}
		if ($label != '') $ret .= '    <label class="form-label-'.$name.'" for="'.$name.'">'.$label.'</label>'."\n";
		$ret .= '    <input type="'.$type.'" name="'.$conf->name.'" '.$this->get_options_html($conf->options).' '.$selected.' />'."\n";
		
		if (!isset($conf->opt) || $conf->opt != 'nocont')
		{
			$ret .= '  </p>'."\n";
		}
		// printr($conf, $name);
		// printr(htmlentities($ret));
		return $ret;
	}


	
	// Rend le code HTML du formulaire.
	// Le param�tre $f_affiche_erreurs � true permet d'afficher les 
	// �ventuels messages d'erreurs associ�s au formulaire (voir seterreurs).
	// Note : dans un syst�me controleur/view les erreurs doivent �tre
	// transf�r�s au formulaire par une variable de session.
	public function getform($f_affiche_erreurs=true)
	{
		$rep = "";

		$rep .= '<div ';
		if (isset($this->config->options)) $rep .= $this->get_options_html($this->config->options);
		$rep .= ">\n";
		$rep .= '<form ';
		if (isset($this->config->form->options)) $rep .= $this->get_options_html($this->config->form->options)."\n";
		$rep .= '      action="'.$this->appurl.'/'.$this->config->form->action.'" method="'.$this->config->form->method.'">'."\n";
		// il faut construire les fieldsets selon le fichier de configuration.
		// Si aucun fieldset n'est configur�, on en prend un par d�faut.
		$fieldsets = $this->get_fieldsets($this->config->fieldsets);
		foreach($this->config->elements as $k => $element)
		{
			// printr($this->hidefields);
			if (!isset($this->hidefields[$k]))
			{
				// printr($element, $k);
				$fds = $this->search_fieldset($fieldsets, $element->fieldset);
				$fieldsets[$fds]['html'] .= '';
				if ($element->type == "date-sep" || $element->type == "DATE-SEP")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_datesep_html($element, $k);
				}
				if ($element->type == "select-list" || $element->type == "SELECT-LIST")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_selectlist_html($element, $k);
				}
				if ($element->type == "radio-list" || $element->type == "RADIO-LIST")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_radiolist_html($element, $k);
				}
				if ($element->type == "textarea" || $element->type == "TEXTAREA")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_textarea_html($element, $k);
				}
				if ($element->type == "input" || $element->type == "INPUT" || $element->type == "date" || $element->type == "DATE")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_input_html($element, $k);
				}
				if ($element->type == "text" || $element->type == "text") // synonime de input
				{
					$fieldsets[$fds]['html'] .= $this->get_element_input_html($element, $k);
				}
				if ($element->type == "file" || $element->type == "FILE")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_file_html($element, $k);
				}
				if ($element->type == "password" || $element->type == "PASSWORD")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_input_html($element, $k);
				}
				if ($element->type == "radio" || $element->type == "RADIO")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_radio_html($element, $k);
				}
				if ($element->type == "checkbox" || $element->type == "CHECKBOX")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_checkbox_html($element, $k);
				}
				if ($element->type == "submit" || $element->type == "SUBMIT")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_input_html($element, $k);
				}
				if ($element->type == "hidden" || $element->type == "HIDDEN")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_input_html($element, $k);
				}
				if ($element->type == "select" || $element->type == "SELECT")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_select_html($element, $k);
				}
				if ($element->type == "html" || $element->type == "HTML")
				{
					$fieldsets[$fds]['html'] .= $this->get_element_html_html($element, $k);
				}
			}
		}
		//printr($fieldsets);
		foreach($fieldsets as $fieldset)
		{
			$rep .= $this->get_fieldset_html($fieldset);
		}
		$rep .= "</form>\n";
		$rep .= "</div>\n";
		return $rep;
		return ("<pre style='text-align:left'>".htmlentities($rep)."</pre>");

	}

	// Charge un tableau avec une liste d'erreurs � afficher avec
	// le formulaire.
	// normalement c'est utilis� apr�s une v�rification
	// pour reafficher le formulaire pour la saisie des valeurs corrig�es.
	// Ceci pour afficher les erreurs automatiquement si le param�tre
	// correspondant de la fonction getform est activ�.
	public function seterreurs($erreurs)
	{
		$this->erreurs = $erreurs;
	}

	// Charge un tableau avec le valeurs pr�s�l�ctionn�es du formulaire.
	// Normalement c'est utilis� apr�s des erreurs de saisie pour demander
	// les corrections sans faire resaisir enti�rement le formulaire
	public function setvaleurs($valeurs)
	{
		$this->valeurs = $valeurs;
	}

	// Charge dans un tableau interne la configuration des validateurs
	private function setvalidators($data)
	{
		if (isset($data->elements))
		{
			foreach($data->elements as $k1 => $element)
			{
				$name = $k1;
				if (isset($element->name) && $element->name != '') $name = $element->name;
				if (isset($element->validator))
				{
					$this->validators[$name] = array();
					foreach($element->validator as $id => $validator)
					{
						$this->validators[$name][$id] = $validator;
					}
				}
			}
		}
		if (isset($data->fieldset))
		{
			foreach($data->fieldset as $k => $fieldset)
			{
				if (isset($fieldset->elements))
				{
					foreach($fieldset->elements as $k1 => $element)
					{
						$name = $k1;
						if (isset($element->name) && $element->name != '') $name = $element->name;
						if (isset($element->validator))
						{
							$this->validators[$name] = array();
							foreach($element->validator as $id => $validator)
							{
								$this->validators[$name][$id] = $validator;
							}
						}
					}
				}
			}
		}
	}
	
	// V�rification de la validit� des donn�es post�es ($data) 
	// Utilisation de la librairie php-form-validator
	// Voir : http://www.html-form-guide.com/php-form/php-form-validation.html
	// La fonction rend un tableau avec la liste des champs en erreur
	public function verifreponse($data)
	{
		$dirlib = realpath(dirname(__FILE__));
		require_once("$dirlib/php-form-validator/formvalidator.php");
		// On charge la liste des validations programm�es dans la configuration du formulaire
		$this->setvalidators($this->config);
		
		// printr($this->config);
		// printr($this->validators, "VALIDATORS");
		// printr($data, "_POST");

		$VALID = new FormValidator();
		foreach($this->validators as $field => $validators)
		{
			foreach($validators as $validator)
			{
				$VALID->addValidation($field,$validator, "");
			}
		}

		if($VALID->ValidateForm($data))
		{
			//Validation success. 
			return;
		}
		else
		{
			//Validation error. 
			$rep = array();
			$error_hash = $VALID->GetErrors();
			foreach($error_hash as $inpname => $inp_err)
			{
				$rep[$inpname] = $inp_err;
			}        
			return $rep;
		}
	}
}

