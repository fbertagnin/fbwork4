<?php
/**
 * Fichier de test pour la classe multipage
 * Le code ci-dessous teste le d�coupage de r�sultats en mode alphab�tique
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Fev 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode alphab�tique</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Nous r�cup�rons ici les variables provenant du formulaire
if (!IsSet($_GET['Template']) || empty($_GET['Template'])) $_GET['Template'] = 'tpl1.htm';
if (!IsSet($_GET['alphaEncaps'])) $_GET['alphaEncaps'] = 1;

// Chargement du fichier contenant la classe
include_once('../../multipage.class.php');

// Param�trage de la classe
$params = Array(
    'dbType'      => 'mysql:native',
    'delta'       => 7,                             // Nous voulons 7 liens dans notre multipage (pr�f�rez tjrs une valeur impaire pour un affichage correcte)
    'alphaColumn' => 'domaine',                     // Nous allons travailler en mode alphanum�rique en nous basant sur la colonne 'domaine'
    'alphaEncaps' => (int) $_GET['alphaEncaps'],    // Ceci va nous permettre de d�finir l'intervalle du tableau alpha sur lequel nous allons "filtrer" nos �l�ments
    'template'    => $_GET['Template'],             // On r�cup�re le template avec lequel on souhaite afficher notre multipage
    'query'       => 'SELECT domaine, description FROM domaines ORDER BY domaine, description'
);

// Nous instancions maintenant un objet $Pager en lui passant les param�tres d�finit dans le tableau $params
$Pager = Multipage::Load('DB', $params);

// Nous stockons maintenant notre multipage dans la variable $MonMultipage pour un affichage ult�rieur
$MonMultipage = $Pager-> GetMultipage();
$out          = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}

// Pour regrouper les lettres dans notre formulaire
$options = '';

for ($i=1; $i<27; $i++) {
    $options .= '<option value="'. $i .'">' . $i;
}
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h4 style="font-family:Arial;">Multipage en mode alphab�tique</h4></td>
    <td valign="top" colspan="2">&nbsp;<select name="Template" style="font-size:11px;"><option></option><option value="tpl1.htm">Template n�1<option value="tpl2.htm">Template n�2<option value="tpl3.htm">Template n�3<option value="tpl4.htm">Template n�4</select></td>
  </tr>
  <tr>
    <td style="font-family:Arial;font-size:11px;" colspan="3">Regroupement <select name="alphaEncaps" style="font-size:11px;"><?php echo $options; ?></select> <input type="submit" value="ok" style="font-size:11px;"></td>
  </tr>
</table>
</form>
<?php
// Nous affichons le multipage
echo $MonMultipage
?>
<hr>


<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php
  // Nous affichons nos r�sultats
  print $out;
  ?>
</table>
</body>
</html>