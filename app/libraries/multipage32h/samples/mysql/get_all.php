<?php
/**
 * Fichier de test pour la classe multipage
 * Teste la cr�ation d'un pager sans passer par les templates
 * La m�thode GetAll() est appel�e pour r�cup�rer les propri�t�s du multipage et en g�rer le r�sultat manuellement.
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Fev 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    .current{color:#000000;font-size:12px;text-decoration:underline;font-weight:bold;}
    //-->
	</style>
</head>

<body>
<?php
// Fichier de test de la classe multipage
include_once('../../multipage.class.php');

// Param�trage de la classe
$params = Array(
    'dbType'          => 'mysql:native',
    'perPage'         => 10,                                            // Nous souhaitons 10 r�sultats par page
    'delta'           => 10,                                            // Avec un multipage comprenant 10 liens max
    'template'        => '',                                            // Nous ne sp�cifions pas de template pour ne pas lancer les traitements relatifs au parsing des templates (vu que nous allons pour une fois nous en passer)
    'nextPage'        => '<img src="img/img_next.gif" border="0">',     // Nous red�finissons la propri�t� next_page pour lui associer une image
    'previousPage'    => '<img src="img/img_previous.gif" border="0">', // M�me chose mais cette fois pour la derni�re page
    'query'           => 'SELECT domaine, description FROM domaines ORDER BY domaine, description',
    'curPageSpanPre'  => '<span class="current">',                      // Nous allons �galement red�finir les balises encapsulant la page courante pour y associer notre propre style "current"
    'curPageSpanPost' => '</span>'
);

// Nous instancions notre objet en lui passant le tableau des propri�t�s pr�c�demment d�fini
$Pager = MultiPage::Load('DB', $params);

// Comme nous n'utilisons de template, il nous faut pouvoir rappatrier nos diff�rents �l�ments composant un multipage
// La m�thode getAll() est l� pour �a. Elle nous retourne un tableau � multiple dimension o� chaque index correspond �
// une propri�t� du multipage. Ce tableau est ici stock� sous le nom de variable $result.
$result = $Pager-> getAll();

$out = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}
?>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h4 style="font-family:Arial;">Multipage en mode arbitraire : utilisation de la m�thode GetAll()</h4></td>
  </tr>
</table>

<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="80"></td>
    <td><?php
    // Pour afficher le lien vers la page pr�c�dente, il nous suffit d'afficherl'index 'PREVIOUS_PAGE' de notre variable $result 
    print $result['PREVIOUS_PAGE']; ?></td>
    <td width="10">&nbsp;</td>
    <td><img src="img/g_oogle.gif"></td>
    <td background="img/o_repeat.gif">&nbsp;</td>
    <td><img src="img/goo_gle.gif"></td>
    <td>&nbsp;</td>
    <td width="10">&nbsp;</td>
    <td><?php
    // Il en va de m�me pour la page suivante
    print $result['NEXT_PAGE']; ?></td>
  </tr>
  <tr>
    <td colspan="4"><font size=-1>Page de r�sultat</font></td>
    <td>
    <?php
    // Concernant les liens vers les autres pages, il faut r�cuperer l'index $result['PAGE'] qui est lui m�me un tableau
    // Nous parcourons ce tableau facilement gr�ce � la boucle "foreach" et affichons les liens vers les autres pages
    foreach ($result['PAGE'] as $href) {
        print $href;
    }
    ?>
    </td>
  </tr>
</table>
<hr>
<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
<?php
// Les 3 lignes ci-dessous permettent de voir toutes les propri�t�s r�cup�r�es dans la variable $result.
print '<xmp>';
print_r($result);
print '</xmp>';
?>
</body>
</html>
