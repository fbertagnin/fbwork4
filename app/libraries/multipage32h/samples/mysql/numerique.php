<?php
/**
 * Fichier de test pour la classe multipage
 * Teste une connexion MySql native en utilisant le mode alphab�tique et en red�finissant le tableau de tri
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Fev 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode alphab�tique</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Nous r�cup�rons ici les variables provenant du formulaire
if (!IsSet($_GET['Template']) || empty($_GET['Template'])) $_GET['Template'] = 'tpl1.htm';
if (!IsSet($_GET['alphaEncaps'])) $_GET['alphaEncaps'] = 1;

// Fichier de test de la classe multipage
include_once('../../multipage.class.php');

$params = Array(
    'dbType'      => 'mysql:native',
    'delta'       => 5,                                   // Nombre de liens du multipage
    'aAlpha'      => Array(1, 2, 3, 4, 5, 6, 7, 8, 9),    // On red�finit le tableau aAlpha en pr�cisant les nouvelles valeurs sur lesquelles on souhaite "d�couper" les r�sultats.
    'alphaColumn' => 'id',                                // Pour le tri num�rique nous allons utiliser la colonne id
    'alphaEncaps' => (int) $_GET['alphaEncaps'],          // Lorsque l'on r�cup�re une donn�e en provenance d'une Url, elle est typ�e en string. Or la classe multipage veut un entier pour ce param�tre, nous transtypons la variable en entier.
    'template'    => $_GET['Template'],
    'query'       => 'SELECT id, domaine, description FROM domaines ORDER BY id'
);

// D�claration d'un objet multipage $pager
$Pager = Multipage::Load('DB', $params);

$MonMultipage = $Pager-> GetMultipage();
$out          = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['id'] . ' </td><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}

// Valeur dans notre formulaire pour cr�er la liste de regroupement
$options = '';

for ($i=1; $i<27; $i++) {
    $options .= '<option value="'. $i .'">' . $i;
}
?>
<form action="numerique.php" method="GET">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h4 style="font-family:Arial;">Multipage en mode num�rique (red�finition du tableau alphab�tique)</h4></td>
    <td valign="top" colspan="2">&nbsp;<select name="Template" style="font-size:11px;"><option></option><option value="tpl1.htm">Template n�1<option value="tpl2.htm">Template n�2<option value="tpl3.htm">Template n�3</select></td>
  </tr>
  <tr>
    <td style="font-family:Arial;font-size:11px;" colspan="3">Regroupement <select name="alphaEncaps" style="font-size:11px;"><?php echo $options; ?></select> <input type="submit" value="ok" style="font-size:11px;"></td>
  </tr>
</table>
</form>
<?php echo $MonMultipage?>
<hr>


<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Id</strong></td>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
