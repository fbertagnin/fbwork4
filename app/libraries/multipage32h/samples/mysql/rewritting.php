<?php
/**
 * Fichier de test pour la classe multipage
 * Teste les fonctions d'url Rewritting en mode arbitraire
 * Pr�requis : avoir impl�mant� la base 'multipagetest', 
 *             avoir configur� le fichier config.class.php,
 *             avoir activ� le mode url rewritting d'apache
 *             avoir renomm� le fichier htaccess.txt avec le nom accept� par votre serveur
 *             avoir configur� l'acc�s http � cette page dans la variable TemplateUri
 * Le fichier htaccess.txt contient les r�gles pour cette page
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.1b 14 Jan 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire - Url Rewritting</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// R�cup�ration du fichier template pass� en GET
if (!IsSet($_GET['Template'])) $_GET['Template'] = 'tpl1.htm';

// Chargement du fichier contenant la classe
include_once('../../multipage.class.php');

// Param�trage de la classe
$params = Array(
    'dbType'   => 'mysql:native',
    'perPage'  => 10,
    'delta'    => 5,
    'template' => $_GET['Template'],
    'tplUri'   => 'http://' . $_SERVER['SERVER_NAME'] . '/' . $_SERVER['SCRIPT_NAME'] . '/rewritting/'. $_GET['Template'] .'/<#PAGE>.htm',
    'query'    => 'SELECT domaine, description FROM domaines ORDER BY domaine, description'
);


// D�claration d'un objet multipage $pager
$Pager = Multipage::Load('DB', $params);
$MonMultipage = $Pager-> GetMultipage();

$out = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}
?>
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h4 style="font-family:Arial;">Multipage en mode arbitraire - Url Rewritting</h4></td>
  </tr>
</table>
<?php echo $MonMultipage; ?>
<hr>


<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td align="center"><strong>Domaine</strong></td>
    <td align="center"><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
