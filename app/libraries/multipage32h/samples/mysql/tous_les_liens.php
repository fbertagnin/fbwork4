<?php
/**
 * Fichier de test pour la classe multipage
 * Teste une connexion MySql native en mode arbitraire, o� tous les liens sont pr�sents sur la m�me page
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Fev 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire (tous les r�sultats)</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// R�cuperation des donn�es du formulaire
if (!IsSet($_GET['Template'])) $_GET['Template'] = 'tpl1.htm';

// Chargement du fichier contenant la classe
include_once('../../multipage.class.php');

$params = Array(
    'dbType'   => 'mysql:native',
    'perPage'  => 10,                  // Nous souhaitons limiter "arbitrairement" nos r�sultats � 10 par page
    'delta'    => 0,                   // En positionnement notre propri�t� delta � 0, nous sp�cifions notre volont� d'avoir tous les liens du multipage en un bloc
    'template' => $_GET['Template'],   // On passe le template en param�tre
    'query'    => 'SELECT domaine, description FROM domaines ORDER BY domaine, description'
);

// Nous instancions maintenant un objet $Pager en lui passant les param�tres d�finit dans le tableau $params
$Pager = MultiPage::Load('DB', $params);

// Nous stockons maintenant notre multipage dans la variable $MonMultipage pour un affichage ult�rieur
$MonMultipage = $Pager-> GetMultipage();
$out          = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= "<tr><td>" . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}
?>
<form action="arbitraire_all.php" method="GET">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h4 style="font-family:Arial;">Multipage en mode arbitraire</h4></td>
    <td valign="top">&nbsp;<select onChange="window.document.forms[0].submit()" name="Template" style="font-size:11px;"><option><option value="tpl1.htm">Template n�1<option value="tpl2.htm">Template n�2<option value="tpl3.htm">Template n�3<option value="tpl4.htm">Template n�4</select></td>
  </tr>
</table>
</form>
<?php echo $MonMultipage?>
<hr>


<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
