<?php
/**
 * Fichier de test pour la classe multipage
 * Teste une connexion MySql via ADOdb en mode arbitraire
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Fev 2004
 */

// Chargement de la classe ADOdb
include_once('D:\Apache2\www\classes\adodb\adodb 3.60\adodb.inc.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Fichier de test de la classe multipage
include_once('../../multipage.class.php');

$params = Array(
    'dbType'     => 'mysql:adodb',
    'dbHostname' => 'localhost',
    'dbUser'     => 'root',
    'dbPassword' => '',
    'dbName'     => 'multipagetest',
    'perPage'    => 15,
    'delta'      => 5,
    'template'   => 'tpl4.htm',
    'query'      => 'SELECT domaine, description FROM domaines ORDER BY domaine, description',
);

$Pager = Multipage::Load('DB', $params);
$MonMultipage = $Pager-> GetMultipage();
$out = '';

while (list($row, $field) = each($Pager-> data)) {
    $out .= "<tr><td>" . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}

echo $MonMultipage
?>
<hr>
<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
