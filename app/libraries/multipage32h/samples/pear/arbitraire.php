<?php
/**
 * Fichier de test pour la classe multipage
 * 
 * Teste une connexion MySql via PEAR::DB
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.1b 14 Jan 2004
 */

// Chargement de la classe PEAR::DB
include('DB.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Fichier de test de la classe multipage
include_once('../../multipage.class.php');

$params = Array(
    'dbType'     => 'mysql:pear',
    'dbHostname' => 'localhost',
    'dbUser'     => 'root',
    'dbPassword' => '',
    'dbName'     => 'multipagetest',
    'perPage'    => 15,
    'delta'      => 5,
    'template'   => 'tpl4.htm',
    'query'      => 'SELECT domaine, description FROM domaines ORDER BY domaine, description'
);

$Pager = Multipage::Load('DB', $params);

$MonMultipage = $Pager-> GetMultipage();
$out = '';

while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field[0] . ' </td><td>' . $field[1] . '</td></tr>';
}

echo $MonMultipage
?>
<hr>
<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
