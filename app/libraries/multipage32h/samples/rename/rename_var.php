<?php
/**
 * Fichier de test pour la classe multipage
 * Teste le renommage des variables de templates � la vol�e
 *
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Fev 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    .current{color:#000000;font-size:12px;text-decoration:underline;font-weight:bold;}
    //-->
	</style>
</head>

<body>
<?php
// Fichier de test de la classe multipage
include_once('../../multipage.class.php');

$new_name = Array(
    'PAGE'          => 'PAGE_TMP',
    'COLSPAN'       => 'COLSPAN_TMP',
    'NBRECORD'      => 'NBRECORD_TMP',
    'NEXT_PAGE'     => 'NEXT_PAGE_TMP',
    'PREVIOUS_PAGE' => 'PREVIOUS_PAGE_TMP',
    'FIRST_PAGE'    => 'FIRST_PAGE_TMP',
    'LAST_PAGE'     => 'LAST_PAGE_TMP',
    'CURRENT_PAGE'  => 'CURRENT_PAGE_TMP',
    'TOTAL_PAGE'    => 'TOTAL_PAGE_TMP',
    'LIMIT'         => 'LIMIT_TMP',
    'FROM'          => 'FROM_TMP',
    'TO'            => 'TO_TMP'
);


// Param�trage de la classe
$params = Array(
    'dbType'     => 'mysql:native',
    'query'      => 'SELECT domaine, description FROM domaines ORDER BY domaine, description',
    'tplVarName' => $new_name,
    'tplDir'     => dirname(__FILE__) . '\\',
    'template'   => 'tpl_spec.htm'
);

// Nous instancions notre objet en lui passant le tableau des propri�t�s pr�c�demment d�fini
$Pager = MultiPage::Load('DB', $params);
$Pager-> pMultipage();

// Comme nous n'utilisons de template, il nous faut pouvoir rappatrier nos diff�rents �l�ments composant un multipage
// La m�thode getAll() est l� pour �a. Elle nous retourne un tableau � multiple dimension o� chaque index correspond �
// une propri�t� du multipage. Ce tableau est ici stock� sous le nom de variable $result.
$out = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}
?>
<hr>
<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
<hr>
<?php $Pager-> pMultipage(); ?>
</body>
</html>
