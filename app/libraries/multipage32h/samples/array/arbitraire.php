<?php
/**
 * Fichier de test pour la classe multipage
 * Teste le d�coupage d'un tableau en mode arbitraire
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 04 Feb 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Nous r�cup�rons la variable du formulaire permettant de sp�cifier avec quel template on souhaite travailler
if (!IsSet($_GET['Template'])) $_GET['Template'] = 'tpl1.htm';

// Chargement du fichier contenant la classe
include_once('../../multipage.class.php');

// La propri�t� colspan est importante avec le template "tpl1.htm".
// Editez ce fichier pour comprendre le fonctionnement de l'attribut addToColspan.
if ($_GET['Template'] == 'tpl1.htm') $addToColspan = 3; else $addToColspan = 0;

// Construction simpliste du tableau
$array_test = Array();

for ($i = 1; $i < 100; $i++) {
    $array_test[] = 'valeur de $i : ' . $i;
}

$params = Array(
    'perPage'      => 10,                  // Nous souhaitons 10 r�sultats par page
    'delta'        => 5,                   // Avec 5 liens dans notre multipage
    'template'     => $_GET['Template'],   // Permet de d�finir avec quel template on souhaite travailler
    'addToColspan' => $addToColspan,       // Ajoute x nombre de colonne pour la fusion html
    'recordSet'    => $array_test          // Tableau � traiter
);

// Nous instancions un objet $Pager en mode 'Array' en lui passant les param�tres d�finis dans le tableau $params
$Pager = MultiPage::Load('Array', $params);

// Nous stockons maintenant notre multipage dans la variable $MonMultipage pour un affichage ult�rieur
$MonMultipage = $Pager-> GetMultipage();
$out          = '';

// Cr�ation dans la variable $out d'un affichage simple de nos donn�es
while (list($index, $element) = each($Pager-> data)) {
    $out .= "<tr><td>" . $index . ' </td><td>' . $element . '</td></tr>';
}
?>
<form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="GET">
<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h4 style="font-family:Arial;">Multipage en mode arbitraire</h4></td>
    <td valign="top">&nbsp;<select onChange="window.document.forms[0].submit()" name="Template" style="font-size:11px;"><option><option value="tpl1.htm">Template n�1<option value="tpl2.htm">Template n�2<option value="tpl3.htm">Template n�3<option value="tpl4.htm">Template n�4</select></td>
  </tr>
</table>
</form>
<?php echo $MonMultipage?>
<hr>


<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
