<?php
//=========================================================================
// Connexion � la base de donn�es
//=========================================================================
require_once 'DB.php';
$db = DB::connect('mysql://root@localhost/multipagetest');
//$db->setFetchMode(DB_FETCHMODE_ASSOC);

//=========================================================================
// D�claration de la requete de recup�ration des donn�es
//=========================================================================
$sql = 'SELECT domaine, description FROM domaines ORDER BY domaine, description';

//=========================================================================
// Construction du multipage
//=========================================================================
require_once '../../multipage.class.php';
$params = array(
	
	// Param�tres relatifs aux donn�es
	'handle'		=> $db,
	'dbType'		=> 'handle:pear',
    'query'		    => 'SELECT domaine, description FROM domaines ORDER BY domaine, description',
    'setFetchMode'	=> 'ASSOC',

	// Param�tres relatifs au pager
    'perPage'			=> 15,
    'delta'       		=> 5,
	'varUrl'			=> 'page',

	// Param�tres relatifs au template
    'tplVarBegin'		=> '{$',
    'tplVarEnd'			=> '}',
    'tplVarLoopBegin'	=> '<loop>',
    'tplVarLoopEnd'		=> '</loop>',
	'tplDir'			=> dirname(__FILE__).'/templates/',
	'template'			=> 'test02.html',

	// Red�finition des noms de variables du template
	'tplVarName'		=> array(
        
	    'CURRENT_PAGE'		=> 'PageCurrent',
	    'TOTAL_PAGE'		=> 'PageCount',
	    'NBRECORD'			=> 'RecordCount',
	    'LIMIT'				=> 'RecordLimit',
	    'FROM'				=> 'RecordFirst',
	    'TO'				=> 'RecordLast',
	    'FIRST_PAGE'		=> 'LinkFirst',
	    'PREVIOUS_PAGE'		=> 'LinkPrev',
	    'PAGE'				=> 'LinkPage',
	    'NEXT_PAGE'			=> 'LinkNext',
	    'LAST_PAGE'			=> 'LinkLast',
        
	   )
);
$pager			=& Multipage::load('DB', $params);
$multipage		= $pager->getMultipage();
$data			= $pager->data;

$pagerVars		= $pager->getAll();
$pagerResult	= $pagerVars['RawPages'];

unset($pagerVars['RawPages'], $pagerVars['LinkPage'], $pagerVars['COLSPAN']);


$pager = $pagerVars;

?>
<html>
<style type="text/css">
body {
	font-family:verdana, arial, sans-serif;
	font-size:10pt;
}
div#pager a {
	color:#0000FF;
}
div#pager a:hover {
	color:#CC0000;
	text-decoration:none;
}
</style>
</head>
<body>

<!-- CONSTRUCTION DU MULTIPAGE -->
<div id="pager">
Page <?= $pager['PageCurrent'] ?> sur <?= $pager['PageCount'] ?> (enregistrements <?= $pager['RecordFirst'] ?> - <?= $pager['RecordLast'] ?> sur <?= $pager['RecordCount'] ?>)<br />
Pages : <?= $pager['LinkFirst'] ?> 
<?= $pager['LinkPrev'] ?> 
<? foreach ( $pagerResult as $page ) : ?>
<a<? if ( $page['PageIsCurrent'] ) : ?> style="color:#000000;font-weight:bold;text-decoration:none;"<? endif; ?> href="<?= $page['PageUri'] ?>"><?= $page['PageLabel'] ?></a>
<? endforeach; ?>
<?= $pager['LinkNext'] ?> 
<?= $pager['LinkLast'] ?>
</div>

</body>
</html>
<?php
echo "<xmp>";
print_r($pagerVars);
echo "</xmp>";

echo "<xmp>";
print_r($pagerResult);
echo "</xmp>";
?>