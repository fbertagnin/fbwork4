<?php
//=========================================================================
// Connexion � la base de donn�es
//=========================================================================
require_once 'DB.php';
$db = DB::connect('mysql://root@localhost/multipagetest');
//$db->setFetchMode(DB_FETCHMODE_ASSOC);

//=========================================================================
// D�claration de la requete de recup�ration des donn�es
//=========================================================================
$sql = 'SELECT domaine, description FROM domaines ORDER BY domaine, description';

//=========================================================================
// Construction du multipage
//=========================================================================
require_once '../../multipage.class.php';

$new_name = Array(
    'CURRENT_PAGE'		=> 'PageCurrent',
    'TOTAL_PAGE'		=> 'PageCount',
    'NBRECORD'			=> 'RecordNumber',
    'LIMIT'				=> 'RecordLimit',
    'FROM'				=> 'RecordFirst',
    'TO'				=> 'RecordLast',
    'FIRST_PAGE'		=> 'LinkFirst',
    'PREVIOUS_PAGE'		=> 'LinkPrev',
    'PAGE'				=> 'LinkPage',
    'NEXT_PAGE'			=> 'LinkNext',
    'LAST_PAGE'			=> 'LinkLast',
);

$params = array(
	
	// Param�tres relatifs aux donn�es
	'handle'			=> $db,
	'dbType'			=> 'handle:pear',
    'query'				=> 'SELECT domaine, description FROM domaines ORDER BY domaine, description',
    'setFetchMode'	=> 'ASSOC',

	// Param�tres relatifs au pager
    'perPage'			=> 15,
    'delta'       		=> 5,
	'varUrl'			=> 'page',

	// Param�tres relatifs au template
    'tplVarBegin'		=> '{$',
    'tplVarEnd'			=> '}',
    'tplVarLoopBegin'	=> '<loop>',
    'tplVarLoopEnd'		=> '</loop>',
	'tplDir'			=> dirname(__FILE__).'/templates/',
	'template'			=> 'test02.html',

	// Red�finition des noms de variables du template
    'tplVarName'      => $new_name
);

$pager			=& Multipage::load('DB', $params);
$multipage		= $pager->getMultipage();
$result			= $pager->data;

echo $multipage;
?>