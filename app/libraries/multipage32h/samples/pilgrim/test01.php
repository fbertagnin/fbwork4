<?php
//=========================================================================
// Connexion � la base de donn�es
//=========================================================================
require_once 'DB.php';
$db = DB::connect('mysql://root@localhost/multipagetest');

//=========================================================================
// Construction et affichage du multipage
//=========================================================================
require_once '../../multipage.class.php';
$params = array(

	// Configuration propre � la page
	'handle'			=> $db,
	'dbType'			=> 'handle:pear',
    'query'				=> 'SELECT domaine, description FROM domaines ORDER BY domaine, description',

	// Configuration globale commune
    'perPage'			=> 15,
    'delta'       		=> 5,
    'delta_min'			=> 3,			// A integrer dans la classe
	'varUrl'			=> 'page',
	'separator'			=> '&nbsp;',
	'curPageSpanPre'	=> '<strong>[',
	'curPageSpanPost'   => ']</strong>',
    'firstPage'		    => '[�]',
    'previous_page'	    => '&lt;',
	'nextPage'		    => '&gt;',
    'lastPage'			=> '[�]',
);
$pager		= &Multipage::load('DB', $params);

// R�cup�ration des informations brutes du multipage
$multipage	= $pager->getAll();

// Extraction des variables du tableau
extract($multipage);

// Limitation du delta au debut et � la fin du multipage (exp�rimental - � revoir)
$PAGE		= multipage_limit_delta($PAGE, $params['delta_min'], $params['delta'], $CURRENT_PAGE, $TOTAL_PAGE);

// R�cup�ration de la plage des numeros de page dont les liens sont affich�s (experimental - a revoir)
$PageRange	= multipage_get_range($PAGE, '/(?:\>|\[)([0-9]+)(?:\<|\])/');

// R�cup�ration du tableau des enregistrements limit� par le multipage
$result		= $pager->data;
?>
<html>
<style type="text/css">
body, table {
	font-family:verdana;
	font-size:10pt;
}
a {
	color:#0000FF;
}
a:hover {
	color:#CC0000;
	text-decoration:none;
}
div#pager {
	margin-bottom:10px;
}
</style>
<body>

<!-- CONSTRUCTION DU MULTIPAGE -->
<div id="pager">

<!-- Affichage du titre et du nombre total de page -->
Pages&nbsp;: (<?php echo $TOTAL_PAGE; ?>) 

<!-- Affichage conditionnel du lien sur la premiere page -->
<?php if ( !in_array(1, $PageRange) ) : ?><?php echo $FIRST_PAGE; ?> ... <?php endif; ?>

<!-- Affichage des liens sur les numeros de page -->
<?php foreach ( $PAGE as $link ) : ?><?php echo $link; ?><?php endforeach; ?>

<!-- Affichage conditionnel du lien sur la derniere page -->
<?php if ( !in_array($TOTAL_PAGE, $PageRange) ) : ?> ... <?php echo $LAST_PAGE; ?><?php endif; ?>

</div>

<!-- AFFICHAGE DES ENREGISTREMENTS -->
<table border="1">
<tr>
	<th>Domaine</th>
	<th>Description</th>
</tr>
<?php foreach ( $pager->data as $row ) : ?>
<tr>
	<td><?php echo $row['domaine']; ?></td>
	<td><?php echo $row['description']; ?></td>
</tr>
<?php endforeach; ?>
</table>

</body>
</html>
<?php
/**
 * Retourne la liste des num�ros de page affich�es
 *
 * @param	array $pages		Tableau contenant les liens des pages
 * @param	string $pattern		Expression rationnelle de recherche du num�ro de la page dans le lien
 * @return	array				Retourne un tableau contenant les num�ros de page affich�es dans le multipage
 */
function multipage_get_range( $pages, $pattern ) {

	$range = array();
	foreach ( $pages as $page ) {
		preg_match($pattern, $page, $match);
		$range[] = $match[1];
	}
	return $range;

} // end of 'multipage_get_range()'

/**
 * Reduit le nombre des elements d'un tableau suivant une plage de valeur
 *
 * @param	array $array			Tableau de valeur � reduire
 * @param	integer $deltaMin		Nombre de liens minimum souhait� dans le multipage
 * @param	integer $deltaMax		Nombre de liens maximum souhait� dans le multipage
 * @param	integer $pageCurrent	Numero de la page courante
 * @param	integer $pageCount		Nombre total de pages
 * @return	array					Retourne le tableau modifi�
 */
 
function multipage_limit_delta( $array, $deltaMin, $deltaMax, $pageCurrent, $pageCount ) {
	
	// D�but de la plage de liens
	if ( $deltaMin + $pageCurrent - 1 < $deltaMax ) {
		$delta = $deltaMin + $pageCurrent - 1;
		$count = $deltaMax - $delta;
		array_pop_multi($array, $count);
	}

	// Fin de la plage de lien
	elseif ( $pageCount - $pageCurrent + $deltaMin < $deltaMax ) {
		$delta = $deltaMin + $pageCount - $pageCurrent;
		$count = $deltaMax - $delta;
		array_shift_multi($array, $count);
	}

	return $array;
	
} // end of 'multipage_limit_delta()'

/**
 * Supprime des �l�ments au d�but d'un tableau
 *
 * @param	array $array		Tableau � reduire par le d�but
 * @param	integer $count		Nombre d'�l�ments � supprimer
 * @return	array				Retourne un tableau contenant les �l�ments supprim�s
 */
function array_shift_multi( &$array, $count ) {

	$result = array();
	for ( $i = 0; $i < $count; $i++ ) $result[] = array_shift($array);
	return $result;

} // end of 'array_shift_multi()'

/**
 * Supprime des �l�ments � la fin d'un tableau
 *
 * @param	array $array		Tableau � reduire par la fin
 * @param	integer $count		Nombre d'�l�ments � supprimer
 * @return	array				Retourne un tableau contenant les �l�ments supprim�s
 */
function array_pop_multi( &$array, $count ) {

	$result = array();
	for ( $i = 0; $i < $count; $i++ ) $result[] = array_pop($array);
	return array_reverse($result);

} // end of 'array_pop_multi()'

?>