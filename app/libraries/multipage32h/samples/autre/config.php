<?php
/**
 * Fichier de test pour la classe multipage
 * Le code ci-dessous teste le fichier de config. Aucun param�tre n'est g�r� ici, tous le sont dans le 
 * fichier multipage.config.class
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.1b 14 Jan 2004
 */

// Chargement de la classe PEAR::DB
include('DB.php');
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode alphab�tique</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Chargement du fichier contenant la classe
include_once('../../multipage.class.php');

// Param�trage de la classe
$params = Array(
    'query'       => 'SELECT domaine, description FROM domaines ORDER BY domaine, description'
);

// Nous instancions maintenant un objet $Pager en lui passant les param�tres d�finit dans le tableau $params
$Pager = Multipage::Load('DB', $params);

// Nous stockons maintenant notre multipage dans la variable $MonMultipage pour un affichage ult�rieur
$MonMultipage = $Pager-> GetMultipage();
$out          = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}

// Nous affichons le multipage
echo $MonMultipage;
?>
<hr>


<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php
  // Nous affichons nos r�sultats
  print $out;
  ?>
</table>
</body>
</html>