<?php
/**
 * Fichier de test pour la classe multipage
 * Teste le codage des libell�s avec les entit�s HTLM
 * Pr�requis : avoir impl�mant� la base 'multipagetest', avoir configur� le fichier config.class.php
 * @package    multipage
 * @author     <o.veujoz@miasmatik.net>
 * @version 3.2 01 Feb 2004
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html>
<head>
	<title>Multipage::Mode arbitraire</title>
    <style><!--
    * {font-family : arial;font-size:11px;}
    .i{color:#a90a08;}
    a,a:link{color:#008000;}
    //-->
	</style>
</head>

<body>
<?php
// Nous r�cup�rons la variable du formulaire permettant de sp�cifier avec quel template on souhaite travailler
if (!IsSet($_GET['Template'])) $_GET['Template'] = 'tpl1.htm';

// Chargement du fichier contenant la classe
include_once('../../multipage.class.php');

// La propri�t� colspan est importante avec le template "tpl1.htm".
// Editez ce fichier pour comprendre le fonctionnement de l'attribut addToColspan.
if ($_GET['Template'] == 'tpl1.htm') $addToColspan = 3; else $addToColspan = 0;

$params = Array(
    'dbType'         => 'mysql:native',
    'toHtmlEntities' => TRUE,
    'query'          => 'SELECT domaine, description FROM domaines ORDER BY domaine, description'
);

// Nous instancions maintenant un objet $Pager en lui passant les param�tres d�finit dans le tableau $params
$Pager = Multipage::Load('DB', $params);

// Nous stockons maintenant notre multipage dans la variable $MonMultipage pour un affichage ult�rieur
$MonMultipage = $Pager-> GetMultipage();
$out          = '';

// On se cr�e dans la variable $out un affichage rapide de nos donn�es
while (list($row, $field) = each($Pager-> data)) {
    $out .= '<tr><td>' . $field['domaine'] . ' </td><td>' . $field['description'] . '</td></tr>';
}

echo $MonMultipage;
?>
<hr>
<table border="1" cellspacing="5" cellpadding="0">
  <tr>
    <td><strong>Domaine</strong></td>
    <td><strong>Description</strong></td>
  </tr>
  <?php print $out; ?>
</table>
</body>
</html>
