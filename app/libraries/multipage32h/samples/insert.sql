DROP TABLE IF EXISTS domaines;
CREATE TABLE domaines (
  id integer NOT NULL auto_increment,
  domaine char(20) NOT NULL default '',
  description char(50) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

#
# Contenu de la table `domaines`
#

INSERT INTO domaines (domaine, description) VALUES ('ac', 'Ascension (ile de l\')'),
('ad', 'Andorre'),
('ae', 'Emirats  Arabes Unis'),
('af', 'Afghanistan'),
('ag', 'Antigua et Barbuda'),
('ai', 'Anguilla'),
('al', 'Albanie'),
('am', 'Arm�nie'),
('an', 'Antilles Neerlandaises'),
('ao', 'Angola'),
('aq', 'Antarctique'),
('ar', 'Argentine'),
('as', 'American Samoa'),
('au', 'Australie'),
('aw', 'Aruba'),
('az', 'Azerbaijan'),
('ba', 'Bosnie Herzegovine'),
('bb', 'Barbade'),
('bd', 'Bangladesh'),
('be', 'Belgique'),
('bf', 'Burkina Faso'),
('bg', 'Bulgarie'),
('bh', 'Bahrain'),
('bi', 'Burundi'),
('bj', 'Benin'),
('bm', 'Bermudes'),
('bn', 'Brunei Darussalam'),
('bo', 'Bolivie'),
('br', 'Br�sil'),
('bs', 'Bahamas'),
('bt', 'Bhoutan'),
('bv', 'Bouvet (ile)'),
('bw', 'Botswana'),
('by', 'Bi�lorussie'),
('bz', 'B�lize'),
('ca', 'Canada'),
('cc', 'Cocos (Keeling) iles'),
('cd', 'Congo, (R�publique d�mocratique du)'),
('cf', 'Centrafricaine (R�publique )'),
('cg', 'Congo'),
('ch', 'Suisse'),
('ci', 'Cote d\'Ivoire'),
('ck', 'Cook (iles)'),
('cl', 'Chili'),
('cm', 'Cameroun'),
('cn', 'Chine'),
('co', 'Colombie'),
('cr', 'Costa Rica'),
('cu', 'Cuba'),
('cv', 'Cap Vert'),
('cx', 'Christmas (ile)'),
('cy', 'Chypre'),
('cz', 'Tch�que (R�publique)'),
('de', 'Allemagne'),
('dj', 'Djibouti'),
('dk', 'Danemark'),
('dm', 'Dominique'),
('do', 'Dominicaine (r�publique)'),
('dz', 'Alg�rie'),
('ec', 'Equateur'),
('ee', 'Estonie'),
('eg', 'Egypte'),
('eh', 'Sahara Occidental'),
('er', 'Erythr�e'),
('es', 'Espagne'),
('et', 'Ethiopie'),
('fi', 'Finlande'),
('fj', 'Fiji'),
('fk', 'Falkland (Malouines) iles'),
('fm', 'Micron�sie'),
('fo', 'Faroe (iles)'),
('fr', 'France'),
('ga', 'Gabon'),
('gd', 'Grenade'),
('ge', 'G�orgie'),
('gf', 'Guyane Fran�aise'),
('gg', 'Guernsey'),
('gh', 'Ghana'),
('gi', 'Gibraltar'),
('gl', 'Groenland'),
('gm', 'Gambie'),
('gn', 'Guin�e'),
('gp', 'Guadeloupe'),
('gq', 'Guin�e Equatoriale'),
('gr', 'Gr�ce'),
('gs', 'Georgie du sud et iles Sandwich du sud'),
('gt', 'Guatemala'),
('gu', 'Guam'),
('gw', 'Guin�e-Bissau'),
('gy', 'Guyana'),
('hk', 'Hong Kong'),
('hm', 'Heard et McDonald (iles)'),
('hn', 'Honduras'),
('hr', 'Croatie'),
('ht', 'Haiti'),
('hu', 'Hongrie'),
('id', 'Indon�sie'),
('ie', 'Irlande'),
('il', 'Isra�l'),
('im', 'Ile de Man'),
('in', 'Inde'),
('io', 'Territoire Britannique de l\'Oc�an Indien'),
('iq', 'Iraq'),
('ir', 'Iran (R�publique Islamique d\')'),
('is', 'Islande'),
('it', 'Italie'),
('je', 'Jersey'),
('jm', 'Jama�que'),
('jo', 'Jordanie'),
('jp', 'Japon'),
('ke', 'Kenya'),
('kg', 'Kirgizstan'),
('kh', 'Cambodge'),
('ki', 'Kiribati'),
('km', 'Comores'),
('kn', 'Saint Kitts et Nevis'),
('kp', 'Cor�e du nord'),
('kr', 'Cor�e du sud'),
('kw', 'Kowe�t'),
('ky', 'Ca�manes (iles)'),
('kz', 'Kazakhstan'),
('la', 'Laos'),
('lb', 'Liban'),
('lc', 'Sainte Lucie'),
('li', 'Liechtenstein'),
('lk', 'Sri Lanka'),
('lr', 'Liberia'),
('ls', 'Lesotho'),
('lt', 'Lituanie'),
('lu', 'Luxembourg'),
('lv', 'Latvia'),
('ly', 'Libyan Arab Jamahiriya'),
('ma', 'Maroc'),
('mc', 'Monaco'),
('md', 'Moldavie'),
('mg', 'Madagascar'),
('mh', 'Marshall (iles)'),
('mk', 'Mac�doine'),
('ml', 'Mali'),
('mm', 'Myanmar'),
('mn', 'Mongolie'),
('mo', 'Macao'),
('mp', 'Mariannes du nord (iles)'),
('mq', 'Martinique'),
('mr', 'Mauritanie'),
('ms', 'Montserrat'),
('mt', 'Malte'),
('mu', 'Maurice (ile)'),
('mv', 'Maldives'),
('mw', 'Malawi'),
('mx', 'Mexique'),
('my', 'Malaisie'),
('mz', 'Mozambique'),
('na', 'Namibie'),
('nc', 'Nouvelle Cal�donie'),
('ne', 'Niger'),
('nf', 'Norfolk (ile)'),
('ng', 'Nig�ria'),
('ni', 'Nicaragua'),
('nl', 'Pays Bas'),
('no', 'Norv�ge'),
('np', 'N�pal'),
('nr', 'Nauru'),
('nu', 'Niue'),
('nz', 'Nouvelle Z�lande'),
('om', 'Oman'),
('pa', 'Panama'),
('pe', 'P�rou'),
('pf', 'Polyn�sie Fran�aise'),
('pg', 'Papouasie Nouvelle Guin�e'),
('ph', 'Philippines'),
('pk', 'Pakistan'),
('pl', 'Pologne'),
('pm', 'St. Pierre et Miquelon'),
('pn', 'Pitcairn (ile)'),
('pr', 'Porto Rico'),
('pt', 'Portugal'),
('pw', 'Palau'),
('py', 'Paraguay'),
('qa', 'Qatar'),
('re', 'R�union (ile de la)'),
('ro', 'Roumanie'),
('ru', 'Russie'),
('rw', 'Rwanda'),
('sa', 'Arabie Saoudite'),
('sb', 'Salomon (iles)'),
('sc', 'Seychelles'),
('sd', 'Soudan'),
('se', 'Su�de'),
('sg', 'Singapour'),
('sh', 'St. H�l�ne'),
('si', 'Slov�nie'),
('sj', 'Svalbard et Jan Mayen (iles)'),
('sk', 'Slovaquie'),
('sl', 'Sierra Leone'),
('sm', 'Saint Marin'),
('sn', 'S�n�gal'),
('so', 'Somalie'),
('sr', 'Suriname'),
('st', 'Sao Tome et Principe'),
('sv', 'Salvador'),
('sy', 'Syrie'),
('sz', 'Swaziland'),
('tc', 'Turks et Ca�ques (iles)'),
('td', 'Tchad'),
('tf', 'Territoires Fran�ais du sud'),
('tg', 'Togo'),
('th', 'Thailande'),
('tj', 'Tajikistan'),
('tk', 'Tokelau'),
('tm', 'Turkm�nistan'),
('tn', 'Tunisie'),
('to', 'Tonga'),
('tp', 'Timor Oriental'),
('tr', 'Turquie'),
('tt', 'Trinidad et Tobago'),
('tv', 'Tuvalu'),
('tw', 'Taiwan'),
('tz', 'Tanzanie'),
('ua', 'Ukraine'),
('ug', 'Ouganda'),
('uk', 'Royaume Uni'),
('gb', 'Royaume Uni'),
('um', 'US Minor Outlying (iles)'),
('us', 'Etats Unis'),
('uy', 'Uruguay'),
('uz', 'Ouzb�kistan'),
('va', 'Vatican'),
('vc', 'Saint Vincent et les Grenadines'),
('ve', 'Venezuela'),
('vg', 'Vierges Britaniques (iles)'),
('vi', 'Vierges USA (iles)'),
('vn', 'Vi�t Nam'),
('vu', 'Vanuatu'),
('wf', 'Wallis et Futuna (iles)'),
('ws', 'Western Samoa'),
('ye', 'Yemen'),
('yt', 'Mayotte'),
('yu', 'Yugoslavie'),
('za', 'Afrique du Sud'),
('zm', 'Zambie'),
('zr', 'Za�re'),
('zw', 'Zimbabwe'),
('com', '-'),
('net', '-'),
('org', '-'),
('edu', '-'),
('int', '-'),
('arpa', '-'),
('at', 'Autriche'),
('gov', 'Gouvernement'),
('mil', 'Miltaire'),
('su', 'Ex U.R.S.S.'),
('reverse', '-');