<?php
class multipage_array extends Multipage {
    
    /**
     * Tableau des donn�es � traiter
     * 
     * Valeur par d�faut : tableau vide
     * @var string $query
     * @see createSQL(), sql2array()
     */
	var $recordSet;
    
    function Multipage_Array($options) {
        $this-> recordSet = '';
        parent::Multipage($options);
    }
    
    
    /**
     * Retourne le nombre d'enregistrement contenu dans le tableau des donn�es
     * 
     * @access private
     * @since 3.2
     */
    function getNbRecord() {
        return count($this-> recordSet);
    } // end func getNbRecord
    
    
    
    /**
     * Retourne le tableau des donn�es "d�coup�" 
     * Attention : m�thode enti�rement revue. N�cessite PHP5 et la librairie SPL
     * 
     * @access private
     * @return array
     * @since 3.2
     */
    function getRecords() {
        
        $aTmp = Array();
        $aoRecordSet = new ArrayObject($this-> recordSet);
        $aoIterator  = $aoRecordSet-> getIterator();
        
        // On se positione � la premi�re ligne
        $aoIterator-> seek($this-> firstline);
        
        for ($i = $this-> firstline; $i < ($this-> firstline + $this-> perPage); $i++) {
            
            $aTmp[] = $aoIterator-> current();
            $aoIterator-> next();
            
            // V�rifie s'il existe un autre enregistrement
            if (!$aoIterator-> valid())
                break;
        }
        
        unset($aoIterator);
        unset($aoRecordSet);
        return $aTmp;
        
    } // end func getRecords
    
    
    
    /**
     * Initialisation de la classe mode tableau
     * 
     * @access private
     * @return void
     * @since 3.2
      */
    function init() {
        if (!is_array($this-> recordSet)) trigger_error('Propri�t� <b>recordSet</b> mal configur�e <br>', E_USER_ERROR);
    }
    
    
    
    /** 
     * Termine l'appel � la classe
     * 
     * @access public/private
     * @return void
     * @since 3.2
      */
    function close() {
        unset($this-> recordSet);
        return true;
    }
}
?>