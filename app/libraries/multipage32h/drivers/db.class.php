<?php
define('MULTIPAGE_ADODB_PATH', NULL);
define('MULTIPAGE_DB_TYPE'   , 'MySql');

class multipage_db extends Multipage {
    
    /**
     * La requ�te SQL de s�lection des donn�es sans la clause LIMIT
     * 
     * Valeur par d�faut : chaine vide
     * @var string $query
     * @see createSQL(), sql2array()
     */
	var $query;
    
    
    
    /**
     * Drivers bdd (mysql:native || mysql:adodb || driver:adodb)
     * 
     * Permet de sp�cifier l'utilisation de la classe ADOdb ou le mode en natif de mysql
     * @access public
     * @var string $dbType
     * @since 3.1b
     */
    var $dbType;
    
    
    
    /**
     * @IP ou nom d'h�te de la bdd
     * 
     * Permet de sp�cifier l'utilisation de la classe ADOdb ou le mode en natif de mysql
     * @access public
     * @var string $dbHostname
     * @since 3.1b
     */
    var $dbHostname;
    
    
    
    /**
     * Login d'un utilisateur ayant acc�s � la bdd
     * 
     * @access public
     * @var string $dbUser
     */
    var $dbUser;
    
    
    
    /**
     * Mot de passe utilisateur pour acc�s � la bdd
     * 
     * @access public
     * @var string $db_password
     */
    var $dbPassword;
    
    
    
    /**
     * Nom de la base de donn�es � utiliser
     * 
     * @access public
     * @var string $db_name
     */
    var $dbName;
    
    
    
    /**
     * Constructeur
     * 
     * @param array $options Tableaux des param�tres
     * @access public
     * @return void
     */
    function Multipage_DB($options) {
        $this->query = '';
        parent::Multipage($options);
    }
    
    
    
    /**
     * Cr�ation de la requ�te SQL
     * 
     * G�n�re la requ�te SQL suivant le mode de fonctionnement.
     * En mode arbitraire, la clause LIMIT est utilis�e. En mode alphab�tique, c'est la clause LIKE qui est utilis�e.
     * Stocke le r�sultat dans la propri�t� LimitSQL
     * @access private
     * @return void
     * @since 1.0
     */
	function createSQL() {
        if ($this->multipage_mode == 'arbitraire') {
            
            if (MULTIPAGE_DB_MODE == 'mysql') {
                // Gestion de la clause limit en natif pour MySql
                $this->aSql['limit'] = $this->firstline .', '. $this->perPage;
                $this->LimitSQL = $this->buildSql($this->aSql);
            }
            else {
                // Pour PEAR::DB & ADOdb, on passe par leur m�thode respective de s�lection de donn�es "limit�es"
                $this->LimitSQL = $this->buildSql($this->aSql);
            }
            
        } else {
            (Empty($this->aSql['where'])) ? $this->aSql['where'] = $this->buildAlphaLikeClause() : $this->aSql['where'] .= ' and ' . $this->buildAlphaLikeClause();
            $this->LimitSQL = $this->buildSql($this->aSql);
        }
	} // end func createSQL
    
    
    
    /**
     * Ex�cute la requ�te sql et renvoie le tableau des donn�es
     * 
     * Couche d'abstraction d'ex�cution des requ�tes
     * @access private
     * @return array
     * @since 3.1b
     */
    function getRecords() {
        
        switch(MULTIPAGE_DB_MODE) {
            case 'mysql' :
                return $this->mysql_buffered_query($this->LimitSQL, $this->_cnx);
            break;
            
            case 'adodb' :
                if ($this->multipage_mode == 'arbitraire') {
                    $rst = $this->_cnx->SelectLimit($this->LimitSQL, $this->perPage, $this->firstline);
                    return $rst->GetRows();
                }
                else {
                    return $this->_cnx->GetAll($this->LimitSQL);
                }
            break;
            
            case 'pear' :
                if ($this->multipage_mode == 'arbitraire') {
                    return $this->pear_buffered_query($this->_cnx->limitQuery($this->LimitSQL, $this->firstline, $this->perPage));
                } else {
                    // Comme il n'existe pas de fetch_mode = 'BOTH' avec Pear, il va falloir la g�rer � la main
                    if (($this->setFetchMode == 'NUM') || ($this->setFetchMode == 'ASSOC')) return $this->_cnx->GetAll($this->LimitSQL);
                    else return $this->pear_buffered_query($this->_cnx->query($this->LimitSQL));
                }
            break;
        }
    } // end func getRecords
    
    
    
    /**
     * G�n�ration de la clause LIKE
     * 
     * Construit la clause LIKE de la requ�te SQL lorsque la classe fonctionne en mode alphanum�rique.
     * @access private
     * @return string clause LIKE
     * @see getAlphaIndex()
     * @since 3.1
     */
	function buildAlphaLikeClause() {
        
        // Teste la pr�sence de $alphaColumn dans la clause "select"
        if (preg_match('/'. trim($this->alphaColumn) .'/i', $this->aSql['select'])==0) trigger_error('Colonne <b>' . $this->alphaColumn . '</b> introuvable dans la clause SELECT de la requ�te sql (<i>'. $this->query .'</i>)<br>', E_USER_ERROR);
        
        // Suppression de la clause limit si pr�sente
        if (IsSet($this->aSql['limit'])) unset($this->aSql['limit']);
        
        // IndexStart repr�sente l'index du tableau alpha-num�rique auquel il faut commencer, IndexEnd la fin
        $like       = '';
        $aIndex     = $this->getAlphaIndex();
        $IndexStart = $aIndex[0];
        $IndexEnd   = $aIndex[1];
        unset($aIndex);
        
        while ($IndexStart != $IndexEnd) {
            if (!IsSet($this->aAlpha[$IndexStart])) break;
            
            if     (MULTIPAGE_DB_MODE  == 'mysql') $like .= $this->alphaColumn . ' like "' . mysql_real_escape_string($this->aAlpha[$IndexStart]) . '%"';
            elseif (MULTIPAGE_DB_MODE  == 'adodb') $like .= $this->alphaColumn . ' like '  . $this->_cnx->qStr($this->aAlpha[$IndexStart] . '%', get_magic_quotes_gpc());
            elseif (MULTIPAGE_DB_MODE  == 'pear')  $like .= $this->alphaColumn . ' like '  . $this->_cnx->quoteSmart($this->aAlpha[$IndexStart] . '%');
            
            $IndexStart++;
            
            if ($IndexStart < $IndexEnd && IsSet($this->aAlpha[$IndexStart])) $like .= ' or ';
        }
        
        return $like;
    } // end func buildAlphaLikeClause
    
    
    
    /**
	 * Extrait les diff�rentes clauses d'une requete SQL et  les retourne dans un tableau
     *
     * Code original par David Duret
	 * @access private
	 * @param string $sql requ�te SQL � traiter
	 * @return array l'index repr�sente le mot cl� de la clause, la valeur de l'index contient la suite de la clause sans le mot cl�
     * @since 2.0
	 */
    function sql2array() {
		
		$pattern = '/^select(.*?)from(.*?)(?:where(.*?))?(?:group by(.*?))?(?:having(.*?))?(?:order by(.*?))?(?:limit(.*?))?$/is';
		preg_match($pattern, trim($this->query), $match);
        
		$result['select']	= trim($match[1]);
		$result['from']		= trim($match[2]);
		$result['where']	= ( isset($match[3]) ) ? trim($match[3]) : '';
		$result['group by']	= ( isset($match[4]) ) ? trim($match[4]) : '';
		$result['having']	= ( isset($match[5]) ) ? trim($match[5]) : '';
		$result['order by']	= ( isset($match[6]) ) ? trim($match[6]) : '';
		$result['limit']	= ( isset($match[7]) ) ? trim($match[7]) : '';
        
		return $result;
	} // end func sql2array
    
    
    
    /**
     * Reconstruction de la requ�te sql suivant le d�coupage cr�� par sql2array()
     * 
     * @access private
     * @return string requ�te sql
     * @see sql2array()
     * @since 3.0
     */
    function buildSql($aSql) {
        
        $sql = '';
        
        if (is_array($aSql)) {
            
            while (list($keyword, $clause) = each($aSql)) {
                if (!empty($clause)) {
                    $sql .= $keyword . ' ' . $clause . ' ';
                }
            }
        }
        
        return $sql;
    } // end func buildSql
    
    
    
    /** 
     * Description
     * 
     * @param 
     * @access public/private
     * @return void
     * @since 3.2
      */
    function config() {
        $options = parent::config();
        $this->setConfig($options['SGBD']);
    } // end func config
    
    
    
    /** 
     * Initialisation de la classe mode sql
     * 
     * @access private
     * @return void
     * @since 3.2
      */
    function init() {
        if (!is_string($this->query) || empty($this->query)) trigger_error('Propri�t� <b>query</b> mal configur�e <br>', E_USER_ERROR);
        if (($this->setFetchMode != 'BOTH') && ($this->setFetchMode != 'NUM') && ($this->setFetchMode != 'ASSOC')) trigger_error('Propri�t� <b>setFetchMode</b> mal configur�e <br>', E_USER_ERROR);
        
        (Empty($this->alphaColumn)) ? $this->multipage_mode == 'arbitraire' : $this->multipage_mode = 'alphabetique';
        
        $this->Connect();
        $this->aSql = $this->sql2array();
		$this->createSQL();
    } // end func init
    
    
    
    /** 
     * Termine l'appel � la classe
     * 
     * @access private
     * @return void
     * @since 3.2
      */
    function close() {
        // On ne ferme pas la connexion si l'on passe par une connexion directe
        if (empty($this->handle)) {
            if (MULTIPAGE_DB_MODE == 'mysql')       return mysql_close($this->_cnx);
            else if (MULTIPAGE_DB_MODE == 'adodb')  return $this->_cnx->Close();
            else if (MULTIPAGE_DB_MODE == 'pear')   return $this->_cnx->disconnect();
        }
    } // end func close
    
    
    
    /**
     * Retourne le nombre d'enregistrement contenu dans la requ�te de s�lection des donn�es
     * @access private
     * @since 3.1b
     */
    function getNbRecord() {
        
        // Requ�te comptant le nombre de r�sultat
        if ($this->aSql['group by'] != '') {
            return $this->getNbRecordFromGroupBy();
        }
        
        $sql_counter             = $this->aSql;
        $sql_counter['select']   = 'count(*)';
        $sql_counter['limit']    = '';
        $sql_counter['order by'] = '';
        $sql_counter['group by'] = '';
        
        $sql_counter             = $this->buildSql($sql_counter);
        
        if (MULTIPAGE_DB_MODE == 'mysql') {
            $rst = mysql_query($sql_counter) or die('Erreur requ�te sql comptant le nombre d\'enregistrement');
            return mysql_result($rst, 0);
        }
        else if (MULTIPAGE_DB_MODE == 'adodb' || MULTIPAGE_DB_MODE == 'pear') {
            return $this->_cnx->GetOne($sql_counter);
        }
        
    } // end func getNbRecord
    
    
    
    /**
     * Cas sp�cifique en sql : on ne peut pas utiliser simplement
     * un count(*) sur une requ�te disposant d'une clause group by.
     * La requ�te suivante fonctionne (en tout cas via mssql) :
     * $sql = 'SELECT count(*) FROM (select ... group by ...) as toto
     * mais je ne suis pas s�r que ce code soit tr�s "standard".
     * Du coup, on pr�f�rera l'appel au classique recordcount().
     */
    function getNbRecordFromGroupBy() {
        
        $sql = $this->buildSql($this->aSql);
        
        if (MULTIPAGE_DB_MODE == 'mysql') {
            $rs    = mysql_query($sql) or die('Erreur requ�te sql comptant le nombre d\'enregistrement');
            $nbRec = mysql_num_rows($rs);
            
            unset($rs);
            
            return $nbRec;
        }
        else if (MULTIPAGE_DB_MODE == 'adodb') {
            
            $rs    = $this->_cnx->Execute($sql);
            $nbRec = $rs->RecordCount();
            unset($rs);
            
            return $nbRec;
        }
        else if (MULTIPAGE_DB_MODE == 'pear') {
            $rs   =& $this->_cnx->query($sql);
            $nbRec = $rs->numRows();
            unset($rs);
            
            return $nbRec;
        }
    }
    
    
    
    /**
     * Cr�ation de la connexion suivant le type de SGBD
     * 
     * @return void
     * @access private
     * @since 3.1b
     */
    function Connect() {
        // [0] => driver sgbd
        // [1] => native / adodb / pear
        $db_infos = explode(':', $this->dbType);
        
        if (count($db_infos) != 2) trigger_error('Le type de la base de donn�e est mal configur�. V�rifiez votre param�tre "dbType".', E_USER_ERROR);
        
        switch($db_infos[1]) {
            case 'native' :
                if (empty($this->handle)) {
                    $this->_cnx = mysql_connect($this->dbHostname, $this->dbUser, $this->dbPassword) or trigger_error('Impossible de se connecter � la base. V�rifiez les param�tres de connexion.', E_USER_ERROR);
                    mysql_select_db($this->dbName) or trigger_error('Impossible de se connecter � la base. V�rifiez le nom de la base dans les param�tres de connexion', E_USER_ERROR);
                }
                else {
                    $this->_cnx = &$this->handle;
                }
                define('MULTIPAGE_DB_MODE', 'mysql');
            break;
            
            case 'adodb' :
                if (empty($this->handle)) {
                    if (!class_exists('adoconnection')) trigger_error('La classe ADOdb est introuvable.<br>V�rifiez le pr�chargement de la classe ADOdb avant l\'appel � la classe Multipage.<br>Ex. : include(\'adodb.inc.php\')<br>', E_USER_ERROR);
                    $this->_cnx = &ADONewConnection($db_infos[0]);
                    $this->_cnx->Connect($this->dbHostname, $this->dbUser, $this->dbPassword, $this->dbName);
                }
                else {
                    if (!eregi('adodb', get_class($this->handle))) trigger_error('La ressource pass�e en param�tre ne s\'apparente pas � un objet PEAR::DB<br>V�rifiez votre objet de connexion (propri�t� "handle") ainsi que le mode de connexion utilis� (propri�t� "dbType").<br>', E_USER_ERROR);
                    $this->_cnx = &$this->handle;
                }
                
                // Sous quelle forme doit �tre retourn� le tableau des enregistrements
                eval('$ADODB_FETCH_MODE = ADODB_FETCH_' . $this->setFetchMode . ';');
                $this->_cnx->SetFetchMode($ADODB_FETCH_MODE);
                
                define('MULTIPAGE_DB_MODE', 'adodb');
            break;
            
            case 'pear' :
                if (empty($this->handle)) {
                    if (!class_exists('DB')) trigger_error('La classe PEAR::DB est introuvable.<br>V�rifiez le pr�chargement de la classe PEAR::DB avant l\'appel � la classe Multipage.<br>Ex. : include(\'DB.php\')<br>', E_USER_ERROR);
                    $dsn = $db_infos[0] . '://' . $this->dbUser . ':' . $this->dbPassword . '@' . $this->dbHostname . '/' . $this->dbName;
                    $this->_cnx = DB::connect($dsn);
                }
                else {
                    if (!eregi('DB', get_class($this->handle))) trigger_error('La ressource pass�e en param�tre ne s\'apparente pas � un objet PEAR::DB<br>V�rifiez votre objet de connexion (propri�t� "handle") ainsi que le mode de connexion utilis� (propri�t� "dbType").<br>', E_USER_ERROR);
                    $this->_cnx = &$this->handle;
                }
                
                if ($this->setFetchMode == 'NUM')  $this->_cnx->setFetchMode(DB_FETCHMODE_ORDERED);
                else if ($this->setFetchMode == 'ASSOC')  $this->_cnx->setFetchMode(DB_FETCHMODE_ASSOC);
                
                // La m�thode 'BOTH' n'existe pas avec pear. Elle est g�r�e manuellement par la m�thode pear_buffered_query
                define('MULTIPAGE_DB_MODE', 'pear');
            break;
            
            default :
                trigger_error('Le mode de connexion � la base de donn�es est inconnu. V�rifiez votre param�tre "dbType". Les valeurs possibles sont "native/adodb"', E_USER_ERROR);
            break;
        }
    } // end func Connect
    
    
    
    /**
     * Retourne le contenu de la requ�te SQL dans un tableau de type :
     * Array[num_row] = Array(
     *      [field_number] => field_value,
     *      [field_name]   => field_value
     * );
     * 
     * MySQL uniquement
     * @access private
     * @param data_select string Requ�te de s�lection des donn�es
     * @param data_connection La connexion sql
     * @return array
     * @since 3.1b
     */
    function mysql_buffered_query($data_select, $data_connection) {
        $sql_query      = mysql_query($data_select, $data_connection);
        $sql_num_fields = mysql_num_fields($sql_query);
        $info_rows      = 0;
        
        while($sql_fetch_rows = mysql_fetch_row($sql_query)) { 
            $info_offset   = 1;
            $info_columns  = 0;
            
            while ($info_offset <= $sql_num_fields) {
                
                if ($this->setFetchMode == 'BOTH' || $this->setFetchMode == 'NUM')
                    $info_elements[$info_rows][$info_columns] = $sql_fetch_rows[$info_columns];
                
                if ($this->setFetchMode == 'BOTH' || $this->setFetchMode == 'ASSOC')
                    $info_elements[$info_rows][mysql_field_name($sql_query, $info_columns)] = $sql_fetch_rows[$info_columns];
                
                $info_offset++; $info_columns++;
            }
            
            $info_rows++;
        }
        
        return $info_elements;
    } // end func mysql_buffered_query
    
    
    
    /**
     * Retourne le contenu d'une ressource Pear::DB_result dans un tableau de type :
     * Array[num_row] = Array(
     *      [field_number] => field_value,
     *      [field_name]   => field_value
     * );
     * 
     * Connexion via PEAR uniquement
     * @access private
     * @param Pear::DB_result $rst Ressource
     * @return array
     * @since 3.2
     */
    function pear_buffered_query($rst) {
        $aTableInfo     = $rst->tableInfo();
        $sql_num_fields = count($aTableInfo);
        $info_rows      = 0;
        
        while ($sql_fetch_rows = $rst->fetchRow()) {
            $info_offset  = 1;
            $info_columns = 0;
            
            while ($info_offset <= $sql_num_fields) {
                
                if ($this->setFetchMode == 'BOTH') {
                    $info_elements[$info_rows][$info_columns] = $sql_fetch_rows[$info_columns];
                    $info_elements[$info_rows][$aTableInfo[$info_columns]['name']] = $sql_fetch_rows[$info_columns];
                }
                else if ($this->setFetchMode == 'NUM') {
                    $info_elements[$info_rows][$info_columns] = $sql_fetch_rows[$info_columns];
                }
                else {
                    $info_elements[$info_rows][$aTableInfo[$info_columns]['name']] = $sql_fetch_rows[$aTableInfo[$info_columns]['name']];
                }
                
                $info_offset++; $info_columns++;
            }
            
            $info_rows++;
        }
        
        $rst->free();
        return $info_elements;
    } // end func pear_buffered_query
}
?>