<?php
/**
 * Classe Multipage
 * @package multipage
 *
 * Fichier de configuration de la classe multipage pour un projet entier
 * Gestion des param�tres de connexion � la base ainsi que des libell�s
 * Ce fichier sert de valeurs par d�faut pour un projet entier, mais rien n'emp�che de les param�trer directement
 * lors de la cr�ation d'un objet Multipage
 * Ex :
 * <code>
 * $Pager = new Multipage();
 * $Pager-> next_page = '>';
 * </code>
 */

/********************************************************
 * Informations sur la base de donn�es
 ********************************************************/

$options['SGBD'] = Array(
    'dbType'       => 'mysql:native',   // Type de la base de donn�es :
                                        //         'mysql:native'
                                        //         'driver:adodb' (o� 'driver' repr�sente un type de sgbd support� par la librairie ADOdb)
                                        //         'driver:pear' (o� 'driver' repr�sente un type de sgbd support� par la librairie PEAR::DB)
                                        //         'handle:mode' (o� 'mode' repr�sente l'un des choix suivant : 'pear', 'adodb', 'native')
    'dbHostname'   => 'localhost',      // Nom ou @IP du SGDB
    'dbUser'       => 'root',           // Utilisateur
    'dbPassword'   => '',               // Mot de passe
    'dbName'       => 'multipagetest',  // Base de donn�es
    'setFetchMode' => 'BOTH'            // D�termine sous quelle forme sera retourn� le tableau des enregistrements ['NUM' | 'ASSOC' | 'BOTH']
);



/**************************************
 * Param�trage global du multipage 
 **************************************/

$options['PAGER'] = Array(
    'perPage'        => 20,                 // Nombre de r�sultats par page
    'delta'          => 5,                  // Le nombre de liens maximum souhait� dans le multipage (0 = tous les liens)
    'alwaysShow'     => true,               // Que faire si le multipage n'est pas n�cessaire? L'afficher ou non?
    'toHtmlEntities' => false,              // Positionn� � true, les caract�res sp�ciaux des libell�s seront traduits en leur entit� HTML
    'encodeVarUrl'   => false,              // Positionn� � true, les param�tres pass�s par Url seront encod�s
    'display'        => 'sliding'           // 'sliding' || 'jumping'
);



/****************************
 * Param�trage divers
 ****************************/

$options['PARAMS'] = Array(
    'nextPage'        => 'Suivant',         // libell� lien vers la page suivante
    'previousPage'    => 'Pr�c�dent',       // libell� lien vers la page pr�c�dente
    'lastPage'        => '>>',              // libell� lien vers la derni�re page
    'firstPage'       => '<<',              // libell� lien vers la premi�re page
    'separator'       => '&nbsp;-&nbsp;',   // S�parateur de page
    'curPageSpanPre'  => '<b><u>',          // Chaine pr�fixant la page courante
    'curPageSpanPost' => '</u></b>',        // Chaine suffixant la page courante
    'linkClass'       => ''                 // Classe CSS � ajouter aux liens
);



/**************************************************************************************************
 * Configuration avanc�e (optionnel, vous pouvez laisser les param�tres par d�faut.
 * C'est uniquement pour ceux qui aiment bidouiller.)
 **************************************************************************************************/

$options['ADVANCED'] = Array(
    'varUrl'             => 'p',            // Nom de la variable dans l'url servant � indiquer la page en cours
    'tplVarBegin'        => '<#',           // Syntaxe de d�but pour une variable du fichier template
    'tplVarEnd'          => '>',            // Syntaxe de fin pour une variable du fichier template
    'tplVarLoopBegin'    => '<#LOOP>',      // Syntaxe des d�limiteurs pour les zones � r�p�ter du template (voir tpl1.htm pour l'exemple)
    'tplVarLoopEnd'      => '</#LOOP>'
);



// D�finition du nom des variables "template"
$options['TPL_VAR_NAME'] = Array(
    'PAGE'          => 'PAGE',
    'COLSPAN'       => 'COLSPAN',
    'NBRECORD'      => 'NBRECORD',
    'NEXT_PAGE'     => 'NEXT_PAGE',
    'PREVIOUS_PAGE' => 'PREVIOUS_PAGE',
    'FIRST_PAGE'    => 'FIRST_PAGE',
    'LAST_PAGE'     => 'LAST_PAGE',
    'CURRENT_PAGE'  => 'CURRENT_PAGE',
    'TOTAL_PAGE'    => 'TOTAL_PAGE',
    'LIMIT'         => 'LIMIT',
    'FROM'          => 'FROM',
    'TO'            => 'TO'
);
?>