<h1><?php echo $user['prenom'].' '.$user['nom']; ?></h1>
<form class="form-droits" method="post" action="<?php echo $appurl; ?>/outils/action_edit_utilisateur">
<?php
	$droitsform = array();
	$cnt = 0;
	foreach($droits as $droit)
	{
		$fs = "fieldset-1";
		if ($cnt >= 6) $fs = "fieldset-2";
		$droitsform[$fs][$cnt] = array();
		$droitsform[$fs][$cnt]['label'] = $droit->label;
		$droitsform[$fs][$cnt]['name'] = $droit->id_db;
		$droitsform[$fs][$cnt]['val'] = $user[$droit->id_db];
		$cnt++;
	}
	// printr($droitsform);
?>

<?php foreach($droitsform as $fs => $champs) { ?>
	<fieldset id="<?php echo $fs; ?>">
		<table class="table-droits-user" style="font-size:1em">
		<?php foreach($champs as $c => $champ) { ?>
		<tr>
			<td>
				<label for="<?php echo $champ['name']; ?>"><?php echo $champ['label']; ?></label>
			</td>
			<td>
			Lecture <input type="radio" name="<?php echo $champ['name']; ?>" <?php if($champ['val'] == 'lec') echo 'checked="checked"'; ?> value="lec" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			Ecriture <input type="radio" name="<?php echo $champ['name']; ?>" <?php if($champ['val'] == 'ecr') echo 'checked="checked"'; ?> value="ecr" />
			&nbsp;&nbsp;&nbsp;&nbsp;
			Sans <input type="radio" name="<?php echo $champ['name']; ?>" <?php if($champ['val'] != 'lec' && $champ['val'] != 'ecr') echo 'checked="checked"'; ?> value="no" />
			</td>
		</tr>
		<?php } ?>
		</table>
	</fieldset>
<?php } ?>
<fieldset id="fieldset-3">
<input type="hidden" name="cmd" value="upddr" />
<input type="hidden" name="actif" value="<?php echo $user['actif']; ?>" />
<input type="hidden" name="dr_admin" value="<?php echo $user['dr_admin']; ?>" />
<input type="hidden" name="id_user" value="<?php echo $user['id_user']; ?>" />
<input type="submit" name="submit" class="submit" value="Enregistrer" />
</fieldset>
</form>
