<table id="table-infos-user-1">
	<tr>
		<td colspan="2"><h1><?php echo $user['prenom'].' '.$user['nom']; ?></h1></td>
	</tr>
	<tr>
		<td>Identifiant</td><td><?php echo $user['login']; ?></td>
	</tr>
	<tr>
		<td>M&eacute;l</td><td><a href="mailto:<?php echo $user['mail']; ?>" title="envoyer un message"><?php echo $user['mail']; ?></a></td>
	</tr>
	<?php if(isset($user['auteur'])) { ?>
		<tr>
			<td>Cr&eacute;&eacute; ou modifi&eacute; par</td><td><?php echo $user['auteur']; ?></td>
		</tr>
	<?php } ?>
	<tr>
		<td>Date cr&eacute;ation</td><td><?php echo $user['date_creation']; ?></td>
	</tr>
	<tr>
		<td>Derni&egrave;re modification</td><td><?php echo $user['date_modif']; ?></td>
	</tr>
	<tr>
		<td>Derni&egrave;re connexion</td><td><?php echo convert_mysql_format_to_date($user['date_connexion']); ?></td>
	</tr>
	<?php if ($user['notes'] != '') { ?>
	<tr>
		<td colspan="2" class="box-notes"><?php echo nl2br($user['notes']); ?></td>
	</tr>
	<?php } ?>

	<?php if ($user['actif'] != 'oui') { ?>
	<tr>
		<td colspan="2">Cet utilisateur est <strong>desactiv&eacute;</strong></td>
	</tr>
	<?php } ?>
	<?php if (isset($user['date_validite_debut'])) { ?>
	<tr>
		<td>D&eacute;but validit&eacute; : </td>
		<td><?php echo $user['date_validite_debut']; ?></td>
	</tr>
	<?php } ?>
	<?php if (isset($user['date_validite_fin'])) { ?>
	<tr>
		<td>Fin validit&eacute; :</td>
		<td><?php echo $user['date_validite_fin']; ?></td>
	</tr>
	<?php } ?>
	<?php if ($user['dr_admin'] == 'oui') { ?>
	<tr>
		<td colspan="2">Cet utilisateur est <strong>administrateur</strong> de l'ensemble</td>
	</tr>
	<?php } ?>
	<?php if ($user['dr_webmaster'] == 'oui') { ?>
	<tr>
		<td colspan="2">Cet utilisateur est <strong>super-administrateur</strong> de l'application</td>
	</tr>
	<?php } ?>
</table>
<table id="table-infos-user-2">
	<tr>
		<td colspan="2"><h3>Droits d'acc&egrave;s</h3></td>
	</tr>
	<?php foreach($droits as $droit) { ?>
		<tr>
			<tr>
				<td><?php echo $droit->label; ?></td>
				<td><?php echo get_human_string_droits($user[$droit->id_db]); ?></td>
			</tr>
		</tr>
	<?php } ?>
</table>
