<?php
//=====================================================================
// FBWork4 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Bootstrap : code d'initialisation exécuté avant l'affichage de 
// chaque page. Intégré au FW Atomik.
//
//=====================================================================
define ('IDAPPLI', 'fbwork4'); // utrilisé pour marquer les informations de session
define ('CONFIG_FILE', 'app_config/app_config.xml');
global $CONF;
global $DB;
global $MESSAGES;


// reglages initiaux
ini_set('display_errors', 1);
// Quelques outils personnels
require_once 'fb-tools.php';
// Moteur de templates
require_once 'vtemplate/vtemplate.class.php';
// Quelques outils de la plateforme
include ('outils.php');
// Générateur et validateurs de formulaires
require_once 'fb-forms/fb-forms.php';
// Quelques outils Zend
require_once 'Zend/Loader/Autoloader.php';
setlocale (LC_TIME, 'fr-FR');

Zend_Loader_Autoloader::getInstance();

// pour l'affichage des valeurs monetaires (money_format)
setlocale(LC_MONETARY, 'fr_FR');

// Focntion dans fb_tools.php, doc dans http://forums.ovh.net/printthread.php?threadid=7372
fix_magic_quotes_gpc();



// detection de smartphone (fonction dans fb_tools.php)
if (is_mobile())
{
	Atomik::set('app/smartphone', '1');
}
else
{
	Atomik::set('app/smartphone', '0');
}




// Activer / desactiver l'affichge des messages de debug
// Atomik::set('atomik/debug', true);


// Ajout à la conf le path absolu à la racine du site
$appfile = Atomik::get('atomik/scriptname');
$apppath = dirname(realpath($appfile));
Atomik::set('atomik/apppath', $apppath);
Atomik::set('atomik/tmppath', $apppath.'/tmp');

// Configuration de l'application
$confbranche = 'application';
if (Atomik::get('app/config') != '') $confbranche = Atomik::get('app/config');
// printr($confbranche, "confbranche");
// printr(Atomik::get('app/config'), "app/config");
$CONF = new Zend_Config_Xml (Atomik::get('atomik/apppath').'/app/'.CONFIG_FILE, $confbranche);
// printr($CONF);
// printr($_SERVER);


// printr($_GET, "GET");
// printr($_SERVER, "SERVER");



// -------------------------------------------------------------------------
// Messages de l'interface
if (isset($CONF->messages_interface))
{
	// S'il y a un seul groupe alors il y a un niveau d'arborescence en moins
	if (isset($CONF->messages_interface->item->id))
	{
		$grps[0] = $CONF->messages_interface->item;
	}
	else
	{
		$grps = $CONF->messages_interface->item;
	}
	foreach($grps as $grp)
	{
		$ct = '';
		$vv = '';
		$id = '';
		$txt = '';
		if (isset($grp->id)) $id = $grp->id;
		if (isset($grp->text)) $txt = $grp->text;
		if (isset($grp->controleur)) $ct = $grp->controleur;
		if (isset($grp->view)) $vv = $grp->view;
		if ($id != '' && $ct != '' && $vv != '') $MESSAGES[$ct][$vv][$id] = $txt;
	}
	// printr($MESSAGES, "MESSAGES");

}

// -------------------------------------------------------------------------
Atomik::set('application/title', $CONF->infos->app_name);
Atomik::set('application/description', $CONF->infos->app_description);
Atomik::set('application/version', $CONF->infos->app_version);
Atomik::set('application/author', $CONF->infos->app_author);
Atomik::set('application/webmaster', $CONF->infos->app_webmaster_link);

Atomik::set('application/usedatabase', false);
if ($CONF->use->use_database == 'on' || $CONF->use->use_database == 'oui' || $CONF->use->use_database == 'yes' || $CONF->use->use_database == '1')
{
	Atomik::set('application/usedatabase', true);
	try {
		$DB = Zend_Db::factory('PDO_MYSQL', $CONF->database);
		$DB->getConnection();

	}
	catch (Zend_Exception $e) {
		Atomik::Flash("Une tentative de connexion &agrave; la base de donn&eacute;es a &eacute;chou&eacute;. L'application ne peut pas s'ex&eacute;cuter");
	}
}



// Création d'une variable Atomik avec l'id du controleur et de la vue
$tmp = "index/index";
if (isset($_GET['action'])) $tmp = $_GET['action'];
$tmp = explode("/", $tmp);
$tmpcontroleur = 'index'; if (isset($tmp[0])) $tmpcontroleur = $tmp[0];
$tmpview = 'index'; if (isset($tmp[1])) $tmpview = $tmp[1];
Atomik::set('page/controleur', $tmpcontroleur);
Atomik::set('page/view', $tmpview);
// printr("contr:$tmpcontroleur  view:$tmpview");

// ---------------- MISES EN PAGE SPECIALES ----------------------------
// Convention : les controleurs dont le nom commence par export_ n'utilisent pas
// le layout par défaut, mais un layout approprié
if (substr($tmpcontroleur, 0, 7) == 'export_')
{
	$export_layout = Atomik::get('app/export_layout');
	Atomik::set('app/layout', $export_layout);
}
// Convention : les controleurs dont le nom commence par ajax_ n'utilisent pas
// le layout par défaut, mais un layout approprié
if (substr($tmpcontroleur, 0, 5) == 'ajax_')
{
	$ajax_layout = Atomik::get('app/ajax_layout');
	Atomik::set('app/layout', $ajax_layout);
}

// printr($tmpcontroleur, "tmpcontroleur");
//  printr($CONF->menu_selecteurs->selecteur, "CONF->menu_selecteurs->selecteur");
// Lecture de l'ID pour mettre en surbrillance le menu de la rubrique en cours
foreach($CONF->menu_selecteurs->selecteur as $selecteur)
{
	// printr($selecteur, "selecteur");
	if ($selecteur->controleur == $tmpcontroleur)
	{
		// Cette information est exploitée dans la fonction menu_principal
		Atomik::set('page/menuselected', $selecteur->menuid);
		break;
	}
}

// activation / desactivation du panneau pour le développeur
// Voir le code de la page de login pour une activation automatique pour certains utilisateurs
if (isset($_GET['cmd']) && $_GET['cmd'] == "setdevon") $_SESSION[IDAPPLI]['develop-panel'] = true;
if (isset($_GET['cmd']) && $_GET['cmd'] == "setdevoff") unset($_SESSION[IDAPPLI]['develop-panel']);
		// printr(Atomik::$store, "ATOMIK");
		// exit;




// URL de base de l'application
// Si une valeur est programmée en conf on la prend, sinon
// Valeur calculée avec les variables du serveur  suviantes :
// SERVER_NAME + SCRIPT_NAME sans index.php
if (isset($CONF->infos->app_url) && $CONF->infos->app_url != '')
{
	Atomik::set('atomik/appurl', $CONF->infos->app_url);
}
else
{
// Valeur qui ne tient pas compte si le site est éventuellement en https
// Dans la base cette valeur peut être éventuellement reprogrammée manuellement
	$url = ".";
	$url = "http://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
	$url = str_replace('/index.php', '', $url);
	$url = str_replace('index.php', '', $url);
	// printr($url, "URL");
	Atomik::set('atomik/appurl', $url);
}
if (Atomik::get('application/usedatabase'))
{
	// La base est activée, peut aller voir si les paramètres sont présents

	// Notamment l'url de l'application programmée manuellement par le webmaster
	if (isset($CONF->database->table_parametres) && $CONF->database->table_parametres != '')
	{
		$sql = "SELECT * ";
		$sql .= "FROM ".$CONF->database->table_parametres." ";
		// $sql .= "WHERE nom = 'appurl' ";
		$result = $DB->fetchAll($sql);
		foreach ($result as $k => $r)
		{
			if ($r['nom'] == 'appurl')
			{
				$appurl = $r['valeur'];
				if ($appurl != '')
				{
					Atomik::set('atomik/appurl', $appurl);
				}
			}
			$_SESSION[IDAPPLI]['fw3config'][$r['nom']] = $r['valeur'];
		}
	}
}

// Authentification éventuelle
Atomik::set('application/use_authentification', false);
if ($CONF->use->use_authentification == 'on' || $CONF->use->use_authentification == 'oui' || $CONF->use->use_authentification == 'yes' || $CONF->use->use_authentification == '1')
{
	Atomik::set('application/use_authentification', true);
	Atomik::set('application/auth_table', $CONF->authentification->database_table);
	Atomik::set('application/auth_login', $CONF->authentification->user_field);
	Atomik::set('application/auth_passwd', $CONF->authentification->password_field);

	// redirection des utilisateurs non authentifiés
	if (!isset($_SESSION[IDAPPLI]['user']['login']) || $_SESSION[IDAPPLI]['user']['login'] == '')
	{
		// printr(Atomik::$store, "ATOMIK");
		// Une page personnalisée
		Atomik::set('app/layout', '_nonconnecte-layout');
		if (true) // false pour desactiver la redirection, pour debug
		{
			if (Atomik::get('page/controleur') != 'login')
			{
				/*
				*/
				echo '<html>'."\n";
				echo '<head>'."\n";
				echo '<meta http-equiv="refresh" content="0;url=\''.Atomik::get('atomik/appurl').'/login\'">'."\n";
				echo '</head>'."\n";
				echo '</html>'."\n";
				// Atomik::redirect('login');
			}
		}
	}
}


if (isset($_POST['cmd']) && $_POST['cmd'] == "setdebugaffiche")
{
	unset($_SESSION[IDAPPLI]['dev']['afficher']);
	unset($_POST['cmd']);
	if (isset($_POST) && count($_POST) > 0) $_SESSION[IDAPPLI]['dev']['afficher'] = $_POST;
}


if (isset($_SESSION[IDAPPLI]['user']['id']) && $_SESSION[IDAPPLI]['user']['id'] > 0)
{
	if (Atomik::get('application/usedatabase'))
	{
		global $DB;
		$data['date_connexion'] = date('Y-m-d H:i:s');
		$DB->update('users', $data, "id_user=".$_SESSION[IDAPPLI]['user']['id']);
	}
}

