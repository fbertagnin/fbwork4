<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Outils et fonctions diverses.
//
// ====================================================================

class LoginController
{
	public function __construct()
	{
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		/*
		if (Atomik::get('app/smartphone') == '1')
		{
			Atomik::set('app/layout', '_smartphone-nonconnecte-layout');
		}
		*/
	}
	public function indexsmart ()
	{
	}

	public function index()
	{
		$fileini = Atomik::get('atomik/apppath').'/app/views/login/login_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->login;
		$form = new FBForms($configform, './');
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->form = $form->getform();

	}
	public function valider()
	{
		global $DB;
		global $CONF;
		$fileini = Atomik::get('atomik/apppath').'/app/views/login/login_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->login;
		$form = new FBForms($configform, '');

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			unset($_SESSION[IDAPPLI]['data_form']);
			foreach($errors as $k => $v)
			{
				// if (isset($data[$k])) unset($data[$k]);
			}
			unset($data['cmd']);
			unset($data['passwd']);
			unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash($errors, 'error');
			Atomik::redirect('login');
			return;
		}
		// Le formulaire est bien validé
		// Vérification de l'user
		$login = $_POST['login'];
		$passwd = $_POST['passwd'];
		$crypt = sha1($passwd);
		$sql = 'SELECT * FROM users WHERE login = ? AND passwd = ?';
		$stmt = $DB->query($sql, array($login, $crypt));
		$user = $stmt->fetch();
		if (!$user)
		{
			// l'authentification a échoué
			$errors[] = "Les param&egrave;tres de connexion ne sont pas corrects. Connxion r&eacute;fus&eacute;e.";
			Atomik::flash($errors, 'error');
			Atomik::redirect('login');
			return;
		}
		// L'authentification a réussi
		// Vérifier que le compte est activé
		if ($user['actif'] != 'oui')
		{
			// le compte a été désactivé
			$errors[] = "Vos identifiants sont corrects. Cependant votre compte a été désactivé. Connexion réfusée.";
			Atomik::flash($errors, 'error');
			Atomik::redirect('login');
			return;
		}

		// Vérification des dates de validité
		$aujourdhui  = mktime(0, 0, 0, date("m")  , date("d")+1, date("Y"));
		$dd = convert_date_us_to_timestamp($user['date_validite_debut']);
		if ($dd <= 946681200) $dd = 0;
		$df = convert_date_us_to_timestamp($user['date_validite_fin']);
		if ($df <= 946681200) $df = 0;
		// printr($dd, $user['date_validite_debut']);
		// printr($df, $user['date_validite_fin']);
		// printr($aujourdhui, 'aujourdhui');
		if ($dd > 0 && $dd >  $aujourdhui)
		{
			// date début non valable
			$dat = convert_mysql_format_to_date($user['date_validite_debut']);
			$errors[] = "Vos identifiants sont corrects. Cependant votre compte ne sera pas activ&eacute; avant le $dat. Connexion réfusée.";
			Atomik::flash($errors, 'error');
			Atomik::redirect('login');
			return;
		}
		if ($df > 0 && $df <  $aujourdhui)
		{
			// date fin non valable
			$dat = convert_mysql_format_to_date($user['date_validite_fin']);
			$errors[] = "Vos identifiants sont corrects. Cependant votre compte n'est plus actif depuis le $dat. Connexion réfusée.";
			Atomik::flash($errors, 'error');
			Atomik::redirect('login');
			return;
		}


		// Initialisation de la session pour l'utilisateur identifié
		// printr($user, "USER IDENTIFIE");
		$_SESSION[IDAPPLI]['user'] = array();
		$_SESSION[IDAPPLI]['user']['id'] = $user['id_user'];
		$_SESSION[IDAPPLI]['user']['login'] = $user['login'];
		$_SESSION[IDAPPLI]['user']['nom'] = $user['prenom']." ".$user['nom'];
		$_SESSION[IDAPPLI]['user']['id_compte'] = $user['id_compte']+0;
		$_SESSION[IDAPPLI]['user']['date_connexion'] = convert_mysql_format_to_date($user['date_connexion']);
		$_SESSION[IDAPPLI]['user']['signature'] = $user['signature'];

		$droits = array();
		if ($user['dr_admin'] == 'oui') $droits['admin'] = 1;
		if ($user['dr_webmaster'] == 'oui')
		{
			$droits['webmaster'] = 1;
			if ($_SESSION[IDAPPLI]['user']['id_compte'] < 1) $_SESSION[IDAPPLI]['user']['id_compte'] = 1;

			// ---------------------------------------------------------------
			// pour faciliter les interventions de maintenance et de débogage
			// Activation automatique du panneau de debug pour les users listés
			// dans le fichier app_config.xml
			if (isset($CONF->use->use_debug_panel_users))
			{
				$aa = explode(" ", $CONF->use->use_debug_panel_users);
				if (is_array($aa) && count($aa) > 0)
				{
					foreach($aa as $aaa)
					{
						if ($user['login'] == $aaa)
						{
							$_SESSION[IDAPPLI]['develop-panel'] = 1;
							break;
						}
					}
				}
			}
			// ---------------------------------------------------------------

		}
		foreach($CONF->droits->definition->droit as $k => $droit)
		{
			$id_db = $droit->id_db;
			$n = $droit->name;
			$d = $user[$id_db];
			if ($d == 'lec') $droits[$n] = $d;
			if ($d == 'ecr') $droits[$n] = $d;
		}
		$_SESSION[IDAPPLI]['user']['droits'] = $droits;

		// informations sur le compte associé à l'utilisateur
		$sql = "SELECT nom FROM comptes WHERE id_compte = ? ";
		$stmt = $DB->query($sql, $_SESSION[IDAPPLI]['user']['id_compte']);
		$compte = $stmt->fetch();
		if (!$compte)
		{
			// aucun compe associé à l'utilisateur
			$errors[] = "Attention : aucun compte n'est associ&eacute; à l'utilisateur ".$_SESSION[IDAPPLI]['user']['login'];
			Atomik::flash($errors, 'error');
			if (!isset($_SESSION[IDAPPLI]['user']['droits']['dr_webmaster']) || $_SESSION[IDAPPLI]['user']['droits']['dr_webmaster'] != 'oui')
			{
				Atomik::redirect('login');
				return;
			}
		}
		else
		{
			$_SESSION[IDAPPLI]['user']['nom_compte'] = $compte['nom'];
		}

		// paramètres personnels de l'utilisateurs
		// les valeurs par défaut sont cherchées dans le fichier XML de paramètrage
		// les valeurs modifiées, si elles existent, sont cherchées dans la base
		$_SESSION[IDAPPLI]['user']['params'] = array();
		$pars = $CONF->config_user->param;
		if (isset($CONF->config_user->param->name))
		{
			$pars = array();
			$pars[0] = $CONF->config_user->param; // un seul élément
		}
		foreach($pars as $par)
		{
			$_SESSION[IDAPPLI]['user']['params'][$par->id] = $par->default;
		}
		// Mise à jour des valeurs avec les informations de la base
		$sql = "SELECT * ";
		$sql .= "FROM parametres_user ";
		$sql .= "WHERE id_user = ".$user['id_user']." ";
		$result = $DB->fetchAll($sql);
		if ($result)
		{
			foreach($result as $p)
			{
				$_SESSION[IDAPPLI]['user']['params'][$p['nom']] = $p['value'];
			}
		}

		// ------------------------------------------------------------
		// redirection pour smartphone
		/*
		if (Atomik::get('app/smartphone') == '1')
		{
			Atomik::redirect('smart_index');
		}
		*/
		// ------------------------------------------------------------

		if (isset($droits['webmaster']) && $droits['webmaster'] == 1)
		{
			// C'est le super administrateur
			// $msg = verifier_config_appurl();
			if (isset($msg) && $msg != '')
			{
				$errors[] = $msg;
				Atomik::flash($errors, 'error');
			}
			// Atomik::redirect('superadmin/liste_comptes/');
			Atomik::redirect('/');
		}
		else
		{
			Atomik::redirect('/');
		}
	}

	public function logout()
	{
		unset($_SESSION[IDAPPLI]['user']);
		Atomik::redirect('/');
	}

	public function recuperer_password()
	{
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-login-recuperer_password.css');
		$this->ariane = get_fil_ariane(array(
		));
		$id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		global $DB;
		$fileini = Atomik::get('atomik/apppath').'/app/views/login/recuperer_password_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->recuperer_password;
		$form = new FBForms($configform);
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->appurl = Atomik::get('atomik/appurl');
		$this->form = $form->getform();
		/*if ($error)
		{
			$errors = array(1 => "Erreur...");
			Atomik::flash($errors, 'error');
			Atomik::redirect('personnes');
			return;
		\}*/

		$this->texte = "Page en construction";
	}
	public function action_recuperer_password()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('personnes', true)) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		global $DB;
	}

}
