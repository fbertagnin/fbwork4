<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Outils et fonctions diverses.
//
// ====================================================================

class ajax_aideController
{
	public function index()
	{
		$texte = '';
		$titre = '';
		$soustitre = '';
		$path = Atomik::get('atomik/apppath').'/'.Atomik::get('app/aide_folder');
		$page = 'default';
		if (isset($_GET['page']) && $_GET['page'] != '') $page = $_GET['page'];
		$pageurl =  Atomik::get('atomik/appurl').'/ajax_aide/edit?page='.$page;
		$aa = explode("-",$page);
		// URL de type ?page=folder-floder-fichier
		// le tableau contient les répertoires, le dernier élément le nom du fichier sans extension
		
		for ($i=0; $i < count($aa)-1; $i++)
		{
			$path .= '/'.$aa[$i];
			if (!is_dir($path)) mkdir($path);
		}
		$file = $path . '/' . $aa[count($aa)-1].".doc.html";
		if (!is_file($file)) 
		{
			$texte .= "<h3>Le fichier demandé n'a pas encore été créé</h3>";
		}
		else
		{
			$handle = fopen($file, "r");
			if ($handle) 
			{
				$count = 0;
				while (($buffer = fgets($handle, 4096)) !== false) 
				{
					if ($count == 0) 
					{
						$titre = $buffer;
					}
					elseif($count == 1)
					{
						$soustitre = $buffer;
					}
					else
					{
						$texte .= $buffer;
					}
					$count++;
				}
				fclose($handle);
			}
		}

		// printr($aa, "PAGE");
		// printr($path, "PATH");
		// printr($file, "FILE");
		
		$this->editer = Atomik::get('atomik/appurl').'/ajax_aide/edit?page='.$page;
		if ($titre != '') $this->titre = $titre;
		if ($soustitre != '') $this->soustitre = $soustitre;
		$texte = encodehtml($texte);
		$this->texte = $texte;
	}

	public function edit()
	{
		$texte = '';
		$titre = '';
		$soustitre = '';
		$path = Atomik::get('atomik/apppath').'/'.Atomik::get('app/aide_folder');
		$page = 'default';
		if (isset($_GET['page']) && $_GET['page'] != '') $page = $_GET['page'];
		$pagedoc = $page;
		$aa = explode("-",$page);
		// URL de type ?page=folder-floder-fichier
		// le tableau contient les répertoires, le dernier élément le nom du fichier sans extension
		for ($i=0; $i < count($aa)-1; $i++)
		{
			$path .= '/'.$aa[$i];
			if (!is_dir($path)) mkdir($path);
		}
		$file = $path . '/' . $aa[count($aa)-1].".doc.html";
		if (is_file($file)) 
		{
			$handle = fopen($file, "r");
			if ($handle) 
			{
				$count = 0;
				while (($buffer = fgets($handle, 4096)) !== false) 
				{
					if ($count == 0) 
					{
						$titre = $buffer;
					}
					elseif($count == 1)
					{
						$soustitre = $buffer;
					}
					else
					{
						$texte .= $buffer;
					}
					$count++;
				}
				fclose($handle);
			}
		}

		// printr($aa, "PAGE");
		// printr($path, "PATH");
		// printr($file, "FILE");
		
		$this->titre = $titre;
		$this->soustitre = $soustitre;
		$this->pagedoc = $pagedoc;
		// $texte = encodehtml($texte);
		$texte = encodehtml($texte);
		$texte = stripslashes($texte);
		$texte = htmlentities($texte);
		
		$this->appurl = Atomik::get('atomik/appurl');
		$this->texte = $texte;
	}
	public function action_edit()
	{
		// printr($_POST, 'POST');
		$titre = $_POST['titre'];
		$soustitre = $_POST['soustitre'];
		$texte = $_POST['texte'];
		$page = 'default';
		if (isset($_POST['pagedoc']) && $_POST['pagedoc'] != '') $page = $_POST['pagedoc'];
		$pagedoc = $page;
		$aa = explode("-",$page);
		// le tableau contient les répertoires, le dernier élément le nom du fichier sans extension
		$path = Atomik::get('atomik/apppath').'/'.Atomik::get('app/aide_folder');
		for ($i=0; $i < count($aa)-1; $i++)
		{
			$path .= '/'.$aa[$i];
			if (!is_dir($path)) mkdir($path);
		}
		$file = $path . '/' . $aa[count($aa)-1].".doc.html";
		
		$texte = stripslashes($texte);
		$titre = stripslashes($titre);
		$soustitre = stripslashes($soustitre);
		$texte = $titre."\n".$soustitre."\n".$texte;
		file_put_contents($file, $texte);
	}

}
