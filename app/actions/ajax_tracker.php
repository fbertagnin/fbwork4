<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : 
//
//=====================================================================

class ajax_trackerController
{
	public function __construct()
	{
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific', "assets/css/appli/specific/".$this->controler.".css");
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array()); // peut être surchargé dans une fonction
	}

	public function ajouter_fichier()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-ajax_services-recherche_personne.css');
		$id_ticket = 0;
		if(isset($_GET['id_ticket'])) $id_ticket=$_GET['id_ticket']+0;

		$fileini = Atomik::get('atomik/apppath').'/app/views/ajax_tracker/ajouter_fichier_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_fichier;
		$form = new FBForms($configform);
		$_SESSION[IDAPPLI]['data_form']['id_ticket'] = $id_ticket;
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->form = $form->getform();
		
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
	}

}
	 

