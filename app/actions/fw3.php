<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 3
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2014, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Outils et fonctions diverses.
//
//=====================================================================

class Fw3Controller
{
	public function __construct()
	{
		$this->id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		$this->id_user=Atomik::get('session/'.IDAPPLI.'/user/id');
		if ($this->id_compte == 0) {erreur_inattendue("ERREUR : le compte n'a pas pu être identifié");return;}
		// ----------------------------------------------------------------
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler', "assets/css/appli/specific/".$this->controler.".css");
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler."-".$this->view.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler-view', "assets/css/appli/specific/".$this->controler."-".$this->view.".css");
        // Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        // Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array("fw3" => "Framework FBWork4")); // peut être surchargé dans une fonction
	}
		
	public function index()
	{
		global $CONF;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		$this->test = "test";
		$this->appurl = Atomik::get('atomik/appurl');
		// Atomik::flash('Message direct 1');
		// Atomik::redirect('users');

		// Si la base est activée, et si une table de paramètrage existe, recupère la liste des
		// paramètres pour leur mise à jour
		if (Atomik::get('application/usedatabase') && isset($CONF->database->table_parametres))
		{
			global $DB;
			$sql = "SELECT * ";
			$sql .= "FROM ".$CONF->database->table_parametres." ";
			$result = $DB->fetchAll($sql);
			if ($result)
			{
				$this->params = $result;
				foreach($this->params as $i => $par)
				{
					$this->params[$i]['notes'] = encodestringhtml(utf8_encode($par['notes']));
				}
			}
			// printr($this->params);

			if (Atomik::get('app/config') != '')
			{
				$this->messages[] = "Fichier de configuration : utilisation de la branche <strong>".Atomik::get('app/config')."</strong>";
			}
			else
			{
				$this->messages[] = "Fichier de configuration : utilisation de la branche par d&eacute;faut <strong>".Atomik::get('app/config')."</strong>";
			}
			foreach($this->params as $param)
			{
				if ($param['nom']=='database_version' && isset($CONF->database->database_version))
				{
					$lue = trim($param['valeur']);
					$attendue = trim($CONF->database->database_version);
					// printr("lue=$lue attendue=$attendue");
					if ($lue != $attendue) $this->messages[] = "ATTENTION : la version de la base ne correspond pas. Attendue=$attendue lue=$lue";
				}

				if ($param['nom']=='appurl' && $param['valeur'] == '')
				{
					if (isset($CONF->infos->app_url) && $CONF->infos->app_url != '')
					{
						$this->messages[] = "Vérifier l'URL du site configur&eacute;e dans le fichier de conf : <br />".$CONF->infos->app_url;
					}
					else
					{
						$url = Atomik::get('atomik/appurl');
						$this->messages[] = "Vérifier l'URL du site calcul&eacute;e automatiquement : <br />$url";
					}
				}


			}


		}
	}

	public function action_parametres()
	{
		global $CONF;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		if (isset($_POST))
		{
			global $DB;
			unset($_POST['submit']);
			foreach($_POST as $k => $param)
			{
				printr($param, "K=$k");
				$champs = array();
				$champs['valeur'] = $param;
				$DB->update($CONF->database->table_parametres, $champs, "id_param=$k");
			}
		}
		Atomik::redirect('fw3');
	}

	public function nouvelle_page()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		$config = new Zend_Config_Ini(Atomik::get('atomik/apppath').'/app/views/fw3/fw3_nouvelle_page_form.ini', 'formulaires');

		$fileini = Atomik::get('atomik/apppath').'/app/views/fw3/fw3_nouvelle_page_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->nouvelle_page;
		// printr($configform);
		$form = new FBForms($configform);
		$form->setvaleurs(array('controleur' => '', 'page' => 'index'));
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->form = $form->getform();
		$this->appurl = Atomik::get('atomik/appurl');
	}
	public function set_page()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		/*
		$cmd = $_POST['cmd'];
		$page = $_POST['page'];
		$controleur = $_POST['controleur'];
		$typepage = $_POST['typepage'];
		*/
		$cmd = '';
		$page = '';
		$controleur = '';
		$widget = '';
		$onglets = '';
		if (isset($_POST['cmd']))
		{
			$cmd = $_POST['cmd'];
			$page = $_POST['page'];
			$controleur = $_POST['controleur'];
			$typepage = $_POST['typepage'];
			$widget = $_POST['widget'];
			if (isset($_POST['onglets'])) $onglets = $_POST['onglets'];
		}
		if ($cmd == "create" && $controleur != "" && $page != "")
		{
			$view_path = Atomik::get('atomik/apppath')."/app/views/$controleur";
			$view_page = $view_path."/$page.phtml";
			// la view ne doit pas exister, sinon c'est un doublon
			if (is_file($view_page))
			{
				Atomik::flash("Erreur : La view existe d&eacute;j&agrave;");
				Atomik::redirect('/fw3/nouvelle_page');
			}
			else
			{
				// création du fichier de la view
				if ($typepage == '1col')
				{
					$template_file = Atomik::get('atomik/apppath').'/app/views/fw3/nouvelle_page-1col.view.tpl.phtml';
				}
				elseif($typepage == '3col')
				{
					$template_file = Atomik::get('atomik/apppath').'/app/views/fw3/nouvelle_page-3col.view.tpl.phtml';
				}
				elseif($typepage == 'ajax')
				{
					$template_file = Atomik::get('atomik/apppath').'/app/views/fw3/nouvelle_page-ajax.view.tpl.phtml';
				}
				else
				{
					// Par défaut mise en page sur 2 colonnes
					$template_file = Atomik::get('atomik/apppath').'/app/views/fw3/nouvelle_page-2col.view.tpl.phtml';
				}

				if (!is_dir($view_path))
				{
					// création du dossier pour la nouvelle view
					mkdir($view_path);
					chmod($view_path, 0777);
				}

				$buff = "";
				$buff = file_get_contents($template_file);
				$buff = str_replace("@@CONTROLEUR_NAME@@", $controleur, $buff);
				$buff = str_replace("@@FUNCTION_NAME@@", $page, $buff);
				$ex = '';
				if ($widget == 'form')
				{
					$ex .= "<?php







 echo \"\$form\"; ?>\n";

					$template_ini = Atomik::get('atomik/apppath').'/app/views/fw3/form.tpl.ini';
					$dest_ini = Atomik::get('atomik/apppath')."/app/views/$controleur/$page"."_form.ini";
					$in = file_get_contents($template_ini);
					$in = str_replace("@@formname@@", $page, $in);
					$in = str_replace("@@controleur@@", $controleur, $in);
					file_put_contents($dest_ini, $in);
					chmod($dest_ini, 0777);

				}
				if ($widget == 'tabledata')
				{
					$template_tabledata = Atomik::get('atomik/apppath').'/app/views/fw3/tabledata.inc.phtml';
					$ex .= file_get_contents($template_tabledata);
				}
				if ($onglets == 'onglets')
				{
					$template_onglets = Atomik::get('atomik/apppath').'/app/views/fw3/onglets.inc.phtml';
					$ex .= file_get_contents($template_onglets);
				}
				$buff = str_replace("@@EXTRAS@@", $ex, $buff);

				file_put_contents($view_page, $buff);
				chmod($view_page, 0777);

				$controleur_file = Atomik::get('atomik/apppath')."/app/actions/$controleur.php";
				if (!is_file($controleur_file))
				{
					// création du fichier avec la classe controleur
					$template_file = Atomik::get('atomik/apppath').'/app/views/fw3/nouvelle_page.class.tpl.php';
					$buff = "";
					$buff = file_get_contents($template_file);
					$buff = str_replace("@@CONTROLEUR_NAME@@", $controleur, $buff);
					file_put_contents($controleur_file, $buff);
					chmod($controleur_file, 0777);
				}
				// chargement du code du controleur
				$buff = file_get_contents($controleur_file);
				// Ajout des éventuels include en début du fichier
				$wg = "<?php










\n";
				$cd = '';
				$cd1 = '';
				if ($widget == 'form')
				{
					$cd .= "\t\t\$id_ensemble=Atomik::get('session/".IDAPPLI."/user/id_ensemble')+0;\n";
					$cd .= "\t\tglobal \$DB;\n";
					$cd .= "\t\t\$fileini = Atomik::get('atomik/apppath').'/app/views/$controleur/$page"."_form.ini';\n";
					$cd .= "\t\t\$configform = new Zend_Config_Ini(\$fileini, 'formulaires');\n";
					$cd .= "\t\t\$configform = \$configform->$page;\n";
					$cd .= "\t\t\$form = new FBForms(\$configform);\n";
					$cd .= "\t\tif (isset(\$_SESSION[IDAPPLI]['data_form'])) \$form->setvaleurs(\$_SESSION[IDAPPLI]['data_form']);\n";
					$cd .= "\t\tunset(\$_SESSION[IDAPPLI]['data_form']);\n";
					$cd .= "\t\t\$this->appurl = Atomik::get('atomik/appurl');\n";
					$cd .= "\t\t\$this->form = \$form->getform();\n";
					$cd .= "\t\t/*if (\$error)\n";
					$cd .= "\t\t{\n";
					$cd .= "\t\t\t\$errors = array(1 => \"Erreur...\");\n";
					$cd .= "\t\t\tAtomik::flash(\$errors, 'error');\n";
					$cd .= "\t\t\tAtomik::redirect('personnes');\n";
					$cd .= "\t\t\treturn;\n";
					$cd .= "\t\t\}*/\n";

					$cd1 .= "\tpublic function action_$page()\n";
					$cd1 .= "\t{\n";
					$cd1 .= "\t\t// ---- vérification des droits d'accès à cette fonctionnalité ----\n";
					$cd1 .= "\t\tif (!verif_droits_acces('personnes', true)) {Atomik::redirect('fw3/non_autorise'); return;}\n";
					$cd1 .= "\t\t// ----------------------------------------------------------------\n";
					$cd1 .= "\t\t\$id_ensemble=Atomik::get('session/".IDAPPLI."/user/id_ensemble')+0;\n";
					$cd1 .= "\t\tglobal \$DB;\n";
					$cd1 .= "\t}\n";
				}
				if ($widget == 'tabledata')
				{
					$cd .= "\t\t\$id_ensemble=Atomik::get('session/".IDAPPLI."/user/id_ensemble')+0;\n";
					$cd .= "\t\tglobal \$DB;\n";
					$cd .= "\t\t\$sql = \"SELECT *, \";\n";
					$cd .= "\t\t\$sql .= \"DATE_FORMAT(date_creation, '%d/%m/%Y') as date_creation, \";\n";
					$cd .= "\t\t\$sql .= \"DATE_FORMAT(date_modif, '%d/%m/%Y') as date_modif \";\n";
					$cd .= "\t\t\$sql .= \"FROM users \";\n";
					$cd .= "\t\t\$sql .= \"WHERE id_ensemble = \$id_ensemble \";\n";
					$cd .= "\t\t\$stmt = \$DB->query(\$sql);\n";
					$cd .= "\t\t\$result = \$stmt->fetchAll();\n";
					$cd .= "\t\t\$this->table = \$result;\n";
					$cd .= "\t\t\$this->appurl = Atomik::get('atomik/appurl');\n";
					$cd .= "\t\t// if (\$error)\n";
					$cd .= "\t\t// {\n";
					$cd .= "\t\t\t// \$errors = array(1 => \"Erreur...\");\n";
					$cd .= "\t\t\t// Atomik::flash(\$errors, 'error');\n";
					$cd .= "\t\t\t// Atomik::redirect('personnes');\n";
					$cd .= "\t\t\t// return;\n";
					$cd .= "\t\t// }\n";


					$wg .= "Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.8.2/media/js/jquery.dataTables.min.js');\n";
					$wg .= "Atomik::set('styles/datatab', 'assets/libs/DataTables-1.8.2/media/css/gestpar_table_1.css');\n";
				}

				$buff = str_replace('<?php







', $wg, $buff);

				// suppression de la dernière }
				$der = strrchr($buff, "}");
				$buff = substr($buff, 0, strlen($buff) - strlen($der));

				$buff .= "\n";
				$buff .= "\tpublic function $page()\n";
				$buff .= "\t{\n";
				$buff .= "\t\t// ---- vérification des droits d'accès à cette fonctionnalité ----\n";
				$buff .= "\t\tif (!verif_droits_acces('personnes', true)) {Atomik::redirect('fw3/non_autorise'); return;}\n";
				$buff .= "\t\t// ----------------------------------------------------------------\n";
				$buff .= "\t\t// Atomik::set('styles/specific', 'assets/css/appli/specif-$controleur-$page.css');\n";
				if ($onglets == 'onglets') $buff .= "\t\tAtomik::set('styles/onglets', 'assets/css/appli/onglets.css');\n";
				// fil d'ariane
				$buff .= "\t\t\$this->ariane = get_fil_ariane(array(\n";
				$buff .= "\t\t\t'$controleur' => '$controleur'\n";
				$buff .= "\t\t));\n";

				$buff .= "$cd\n";
				$buff .= "\t\t\$this->texte = \"Page en construction\";\n";
				$buff .= "\t\t\$this->appurl = Atomik::get('atomik/appurl');\n";
				$buff .= "\t\t\$this->apppath = Atomik::get('atomik/apppath');\n";
				$buff .= "\t}\n";

				$buff .= "$cd1\n";

				$buff .= "}\n";
				file_put_contents($controleur_file, $buff);
				chmod($controleur_file, 0777);

				$action_file = Atomik::get('atomik/apppath')."/app/actions/$page.php";
				Atomik::redirect($_POST['controleur']."/$page/");
			}
		}
		else
		{
			Atomik::redirect('/');
			Atomik::flash("Param&egrave;tres non corrects");
		}
	}

	public function crypt_passwd()
	{
		$login = '';
		$passwd = '';
		$errors = NULL;

		$fileini = Atomik::get('atomik/apppath').'/app/views/fw3/fw3_form_crypt_passwd.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->crypt_passwd;
		$form = new FBForms($configform);

		if (isset($_POST['cmd'])) $errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			Atomik::flash($errors, 'error');
			Atomik::redirect('fw3/crypt_passwd');
		}
		else
		{
			if (isset($_POST['login'])) $login = $_POST['login'];
			if (isset($_POST['passwd'])) $passwd = $_POST['passwd'];
			if ($login != "" && $passwd != "")
			{
				$crypt = crypt($login, $passwd);
				$crypt2 = crypt($passwd, $login);
				$this->login = $login;
				$this->passwd = $passwd;
				$this->crypt = $crypt;
				$this->crypt2 = $crypt2;
				$this->login_md5 = md5($login);
				$this->passw_md5 = md5($passwd);
				$this->passw_sha = sha1($passwd);
				$this->crypt3 = crypt($passwd, base64_encode($passwd));
			}
		}

		$this->form = $form->getform();
	}
	public function test_form ()
	{
		$data = $_POST;
		$fileini = Atomik::get('atomik/apppath').'/app/views/fw3/fw3_form_test_2.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->crypt_passwd;
		$form = new FBForms($configform);
		$rep = $form->verifreponse($_POST);
		printr($rep);
	}

	public function devpanelon()
	{
		$_SESSION[IDAPPLI]['develop-panel'] = true;
	}
	public function devpaneloff()
	{
		$_SESSION[IDAPPLI]['develop-panel'] = false;
	}

	public function doc_display()
	{
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'Framework'
		));
		$doc = '';
		if (isset($_GET['doc'])) $doc = $_GET['doc'];
		$doc = Atomik::get('atomik/apppath')."/doc/pages/doc_$doc.html";
		// printr($doc);
		if (is_file($doc))
		{
			$this->texte = file_get_contents($doc);
			$this->ariane = get_fil_ariane(array(
				'fw3' => 'Framework',
				'fw3/doc_display' => 'Documentation'
			));
			$this->page = "";
			if (isset($_GET['doc'])) $this->page = $_GET['doc'];
		}
		else
		{
			$this->texte = "Page de documentation non disponible";
		}
		$this->appurl = Atomik::get('atomik/appurl');
		$this->files = scan_directory(Atomik::get('atomik/apppath')."/doc/pages/");
	}


	public function doc_edit()
	{
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'fw3',
			'fw3/doc_display' => 'Documentation'
		));

		$doc = '';
		if (isset($_GET['doc'])) $doc = $_GET['doc'];
		$doc = Atomik::get('atomik/apppath')."/doc/pages/doc_$doc.html";
		$content = "";
		if (is_file($doc))
		{
			$content = file_get_contents($doc);
		}
		else
		{
			$content = "";
			$doc = "";
		}

		$fileini = Atomik::get('atomik/apppath').'/app/views/fw3/doc_edit_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->doc_edit;
		$form = new FBForms($configform);
		$vals = array();
		$vals['text'] = $content;
		$form->setvaleurs($vals);
		$this->form = $form->getform();

		$this->appurl = Atomik::get('atomik/apppurl');
		$this->texte = "Page en construction";
	}

	public function action_doc_edit()
	{
		$doc = '';
		if (isset($_POST['doc'])) $doc = $_POST['doc'];
		$doc = Atomik::get('atomik/apppath')."/doc/pages/doc_$doc.html";
		if (isset($_POST['cmd']) && $_POST['cmd'] == 'set')
		{
			$data = $_POST;
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$text = $data['text'];
		}
		Atomik::redirect("fw3/doc_display?doc=$data[page]");
	}


	public function non_autorise()
	{
		// Atomik::set('styles/specific', 'assets/css/appli/specif-fw3-non_autorise.css');
		$this->ariane = get_fil_ariane(array(
		));

		$this->texte = "Page en construction";
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
	}

	public function redirect_home()
	{
		Atomik::redirect('/');
	}

	public function config_generale()
	{
		global $DB;
		global $CONF;
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		Atomik::set('styles/specific', 'assets/css/appli/specif-fw3-conf_generale.css');
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array(
		));

		$id_section = 0;
		if (isset($_GET['id'])) $id_section=$_GET['id']+0;

		$id_parametre = 0;
		if (isset($_POST['id_parametre'])) $id_parametre=$_GET['id_parametre']+0;

		$param_groupes = array();
		// recupération du nom de la section
		$nom_section = '';
		$ref_section = '';
		foreach($CONF->config_generale->groupe as $g)
		{
			$param_groupes[$g->id] = $g->name;
			if ($g->id == $id_section)
			{
				$nom_section = $g->name;
				$ref_section = $g->ref;
			}
		}

		$sql = "SELECT * ";
		$sql .= "FROM parametres_generaux ";
		$sql .= "WHERE groupe = '$ref_section' ";
		$sql .= "AND id_ensemble = 0 ";
		$sql .= "ORDER BY protege,nom ";
		$result = $DB->fetchAll($sql);
		$this->parametres = $result;
		// printr($result, "PARAMS");


		// printr($param_groupes);
		$this->nom_section = $nom_section;
		$this->id_section = $id_section;
		$this->id_parametre = $id_parametre;
		$this->groupes = $param_groupes;
	}

	public function action_config_generale()
	{
		global $DB;
		global $CONF;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// printr($_POST);

		$cmd = '';
		$id_section = 0;
		$id_parametre = 0;
		$nom = 0;
		if (isset($_POST['cmd'])) $cmd=$_POST['cmd'];
		if (isset($_POST['id_section'])) $id_section=$_POST['id_section']+0;
		if (isset($_POST['id_parametre'])) $id_parametre=$_POST['id_parametre']+0;
		if (isset($_POST['nom'])) $nom=$_POST['nom'];

		// recupération du nom de la section
		$nom_section = '';
		foreach($CONF->config_generale->groupe as $g)
		{
			if ($g->id == $id_section)
			{
				$nom_section = $g->name;
				$ref_section = $g->ref;
			}
		}



		if ($cmd == 'setparam' && $id_section != 0 && $nom != '')
		{
			$champs = array();
			$champs['nom'] = $nom;
			$champs['groupe'] = $ref_section;
			$champs['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id');
			if ($id_parametre != 0)
			{
				$DB->update('parametres_generaux', $champs, "id_parametre=$id_parametre AND protege=0");
			}
			else
			{
				$DB->insert('parametres_generaux', $champs);
			}
		}
		Atomik::redirect("fw3/config_generale?id=$id_section");
	}
	public function action_supprimer_config_generale()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_parametre = 0;
		$id_section = 0;
		if (isset($_GET['id_section'])) $id_section = $_GET['id_section']+0;
		if (isset($_GET['id_parametre'])) $id_parametre = $_GET['id_parametre']+0;
		$DB->delete('parametres_generaux', "id_parametre=$id_parametre");
		Atomik::redirect("fw3/config_generale?id=$id_section");
	}

	public function sessions()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('personnes', true)) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-fw3-sessions.css');
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'fw3'
		));

		try {
			$sessions_path = session_save_path();
			// printr($sessions_path, "sessions_path");
			$dir = get_directory($sessions_path);
			// printr($dir);
			$sessions = array();
			$sessions[] = $_SESSION[IDAPPLI];
			foreach($dir as $f)
			{
				if (substr($f, 0, 5) == 'sess_')
				{
					$session = array();
					$session_file = file_get_contents("$sessions_path/$f");
					$s = preg_split("#(\w+)\|#", $session_file, -1, PREG_SPLIT_DELIM_CAPTURE|PREG_SPLIT_NO_EMPTY);
					// printr($s, "S");
					$iMax = count($s);
					for($i = 0; $i < $iMax; $i = $i+2)
					{
						if (isset($s[$i+1]))
						{
							$session[$s[$i]] = unserialize($s[$i+1]);
						}
					}
					if (isset($session['user']['nom']))
					{
						// printr($session);
						$sessions[] = $session;
					}
				}
			}

			foreach($sessions as $session)
			{
				$s = $session['user']['nom'];
				$s .= " ";
				$s .= $session['user']['nom_ensemble'];
				// printr($s);
			}

			$this->sessions = $sessions;
			$this->appurl = Atomik::get('atomik/appurl');
			$this->apppath = Atomik::get('atomik/apppath');
		}
		catch (Exception $e)
		{
			$this->message = 'ERREUR : '.$e->getMessage();
		}
	}

	public function import_ensembles_users()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_auteur = Atomik::get('session/'.IDAPPLI.'/user/id')+0;
		$this->appurl = Atomik::get('atomik/appurl');
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'fw3'
		));


	}



	public function action_import_ensembles_users()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_auteur = Atomik::get('session/'.IDAPPLI.'/user/id')+0;
		$this->appurl = Atomik::get('atomik/appurl');
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'fw3'
		));

		$supprimer = 'no';
		if (isset($_POST['supprimer'])) $supprimer = $_POST['supprimer'];
		if (!isset($_FILES["file"]))
		{
			$errors = array(0 => "Aucun fichier n'a été envoyé");
			Atomik::flash($errors, 'error');
			Atomik::redirect('fw3/import_ensembles_users');
			return;
		}
		// printr($_FILES);
		// return;
		if ($_FILES["file"]["error"] > 0)
		{
			$errors = array(0 => 'Erreur '.$_FILES["file"]["error"].' dans le chargement du fichier');
			Atomik::flash($errors, "error");
			Atomik::redirect('fw3/import_ensembles_users');
			return;
		}
		// printr($_POST, "POST");
		$file = Atomik::get('atomik/tmppath')."/".$_FILES["file"]["name"];
		move_uploaded_file($_FILES["file"]["tmp_name"], $file);
		$handle = fopen($file, 'r');
		if (!$handle)
		{
			$errors = array(0 => 'Erreur lors de l\'ouverture du fichier');
			Atomik::flash($errors, "error");
			Atomik::redirect('fw3/import_ensembles_users');
			return;
		}
		$ligne = fgetcsv($handle, 4096, ";"); // première ligne, les entêtes des champs
		$count=0;
		$counte=0;
		if ($supprimer == 'oui')
		{
			$DB->delete('ensembles');
			$DB->delete('users');
		}
		while (($ligne = fgetcsv($handle, 4096, ";")) !== FALSE)
		{
			if (count($ligne) > 5)
			{
				// printr($ligne, "LIGNE");
				$count++;
				$ensemble = array();
				$user = array();
				$id_import = $ligne[0]*1;
				$passwd = trim($ligne[14]);
				if ($passwd == "") $passwd = 'depart';
				$passwd = sha1($passwd);
				// printr($ligne);
				$ensemble['id_import'] = $id_import;
				$ensemble['nom'] = $ligne[1];
				$ensemble['adresse1'] = $ligne[2];
				$ensemble['adresse2'] = $ligne[3];
				$ensemble['adresse3'] = $ligne[4];
				$ensemble['codepost'] = $ligne[5];
				$ensemble['ville'] = $ligne[6];
				$ensemble['tel'] = $ligne[7];
				$ensemble['fax'] = $ligne[8];
				$ensemble['site'] = $ligne[9];
				$ensemble['mail'] = $ligne[10];
				$user['nom'] = $ligne[11];
				$user['prenom'] = $ligne[12];
				$user['login'] = $ligne[13];
				$user['passwd'] = $passwd;
				$user['mail'] = $ligne[15];
				$user['dr_admin'] = $ligne[16];
				$user['dr_webmaster'] = $ligne[17];
				$user['dr_01'] = $ligne[18];
				$user['dr_02'] = $ligne[19];
				$user['dr_03'] = $ligne[20];
				$user['dr_04'] = $ligne[21];
				$user['dr_05'] = $ligne[22];
				$user['dr_06'] = $ligne[23];
				$user['dr_07'] = $ligne[24];
				$user['dr_08'] = $ligne[25];
				$user['dr_09'] = $ligne[26];
				$user['dr_10'] = $ligne[27];
				$user['dr_11'] = $ligne[28];
				$user['dr_12'] = $ligne[29];
				$user['dr_13'] = $ligne[30];
				$user['dr_14'] = $ligne[31];
				$user['dr_15'] = $ligne[32];
				// $user['id_ensemble'] = $ligne[0];

				if ($id_import == 0)
				{
					$errors = array(0 => 'Erreur dans la bouce :id_ensemble=0');
					Atomik::flash($errors, "error");
					Atomik::redirect('fw3/import_ensembles_users');
					return;
				}

				$sql = "SELECT id_ensemble, id_import ";
				$sql .= "FROM ensembles ";
				$sql .= "WHERE id_import=$id_import ";
				$result = $DB->fetchRow($sql);
				$id_ensemble = 0;
				if (!$result)
				{
					$ensemble['id_auteur'] = $id_auteur;
					$DB->insert('ensembles', $ensemble);
					$id_ensemble = $DB->lastInsertId();
					$counte++;
				}
				else
				{
					$id_ensemble = $result['id_ensemble'];
				}
				$user['id_ensemble'] = $id_ensemble;
				$user['id_auteur'] = $id_auteur;
				$DB->insert('users', $user);
			}
		}
		fclose($handle);
		$str = "Fin importation.<br />$counte ensemble(s).<br />$count utilisateur(s).<br />Suppression des donn&eacute;es anciennes : $supprimer";
		$errors = array(0 => "$str");
		Atomik::flash($errors, "warning");
		Atomik::redirect('fw3/import_ensembles_users');
		return;

	}

	public function reconstruire_champs_recherche_personnes()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'fw3'
		));

		$sql = "SELECT id_ensemble, nom ";
		$sql .= "FROM ensembles ";
		$sql .= "WHERE 1=1 ";
		$sql .= "ORDER BY nom ";
		$this->ensembles = array();
		$ensembles = $DB->fetchAll($sql);
		if ($ensembles) $this->ensembles = $ensembles;


		$sql = "SELECT ensembles.id_ensemble, ensembles.nom, count(*) as cnt ";
		$sql .= "FROM personnes ";
		$sql .= "JOIN ensembles ON ensembles.id_ensemble=personnes.id_ensemble ";
		$sql .= "WHERE personnes.recherche = '' ";
		$sql .= "GROUP BY id_ensemble ";
		$sql .= "ORDER BY ensembles.nom ";
		$this->ensemblesatraiter = array();
		$ensembles = $DB->fetchAll($sql);
		if ($ensembles) $this->ensemblesatraiter = $ensembles;
		// printr($ensembles, "ENS A TRAITER");

	}

	public function action_reconstruire_champs_recherche_personnes()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_auteur = Atomik::get('session/'.IDAPPLI.'/user/id')+0;
		$this->appurl = Atomik::get('atomik/appurl');
		$this->ariane = get_fil_ariane(array(
			'fw3' => 'fw3'
		));
		printr($_POST, "POST");
		if (!isset($_POST['cmd']) || $_POST['cmd'] != "maj") {Atomik::redirect('fw3/reconstruire_champs_recherche_personnes'); return;}

		$id_ensemble = $_POST['ens']*1;

		$DB->beginTransaction();
		try
		{
			$sql = "SELECT id_personne, nom, prenom ";
			$sql .= "FROM personnes ";
			$sql .= "WHERE 1=1 ";
			if ($id_ensemble > 0)
			{
				$sql .= "AND id_ensemble=$id_ensemble ";
			}
			$ensembles = $DB->fetchAll($sql);

			$count = 0;
			foreach ($ensembles as $ens)
			{
				$count++;
				$id_personne = $ens['id_personne'];
				$n = $ens['prenom'].' '.$ens['nom'];
				$r = convert_string_pour_recherche($n);

				$champs = array();
				$champs['recherche'] = $r;
				$DB->update('personnes', $champs, "id_personne=$id_personne");

			}
			$DB->commit();
			$msg = "La mise à jour s'est déroulée correctement";
			if ($id_ensemble > 0)
			{
				$msg .= " pour l'ensemble $id_ensemble";
			}
			$msg .= ". $count fiches mises à jour.";
			Atomik::flash(array(1 => $msg), 'warning');
			Atomik::redirect('fw3/reconstruire_champs_recherche_personnes');
		}
		catch (Exception $e)
		{
			$msg = $e->getMessage();
			$errors = array(1 => "Un erreur s'est produite pendant la mise à jour de la base :<br />$msg<br />Op&eacute;ration annul&eacute;e.");
			Atomik::flash($errors, 'error');
			$DB->rollBack();

			// echo "<pre>";var_dump($e);echo "</pre>";
			Atomik::redirect('fw3/reconstruire_champs_recherche_personnes');
		}

	}
}
