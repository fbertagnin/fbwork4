<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description :
//
// ====================================================================

class SuperadminController
{
	public function __construct()
	{
		// ----------------------------------------------------------------
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific', "assets/css/appli/specific/".$this->controler.".css");
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array("superadmin" => "Administration")); // peut être surchargé dans une fonction
	}

	public function index()
	{
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
	}
	public function liste_comptes()
	{
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		global $DB;
		$sql = "SELECT ";
		$sql .= "comptes.id_compte AS id_compte, ";
		$sql .= "comptes.nom AS compte, ";
		$sql .= "DATE_FORMAT(comptes.date_creation, '%d/%m/%Y') AS date_creation, ";
		$sql .= "DATE_FORMAT(comptes.date_modif, '%d/%m/%Y') AS date_modif, ";
		$sql .= "DATE_FORMAT(comptes.date_dern_acces, '%d/%m/%Y') AS date_dern_acces, ";
		$sql .= "CONCAT(users.nom,' ',users.prenom) as user ";
		$sql .= "FROM comptes ";
		$sql .= "LEFT JOIN users ON users.id_user=comptes.id_auteur ";
		$sql .= "ORDER BY comptes.id_compte ";
		$stmt = $DB->query($sql);
		$result = $stmt->fetchAll();
		$this->table = $result;
	}
	public function superadministrateurs()
	{
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		global $DB;

		$sql = "SELECT users.*, ";
		$sql .= "DATE_FORMAT(users.date_creation, '%d/%m/%Y') as date_creation, ";
		$sql .= "DATE_FORMAT(users.date_modif, '%d/%m/%Y') as date_modif, ";
		$sql .= "comptes.nom as compte ";
		$sql .= "FROM users ";
		$sql .= "JOIN comptes ON comptes.id_compte=users.id_compte ";
		$sql .= "WHERE dr_webmaster = 1 ";
		$stmt = $DB->query($sql);
		$result = $stmt->fetchAll();
		$this->users = $result;
	}
	public function action_ajouter_superadministrateur()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		$id_user = $_GET['id_user'];
		$champs = array();
		$champs['dr_webmaster'] = 1;
		$DB->update('users', $champs, "id_user=$id_user");
		Atomik::redirect('superadmin/superadministrateurs');
	}

	public function supprimer_superadministrateur()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		$id_user = $_GET['id_user'];
		$champs = array();
		$champs['dr_webmaster'] = 0;
		$DB->update('users', $champs, "id_user=$id_user");
		Atomik::redirect('superadmin/superadministrateurs');
	}

	public function administrer_compte()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		$id_compte=0;
		if (isset($_GET['id_compte'])) $id_compte = $_GET['id_compte']+0;
		$sql = "SELECT nom, id_compte FROM comptes WHERE id_compte = ? ";
		$stmt = $DB->query($sql, $id_compte);
		$compte = $stmt->fetch();
		if (!$compte)
		{
			// aucun ensemble associé à l'utilisateur
			if ($_SESSION[IDAPPLI]['user']['droits']['dr_webmaster'] != 'oui')
			{
				$errors[] = "Opération non permise";
				Atomik::flash($errors, 'error');
				Atomik::redirect('login');
				return;
			}
			$errors[] = "Erreur : aucun compte ne correspond à l'id $id_compte";
			Atomik::flash($errors, 'error');
		}
		else
		{
			$_SESSION[IDAPPLI]['user']['id_compte'] = $compte['id_compte'];
			$_SESSION[IDAPPLI]['user']['nom_compte'] = $compte['nom'];
		}
		Atomik::redirect('/');

	}

	public function ajouter_compte()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$this->ariane = get_fil_ariane(array(
			'superadmin' => 'Gestion super-administrateur'
		));
		$fileini = Atomik::get('atomik/apppath').'/app/views/superadmin/ajouter_compte_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_compte;
		$form = new FBForms($configform);
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->form = $form->getform();
	}
	public function action_ajouter_compte()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_compte = 0;
		if (isset($_POST['id_compte'])) $id_compte = $_POST['id_compte'];
		global $DB;
		global $CONF;
		$fileini = Atomik::get('atomik/apppath').'/app/views/superadmin/ajouter_compte_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_compte;
		$form = new FBForms($configform);

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			foreach($errors as $k => $v)
			{
				// commenter si l'on veut que les saisies erronées soient reaffichées dans le formulaire
				// if (isset($data[$k])) unset($data[$k]);
			}
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash($errors, 'error');
			Atomik::redirect('superadmin/ajouter_compte');
			return;
		}
		// Validation des données OK
		// Vérification des droits de l'user
		if (Atomik::get('session/'.IDAPPLI.'/user/droits/webmaster') != 1)
		{
			$errors = array(1 => "Vous n'avez pas l'autorisation pour exécuter la commande demandée");
			Atomik::flash($errors, 'error');
			Atomik::redirect('index');
			return;
		}
		// Autorisations OK, enregistre le nouvel ensemble
		$data = $_POST;
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		$data['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id');
		$DB->insert('comptes', $data);
		$id_ensemble = $DB->lastInsertId();
		Atomik::redirect("superadmin/infos_compte?id_compte=$id_compte");

	}

	public function messages()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		global $DB;
		$id_compte = 0;
		$sql = "SELECT *, ";
		$sql .= "DATE_FORMAT(date_publication, '%d/%m/%Y') as date_publication, ";
		$sql .= "DATE_FORMAT(date_archivage, '%d/%m/%Y') as date_archivage ";
		$sql .= "FROM messages WHERE id_compte=$id_compte ";
		$stmt = $DB->query($sql);
		$result = $stmt->fetchAll();
		for($i=0; $i < count($result); $i++)
		{
			$r = '';
			if ($result[$i]['dest_user'] != 0) $r .= "utilisateurs ";
			if ($result[$i]['dest_admin'] != 0) $r .= "gestionnaires ";
			if ($result[$i]['dest_superadmin'] != 0) $r .= "administrateurs ";
			$result[$i]['destinataires'] = $r;
		}
		$this->table = $result;
	}
	public function action_supprimer_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		global $DB;
		$id_message = 0;
		if(isset($_GET['id_message']))	$id_message = $_GET['id_message'];
		$DB->delete("messages", "id_compte=0 AND id_message=$id_message");

		Atomik::redirect('superadmin/messages');
	}


	public function ajouter_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-administration-ajouter_message.css');
		$this->ariane = get_fil_ariane(array(
			'superadmin' => 'Administration',
			'superadmin/messages' => 'Gestion des messages'
		));
		global $DB;
		$fileini = Atomik::get('atomik/apppath').'/app/views/superadmin/ajouter_message_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_message;
		$form = new FBForms($configform);
		$_SESSION[IDAPPLI]['data_form']['date_publication'] = date("d/m/Y");
		$_SESSION[IDAPPLI]['data_form']['date_archivage'] = "";
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->appurl = Atomik::get('atomik/appurl');
		$this->form = $form->getform();
		/*if ($error)
		{
			$errors = array(1 => "Erreur...");
			Atomik::flash($errors, 'error');
			Atomik::redirect('personnes');
			return;
		\}*/

	}
	public function action_ajouter_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------

		global $DB;
		if (!isset($_POST['titre']) || $_POST['titre'] == '')
		{
			Atomik::flash(array('1' => "ERREUR : un titre est obligatoire"), 'error');
			Atomik::redirect('superadmin/ajouter_message');
			return;
		}
		$data = array();
		if (isset($_POST)) $data = $_POST;
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		if (isset($data['date_publication'])) $data['date_publication'] = convert_date_eu_to_mysql($data['date_publication']);
		if (isset($data['date_archivage'])) $data['date_archivage'] = convert_date_eu_to_mysql($data['date_archivage']);
		$data['id_compte'] = 0;
		$data['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id')+0;

		$data['dest_user'] = 0;
		$data['dest_admin'] = 0;
		if (isset($_POST['dest_user']) && $_POST['dest_user'] == 'oui') $data['dest_user'] = 1;
		if (isset($_POST['dest_admin']) && $_POST['dest_admin'] == 'oui') $data['dest_admin'] = 1;

		$data['dest_superadmin'] = 0;
		if (isset($_POST['dest_superadmin']) && $_POST['dest_superadmin'] == 'oui') $data['dest_superadmin'] = 1;
		// printr($data);
		$DB->insert("messages", $data);
		Atomik::redirect('superadmin/messages');
	}


	public function editer_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-administration-editer_message.css');
		$this->ariane = get_fil_ariane(array(
			'superadmin' => 'Administration',
			'superadmin/messages' => 'Gestion des messages'
		));
		$id_message = 0;
		if (isset($_GET['id_message'])) $id_message = $_GET['id_message'];

		global $DB;
		$sql = "SELECT *, ";
		$sql .= "DATE_FORMAT(date_publication, '%d/%m/%Y') as date_publication, ";
		$sql .= "DATE_FORMAT(date_archivage, '%d/%m/%Y') as date_archivage ";
		$sql .= "FROM messages \n";
		$sql .= "WHERE id_compte=0 \n";
		$sql .= "AND id_message = $id_message ";
		// printr($sql);
		$message = $DB->fetchRow($sql);
		if ($message)
		{
			$fileini = Atomik::get('atomik/apppath').'/app/views/superadmin/editer_message_form.ini';
			$configform = new Zend_Config_Ini($fileini, 'formulaires');
			$configform = $configform->editer_message;
			$form = new FBForms($configform);
			$form->setvaleurs($message);
			$this->form = $form->getform();
		}
		else
		{
			Atomik::flash(array('1' => "ERREUR : le message demandé n'a pas été trouvé"), 'error');
			Atomik::redirect('superadmin/messages');
		}

	}
	public function action_editer_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------

		$id_message = 0;
		if (isset($_POST['id_message'])) $id_message = $_POST['id_message'];
		if ($id_message <= 0)
		{
			return;
		}
		global $DB;
		$data = array();
		// printr($_POST);
		if (isset($_POST)) $data = $_POST;
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		// if (isset($data['id_message'])) unset($data['id_message']);
		if (isset($data['date_publication'])) $data['date_publication'] = convert_date_eu_to_mysql($data['date_publication']);
		if (isset($data['date_archivage'])) $data['date_archivage'] = convert_date_eu_to_mysql($data['date_archivage']);
		$data['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id')+0;

		$data['dest_user'] = 0;
		$data['dest_admin'] = 0;
		if (isset($_POST['dest_user']) && $_POST['dest_user'] == 'oui') $data['dest_user'] = 1;
		if (isset($_POST['dest_admin']) && $_POST['dest_admin'] == 'oui') $data['dest_admin'] = 1;

		$data['dest_superadmin'] = 0;
		if (isset($_POST['dest_superadmin']) && $_POST['dest_superadmin'] == 'oui') $data['dest_superadmin'] = 1;

		// printr($data, "DATA");
		$DB->update("messages", $data, "id_message=$id_message AND id_compte=0");
		Atomik::redirect('superadmin/messages');
	}


}
