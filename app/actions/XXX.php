<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : 
//
// ====================================================================


class XXXController
{
	public function __construct()
	{
		$this->id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		$this->id_user=Atomik::get('session/'.IDAPPLI.'/user/id');
		if ($this->id_compte == 0) {erreur_inattendue("ERREUR : le compte n'a pas pu être identifié");return;}
		// ----------------------------------------------------------------
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler', "assets/css/appli/specific/".$this->controler.".css");
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler."-".$this->view.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler-view', "assets/css/appli/specific/".$this->controler."-".$this->view.".css");
        // Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        // Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array("XXX" => "XXX")); // peut être surchargé dans une fonction
	}
	
	public function index()
	{
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		$this->ariane = get_fil_ariane(array());
	}
}
