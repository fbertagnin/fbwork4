<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Outils et fonctions diverses.
//
//=====================================================================

class doc_pdfController
{
	public function __construct()
	{
		$this->id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		$this->id_user=Atomik::get('session/'.IDAPPLI.'/user/id');
		if ($this->id_compte == 0) {erreur_inattendue("ERREUR : le compte n'a pas pu être identifié");return;}
		// ----------------------------------------------------------------
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler', "assets/css/appli/specific/".$this->controler.".css");
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler."-".$this->view.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler-view', "assets/css/appli/specific/".$this->controler."-".$this->view.".css");
        // Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        // Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array()); // peut être surchargé dans une fonction
	}


	public function index()
	{
		global $DB;
		global $CONF;
		$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
		$path = $CONF->doc_pdf->dossier;
		$this->titre = $CONF->doc_pdf->titre;
		if (isset($_GET['dossier']))
		{
			$path = base64_decode($_GET['dossier']);
		}


		// Composition du fil d'ariane selon l'arborescence du dossier de doc
		$dossiers = explode("/", $path);
		// les premiers deux éléments du tableau sont à la racine de la doc
		// les suivants décrivent l'arborescence
		if (count($dossiers) < 3)
		{
			$this->ariane = get_fil_ariane(array());
		}
		else
		{
			$n = '';
			$aa = array();
			$aa["doc_pdf"] = $CONF->doc_pdf->titre;
			$ad = $CONF->doc_pdf->dossier;
			for($i=2; $i < count($dossiers)-1; $i++)
			{
				$n = $dossiers[$i];
				$n = utf8_encode($n);
				$n = encodehtml($n);
				$ad .= "/".$dossiers[$i];
				$a = "doc_pdf?dossier=".base64_encode($ad);
				$aa[$a] = $n;
			}
			$this->ariane = get_fil_ariane($aa);

			// Modification du titre
			$n = $dossiers[count($dossiers)-1];
			$n = utf8_encode($n);
			$n = encodehtml($n);
			if ($n != '') $this->titre .= " : ".$n;
		}

		$this->html = array();
		$this->fichiers = array();
		$this->dossiers = array();
		$dirname = Atomik::get('atomik/apppath')."/$path";
		if (is_dir($dirname))
		{
			$dir = opendir($path);
			while($file = readdir($dir)) {
				if($file != '.' && $file != '..' && substr($file, 0,1) != '.')
				{
					// printr($dirname.$file, "FILE");
					if (is_dir($dirname.'/'.$file))
					{
						$f = array();
						$f['titre'] = $file;
						$f['titre'] = utf8_encode($f['titre']);
						$f['titre'] = encodehtml($f['titre']);
						$f['encode'] = base64_encode($path."/".$file);
						$this->dossiers[] = $f;
					}
					else
					{
						$path_info_parts = pathinfo($dirname."/".$file);
						// printr($path_info_parts);
						$ext = '';
						if (isset($path_info_parts['extension'])) $ext = trim($path_info_parts['extension']);
						$filename = trim($path_info_parts['filename']);
						if ($ext == 'html')
						{
							$fp = file_get_contents($path."/$filename.html");
							$this->html[$filename] = $fp;
						}
						if ($ext == 'pdf' || $ext == 'doc' || $ext == 'docx' || $ext == 'xls' || $ext == 'xlsx')
						{
							$f = array();
							$f['titre'] = $filename;
							if (is_file($path."/$filename.txt"))
							{
								$fp = file_get_contents($path."/$filename.txt");
								$aa = explode("\n", $fp);
								if (count($aa) > 0)
								{
									$f['titre'] = $aa[0];
									$f['titre'] = utf8_encode($f['titre']);
									$f['titre'] = encodehtml($f['titre']);
									$f['description'] = '';
									for($i=1; $i<count($aa); $i++)
									{
										$aa[$i] = utf8_encode($aa[$i]);
										$aa[$i] = encodehtml($aa[$i]);
										$f['description'] .= "<p>".$aa[$i]."</p>";
									}
								}
							}
							$f['fichier'] = $path."/".$file;
							$this->fichiers[$file] = $f;
						}
					}
				}
			}
		}
	}
}
