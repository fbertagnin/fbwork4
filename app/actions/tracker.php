<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Outils et fonctions diverses.
//
// ====================================================================

class trackerController
{
	public function __construct()
	{
		$this->id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		$this->id_user=Atomik::get('session/'.IDAPPLI.'/user/id');
		if ($this->id_compte == 0) {erreur_inattendue("ERREUR : le compte n'a pas pu être identifié");return;}
		// ----------------------------------------------------------------
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler', "assets/css/appli/specific/".$this->controler.".css");
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler."-".$this->view.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler-view', "assets/css/appli/specific/".$this->controler."-".$this->view.".css");
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array("tracker" => "Tracker")); // peut être surchargé dans une fonction
	}
		 

	public function index()
	{
		global $DB;
		
		$this->affstatut = "";
		if (isset($_POST['affstatut'])) $this->affstatut = $_POST['affstatut'];
		// printr($_POST);
		
		$sql = "SELECT *, ";
		$sql .= "DATE_FORMAT(date_creation, '%d/%m/%Y') as date_creation, ";
		$sql .= "DATE_FORMAT(date_modif, '%d/%m/%Y') as date_modif ";
		$sql .= "FROM tracker ";
		if ($this->affstatut != "")
		{
			$sql .= "WHERE statut = '".$this->affstatut."' ";
		}
		$stmt = $DB->query($sql);
		$result = $stmt->fetchAll();
		$this->table = $result;
		$this->appurl = Atomik::get('atomik/appurl');
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
	}


	public function ajouter_ticket()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
		global $DB;
		$fileini = Atomik::get('atomik/apppath').'/app/views/tracker/ajouter_ticket_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_ticket;
		$form = new FBForms($configform);
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->appurl = Atomik::get('atomik/appurl');
		$this->form = $form->getform();
		$this->texte = "Page en construction";
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
	}
	public function action_ajouter_ticket()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
		$id_auteur=Atomik::get('session/'.IDAPPLI.'/user/id')+0;
		global $DB;

		if (!isset($_POST['cmd']) || ($_POST['cmd'] != 'set' && $_POST['cmd'] != 'add')) 
		{
			$errors = array(1 => "Données du formulaire non correctes");
			Atomik::flash($errors, 'error');
			Atomik::redirect('tracker');
			return;			
		}

		$fileini = Atomik::get('atomik/apppath').'/app/views/tracker/ajouter_ticket_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_ticket;
		$form = new FBForms($configform);

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			foreach($errors as $k => $v)
			{
				// commenter si l'on veut que les saisies erronées soient reaffichées dans le formulaire
				// if (isset($data[$k])) unset($data[$k]);
			}
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash($errors, 'error');
			Atomik::redirect("tracker/ajouter_ticket");
			return;
		}

		$data = $_POST;
		// $data = encodehtml($data);

		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		$data['id_auteur'] = $id_auteur;
		if (isset($_SESSION[IDAPPLI]['data_form'])) unset($_SESSION[IDAPPLI]['data_form']);
		$DB->insert('tracker', $data);
		$id_ticket = $DB->lastInsertId();		
		Atomik::redirect("tracker");	

	}


	public function editer_ticket()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		global $DB;
	
		$id_ticket = 0;
		if (isset($_GET['id_ticket'])) $id_ticket = $_GET['id_ticket'];

		$sql = "SELECT *,  ";
		$sql .= "DATE_FORMAT(date_creation, '%d/%m/%Y') as date_creation, ";
		$sql .= "DATE_FORMAT(date_modif, '%d/%m/%Y') as date_modif ";
		$sql .= "FROM tracker ";
		$sql .= "WHERE id_tracker=$id_ticket ";
		$ticket = $DB->fetchRow($sql);
		if ($ticket)
		{
			unset($_SESSION[IDAPPLI]['data_form']);
			$fileini = Atomik::get('atomik/apppath').'/app/views/tracker/editer_ticket_form.ini';
			$configform = new Zend_Config_Ini($fileini, 'formulaires');
			$configform = $configform->editer_ticket;
			$form = new FBForms($configform);
			if ($ticket['type'] == 'Amélioration') $ticket['type1'] = 'oui';
			if ($ticket['type'] == 'Correction') $ticket['type2'] = 'oui';
			if ($ticket['type'] == 'Documentation') $ticket['type3'] = 'oui';
			if ($ticket['type'] == 'Données') $ticket['type4'] = 'oui';
			if ($ticket['priorite'] == 'Bloquant') $ticket['priorite1'] = 'oui';
			if ($ticket['priorite'] == 'Urgent') $ticket['priorite2'] = 'oui';
			if ($ticket['priorite'] == 'Normal') $ticket['priorite3'] = 'oui';
			if ($ticket['priorite'] == 'Faible') $ticket['priorite4'] = 'oui';
			if ($ticket['statut'] == 'Proposé') $ticket['statut1'] = 'oui';
			if ($ticket['statut'] == 'Validé') $ticket['statut2'] = 'oui';
			if ($ticket['statut'] == 'Terminé') $ticket['statut3'] = 'oui';
			if ($ticket['statut'] == 'Réfusé') $ticket['statut4'] = 'oui';
			if ($ticket['statut'] == 'Reporté') $ticket['statut5'] = 'oui';
			// printr($ticket);
			$form->setvaleurs($ticket);
			$this->form = $form->getform();
			// printr($ticket);
			
			$this->fichiers = array();
			$dirname = Atomik::get('atomik/apppath')."/assets/upload/tracker/$id_ticket";
			if (is_dir($dirname))
			{
				$dir = opendir($dirname); 
				while($file = readdir($dir)) {
					if($file != '.' && $file != '..' && !is_dir($dirname.$file))
					{
						$path_info_parts = pathinfo($dirname."/".$file);
						// printr($path_info_parts);
						$ext = '';
						if (isset($path_info_parts['extension'])) $ext = trim($path_info_parts['extension']);
						$filename = trim($path_info_parts['filename']);
						$this->fichiers[$file] = base64_decode($filename);
					}
				}
			}
			$this->id_ticket = $id_ticket;
			$this->urlfiles = Atomik::get('atomik/appurl')."/assets/upload/tracker/$id_ticket";
			$this->ariane = get_fil_ariane(array("tracker" => "Tracker", "tracker/afficher_ticket?id_ticket=$id_ticket" => "Ticket $id_ticket")); 
			
		}
		else
		{
			$errors = array(1 => "Aucun ticket ne correspond aux informations fournies");
			Atomik::flash($errors, 'error');
			Atomik::redirect('tracker');
			return;			
		}
	}
	public function action_editer_ticket()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
		$id_auteur=Atomik::get('session/'.IDAPPLI.'/user/id')+0;
		global $DB;

		if (!isset($_POST['cmd']) || ($_POST['cmd'] != 'set' && $_POST['cmd'] != 'upd')) 
		{
			$errors = array(1 => "Données du formulaire non correctes");
			Atomik::flash($errors, 'error');
			Atomik::redirect('tracker');
			return;			
		}
		if (!isset($_POST['id_tracker'])) 
		{
			$errors = array(1 => "Données du formulaire non correctes");
			Atomik::flash($errors, 'error');
			Atomik::redirect('tracker');
			return;			
		}
		$id_ticket = $_POST['id_tracker']+0;

		$fileini = Atomik::get('atomik/apppath').'/app/views/tracker/editer_ticket_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->editer_ticket;
		$form = new FBForms($configform);

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			foreach($errors as $k => $v)
			{
				// commenter si l'on veut que les saisies erronées soient reaffichées dans le formulaire
				// if (isset($data[$k])) unset($data[$k]);
			}
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash($errors, 'error');
			Atomik::redirect("tracker/editer_ticket?id_ticket=$id_ticket");
			return;
		}

		$data = $_POST;
		// $data = encodehtml($data);

		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		if (isset($data['id_ticket'])) unset($data['id_ticket']);
		if (isset($data['id_tracker'])) unset($data['id_tracker']);
		$data['id_modif'] = $id_auteur;
		if (isset($_SESSION[IDAPPLI]['data_form'])) unset($_SESSION[IDAPPLI]['data_form']);
		$DB->update('tracker', $data,  "id_tracker=$id_ticket");
		$id_ticket = $DB->lastInsertId();		
		Atomik::redirect("tracker");	

	}


	public function afficher_ticket()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		global $DB;
	
		$id_ticket = 0;
		if (isset($_GET['id_ticket'])) $id_ticket = $_GET['id_ticket'];

		$this->ticket = array();
		$sql = "SELECT *,  ";
		$sql .= "DATE_FORMAT(date_creation, '%d/%m/%Y') as date_creation, ";
		$sql .= "DATE_FORMAT(date_modif, '%d/%m/%Y') as date_modif ";
		$sql .= "FROM tracker ";
		$sql .= "WHERE id_tracker=$id_ticket ";
		$ticket = $DB->fetchRow($sql);
		if ($ticket)
		{
			$this->ticket = $ticket;
			$this->titre = $ticket['titre'];
		}

		$this->nom_creation = '';
		$sql = "SELECT nom, prenom, login  ";
		$sql .= "FROM users ";
		$sql .= "WHERE id_user=".$this->ticket['id_auteur'];
		$pers = $DB->fetchRow($sql);
		if ($pers)
		{
			$this->nom_creation = $pers['prenom'].' '.$pers['nom'];
		}
		$this->nom_modif = '';
		$sql = "SELECT nom, prenom, login  ";
		$sql .= "FROM users ";
		$sql .= "WHERE id_user=".$this->ticket['id_modif'];
		$pers = $DB->fetchRow($sql);
		if ($pers)
		{
			$this->nom_modif = $pers['prenom'].' '.$pers['nom'];
		}

		
		$this->fichiers = array();
		$dirname = Atomik::get('atomik/apppath')."/assets/upload/tracker/$id_ticket";
		if (is_dir($dirname))
		{
			$dir = opendir($dirname); 
			while($file = readdir($dir)) {
				if($file != '.' && $file != '..' && !is_dir($dirname.$file))
				{
					$path_info_parts = pathinfo($dirname."/".$file);
					// printr($path_info_parts);
					$ext = '';
					if (isset($path_info_parts['extension'])) $ext = trim($path_info_parts['extension']);
					$filename = trim($path_info_parts['filename']);
					$this->fichiers[$file] = base64_decode($filename);
				}
			}
		}

		$this->id_ticket = $id_ticket;
		$this->urlfiles = Atomik::get('atomik/appurl')."/assets/upload/tracker/$id_ticket";
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
	}
	
	public function action_ajouter_fichier()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		$NOM = '';
		$id_ticket = 0;
		if(isset($_POST['id_ticket'])) $id_ticket=$_POST['id_ticket']+0;
		if ($id_ticket <= 0) return;
		$nom = '';
		if(isset($_POST['nom'])) $nom=$_POST['nom'];
		
		if (!isset($_FILES["file"]))
		{
			$errors = array(0 => "Aucun fichier n'a été envoyé");
			Atomik::flash($errors, 'error');
			Atomik::redirect('tracker/editer_ticket?id_ticket='.$id_ticket);
			return;
		}
		// printr($_FILES);
		// return;
		if ($_FILES["file"]["error"] > 0)
		{
			$errors = array(0 => 'Erreur '.$_FILES["file"]["error"].' dans le chargement du fichier');
			Atomik::flash($errors, "error");
			Atomik::redirect('tracker/editer_ticket?id_ticket='.$id_ticket);
			return;
		}
		
		$path_info_parts = pathinfo($_FILES["file"]['name']);
		$ext = trim($path_info_parts['extension']);
		
		if ($nom != '') 
		{
			$NOM="$nom";
		}
		else
		{
			$NOM = "$path_info_parts[filename]";
		}
		$NOM = encodestringhtml($NOM);
		$NOM = base64_encode($NOM).".$ext";

		// printr($_FILES["file"], "FILE");
		// printr($path_info_parts);
		// printr($NOM, "NOM");
		
		$file = Atomik::get('atomik/apppath')."/assets/upload/tracker/$id_ticket";
		if (!is_dir($file)) mkdir($file);

		$file = Atomik::get('atomik/apppath')."/assets/upload/tracker/$id_ticket/$NOM";
		move_uploaded_file($_FILES["file"]["tmp_name"], $file);
		Atomik::redirect('tracker/editer_ticket?id_ticket='.$id_ticket);
		
	}

	public function action_supprimer_fichier()
	{
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		$id_ticket = 0;
		if(isset($_GET['id_ticket'])) $id_ticket=$_GET['id_ticket']+0;
		$dirname = Atomik::get('atomik/apppath')."/assets/upload/tracker/$id_ticket";
		$file = "";
		if(isset($_GET['file'])) $file=$_GET['file'];
		$file = "$dirname/$file";
		if (file_exists("$file"))
		{
			unlink("$file");
		}
		// printr($file, "FILE"); return;
		Atomik::redirect('tracker/editer_ticket?id_ticket='.$id_ticket);
	}

	
	public function action_supprimer_ticket()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('webmaster')) {Atomik::redirect('fw3/non_autorise'); return;}
		$id_ticket = 0;
		if(isset($_GET['id_ticket'])) $id_ticket=$_GET['id_ticket']+0;
		if ($id_ticket <= 0) return;
		
		$DB->delete("tracker", "id_tracker=$id_ticket");
		Atomik::redirect('tracker'	);
	}

}
