<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description :
//
// ====================================================================


class OutilsController
{
	public function __construct()
	{
		$this->id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		$this->id_user=Atomik::get('session/'.IDAPPLI.'/user/id');
		if ($this->id_compte == 0) {erreur_inattendue("ERREUR : le compte n'a pas pu être identifié");return;}
		// ----------------------------------------------------------------
		$this->controler = Atomik::get('page/controleur');
		$this->view = Atomik::get('page/view');
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler', "assets/css/appli/specific/".$this->controler.".css");
		$css_file = Atomik::get('atomik/apppath')."/assets/css/appli/specific/".$this->controler."-".$this->view.".css";
		if (is_file($css_file)) Atomik::set('styles/specific-controler-view', "assets/css/appli/specific/".$this->controler."-".$this->view.".css");
        // Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        // Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		$this->appurl = Atomik::get('atomik/appurl');
		$this->apppath = Atomik::get('atomik/apppath');
		$this->ariane = get_fil_ariane(array("outils" => "Outils")); // peut être surchargé dans une fonction
	}

	public function index()
	{
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		$this->ariane = get_fil_ariane(array());
	}
	public function infos_compte()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		$id_compte=Atomik::get('session/'.IDAPPLI.'/user/id_compte')+0;
		if (isset($_GET['id_compte'])) $id_compte = $_GET['id_compte'];

		$sql = 'SELECT comptes.*, users.nom as usernom, users.prenom as userprenom, users.id_user as id_user, ';
		$sql .= 'users.actif, users.dr_webmaster ';
		$sql .= 'FROM comptes ';
		$sql .= 'JOIN users ON comptes.id_auteur=users.id_user ';
		$sql .= 'WHERE comptes.id_compte=? ';
		$stmt = $DB->query($sql, array($id_compte));
		$compte = $stmt->fetch();
		if (!$compte)
		{
			$errors = array(1 => "Aucun compte ne correspond aux informations fournies");
			Atomik::flash($errors, 'error');
			Atomik::redirect('superadmin');
			return;
		}
		$compte['date_modif'] = convert_mysql_format_to_date($compte['date_modif']);
		$compte['date_creation'] = convert_mysql_format_to_date($compte['date_creation']);
		$compte['date_dern_acces'] = convert_mysql_format_to_date($compte['date_dern_acces']);
		$this->baseurl = Atomik::get('atomik/appurl');
		$this->compte = encodehtml($compte);
		$this->id_compte = $compte['id_compte'];


		$sql = "SELECT id_user, login, nom, prenom, actif, dr_webmaster ";
		$sql .= "FROM users WHERE id_compte = $id_compte ";
		$stmt = $DB->query($sql, array($id_compte));
		$users = $stmt->fetchAll();
		if (!$users)
		{
			$errors = array(1 => "Attention : aucun gestionnaire n'est défini pour ce compte");
			Atomik::flash($errors, 'error');
		}
		// echo "<pre>"; print_r($users); exit;
		$this->users = $users;

	}

	public function users()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');

		global $DB;
		$sql = "SELECT *, ";
		$sql .= "DATE_FORMAT(date_creation, '%d/%m/%Y') as date_creation, ";
		$sql .= "DATE_FORMAT(date_modif, '%d/%m/%Y') as date_modif, ";
		$sql .= "DATE_FORMAT(date_connexion, '%d/%m/%Y %H:%i') as date_connexion ";
		$sql .= "FROM users ";
		$sql .= "WHERE id_compte = $this->id_compte ";
		$stmt = $DB->query($sql);
		$result = $stmt->fetchAll();
		$this->table = $result;
		$this->appurl = Atomik::get('atomik/appurl');
	}

	public function ajouter_utilisateur()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
        // Atomik::set('styles/specific1', 'assets/css/appli/specif-gestion_ensembles-liste_ensembles.css');
        // Atomik::set('styles/specific2', 'assets/css/appli/specif-outils-edit_utilisateur.css');
        // Atomik::set('styles/onglets', 'assets/css/appli/onglets.css');
		$this->ariane = get_fil_ariane(array(
			"outils" => "Outils",
			'outils/users' => 'Gestion utilisateurs'
		));



		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/ajouter_utilisateur_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_utilisateur;
		$form = new FBForms($configform);
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		if (isset($_SESSION[IDAPPLI]['data_form'])) unset($_SESSION[IDAPPLI]['data_form']);
		$this->form = $form->getform();
	}

	public function action_ajouter_utilisateur()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		// printr($_POST, "POST");
		if (!isset($_POST['cmd']) || ($_POST['cmd'] != 'set' && $_POST['cmd'] != 'add'))
		{
			$errors = array(1 => "Données du formulaire non correctes");
			Atomik::flash($errors, 'error');
			Atomik::redirect('outils/utilisateurs');
			return;
		}

		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/ajouter_utilisateur_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_utilisateur;
		$form = new FBForms($configform);

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			foreach($errors as $k => $v)
			{
				// commenter si l'on veut que les saisies erronées soient reaffichées dans le formulaire
				// if (isset($data[$k])) unset($data[$k]);
			}
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash($errors, 'error');
			Atomik::redirect("outils/ajouter_utilisateur");
			return;
		}

		// vérification que l'user n'existe pas
		$data = $_POST;
		$login = $_POST['login'];
		$sql = "SELECT login ";
		$sql .= "FROM users ";
		$sql .= "WHERE login = '$login' ";
		$user = $DB->fetchRow($sql);
		if (isset($user['login']) && $user['login'] == $login)
		{
			if (isset($data['login'])) unset($data['login']);
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash(array(1 => "L'utilisateur '$login' existe d&eacute;j&agrave;"), 'error');
			Atomik::redirect("outils/ajouter_utilisateur");
			return;
		}

		$data['id_compte'] = $this->id_compte;
		$crypt = sha1($data['passwd']);

		$data['passwd'] = $crypt;
		$data['dr_01'] = 'lec';
		$data['dr_02'] = 'lec';
		$data['dr_03'] = 'lec';
		$data['dr_04'] = 'lec';
		$data['dr_05'] = 'lec';
		$data['dr_06'] = 'lec';
		$data['dr_07'] = 'no';
		$data['dr_08'] = 'no';
		$data['dr_09'] = 'no';
		$data['dr_10'] = 'no';
		// $data = encodehtml($data);

		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		$data['id_auteur'] = $this->id_user;
		if (isset($_SESSION[IDAPPLI]['data_form'])) unset($_SESSION[IDAPPLI]['data_form']);
		$DB->insert('users', $data);
		$id_user = $DB->lastInsertId();
		Atomik::redirect("outils/edit_utilisateur?id_user=$id_user");
	}

	public function edit_utilisateur()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
        Atomik::set('styles/onglets', 'assets/css/appli/onglets.css');
		/*
        Atomik::set('styles/specific1', 'assets/css/appli/specif-gestion_ensembles-liste_ensembles.css');
        Atomik::set('styles/specific2', 'assets/css/appli/specif-outils-edit_utilisateur.css');
        Atomik::set('styles/onglets', 'assets/css/appli/onglets.css');
        */
        Atomik::set('scripts/datepick', 'assets/js/jquery.tools/jquery.tools.datepick.min.js');
		$this->ariane = get_fil_ariane(array(
			"outils" => "Outils",
			'outils/users' => 'Gestion utilisateurs'
		));

		global $DB;
		global $CONF;

		$id_user=0;
		if (isset($_GET['id_user'])) $id_user = $_GET['id_user']+0;
		if ($id_user <= 0)
		{
			$errors = array(1 => "le paramètre d'identification de l'utilisateur n'est pas valide");
			Atomik::flash($errors, 'error');
			Atomik::redirect('outils/users');
			return;
		}



		$this->onglet1 = 'Utilisateur';
		// ----------------- informations de base sur la personne ----------------
		$sql = "SELECT users.*,  ";
		$sql .= "DATE_FORMAT(users.date_creation, '%d/%m/%Y') as date_creation, ";
		$sql .= "DATE_FORMAT(users.date_modif, '%d/%m/%Y') as date_modif ";
		$sql .= "FROM users ";
		$sql .= "WHERE users.id_user=$id_user AND users.id_compte=$this->id_compte";
		$user = $DB->fetchRow($sql);

		if (!$user)
		{
			$errors = array(1 => "Aucun utilisateur ne correspond aux informations fournies");
			Atomik::flash($errors, 'error');
			Atomik::redirect('outils/users');
			return;
		}
		$sql = "SELECT users.*  ";
		$sql .= "FROM users ";
		$sql .= "WHERE users.id_user=".$user['id_auteur'];
		$auteur = $DB->fetchRow($sql);





		$user['p'] = $user['passwd'];
		unset($user['passwd']);
		$user['date_validite_debut'] = convert_mysql_format_to_date($user['date_validite_debut']);
		$user['date_validite_fin'] = convert_mysql_format_to_date($user['date_validite_fin']);
		if ($user['date_validite_debut'] == "00/00/0000") unset($user['date_validite_debut']);
		if ($user['date_validite_fin'] == "00/00/0000") unset($user['date_validite_fin']);
		if(isset($auteur['nom'])) $user['auteur'] = $auteur['prenom'].' '.$auteur['nom'];

		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/edit_utilisateur_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->edit_utilisateur;
		$form = new FBForms($configform);
		$form->setvaleurs($user);
		$this->form = $form->getform();
		$this->user = $user;
		$this->onglet1 = $user['prenom'].' '.$user['nom'];
		$this->droits = $CONF->droits->definition->droit;
		// printr($this->droits);
	}

	public function action_edit_utilisateur()
	{
		// Cette fonction traite à la fois la modification des paramètres personnels
		// que celle des droits d'accès de l'utilisateur
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		$id_auteur=$this->id_user;
		$id_user=0;
		if (isset($_POST['id_user'])) $id_user = $_POST['id_user']+0;
		if ($id_user <= 0)
		{
			$errors = array(1 => "Tentative de mise à jour d'une donnée iniexistante");
			Atomik::flash($errors, 'error');
			Atomik::redirect('outils/utilisateurs');
			return;
		}
		// printr($_POST, "POST");
		if (!isset($_POST['cmd']) || ($_POST['cmd'] != 'upd' && $_POST['cmd'] != 'upddr'))
		{
			$errors = array(1 => "Données du formulaire non correctes");
			Atomik::flash($errors, 'error');
			Atomik::redirect('outils/users');
			return;
		}

		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/edit_utilisateur_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->edit_utilisateur;
		$form = new FBForms($configform);

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			foreach($errors as $k => $v)
			{
				// commenter si l'on veut que les saisies erronées soient reaffichées dans le formulaire
				// if (isset($data[$k])) unset($data[$k]);
			}
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash($errors, 'error');
			Atomik::redirect("outils/edit_utilisateur?id_user=$id_user");
			return;
		}
		if (isset($_POST['passwd']) && strlen($_POST['passwd']) > 0 && strlen($_POST['passwd']) < 6)
		{
			$data = $_POST;
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			$_SESSION[IDAPPLI]['data_form'] = $data;
			Atomik::flash(array(1=>"Erreur mot de passe trop court : 6 caractères minimum"), 'error');
			Atomik::redirect("outils/edit_utilisateur?id_user=$id_user");
			return;
		}

		$data = $_POST;
		if (!isset($data['dr_admin'])) $data['dr_admin'] = 'no';
		if (!isset($data['actif'])) $data['actif'] = 'no';
		$data = encodehtml($data);
		if (isset($data['passwd']) && $data['passwd'] == '') unset($data['passwd']);
		if (isset($data['passwd']) && $data['passwd'] != '') $data['passwd'] = sha1($data['passwd']);
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['login'])) unset($data['login']);
		if (isset($data['submit'])) unset($data['submit']);
		if (isset($data['date_validite_debut']) && $data['date_validite_debut'] != "") $data['date_validite_debut'] = convert_date_eu_to_mysql($data['date_validite_debut']);
		if (isset($data['date_validite_fin']) && $data['date_validite_fin'] != "") $data['date_validite_fin'] = convert_date_eu_to_mysql($data['date_validite_fin']);

		$data['id_auteur'] = $id_auteur;
		if (isset($_SESSION[IDAPPLI]['data_form'])) unset($_SESSION[IDAPPLI]['data_form']);
		$DB->update('users', $data, "id_user=$id_user");
		Atomik::redirect("outils/edit_utilisateur?id_user=$id_user");
	}
	public function action_supprimer_user()
	{
		global $DB;
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		$id_user = 0;
		if (isset($_GET['id_user'])) $id_user = $_GET['id_user'];

		$sql = "SELECT id_user ";
		$sql .= "FROM users ";
		$sql .= "WHERE id_compte=$this->id_compte ";
		$sql .= "AND id_user=$id_user ";
		$user = $DB->fetchRow($sql);
		if (!$user) {erreur_inattendue("ERREUR : l'user $id_user n'a pas pu être identifié");return;}


		try
		{
			$DB->beginTransaction();

			$DB->delete('users', "id_compte=$this->id_compte AND id_user=$id_user");
			$DB->delete('parametres_user', "id_compte=$this->id_compte AND id_user=$id_user");
			$DB->commit();
		}
		catch (Exception $e)
		{
			Atomik::flash(array(1=>"ERREUR TECHNIQUE. Signalez-la à l'administrateur. [".basename (__FILE__).":".__LINE__."] Code:".$e->getCode()." ".$e->getMessage()), 'error');
			$DB->rollBack();
		}
		Atomik::redirect("outils/users");
	}

	public function editer_compte()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;

		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/editer_compte_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->editer_compte;

		$form = new FBForms($configform);

		$sql = 'SELECT comptes.*';
		$sql .= 'FROM comptes ';
		$sql .= 'WHERE id_compte=? ';
		$stmt = $DB->query($sql, array($this->id_compte));
		$compte = $stmt->fetch();
		if (!$compte)
		{
			$errors = array(1 => "Aucun compte ne correspond aux informations fournies");
			Atomik::flash($errors, 'error');
			Atomik::redirect('/');
			return;
		}
		$form->setvaleurs($compte);
		$this->form = $form->getform();
		$this->nom_compte = $compte['nom'];

		$this->ariane = get_fil_ariane(array(
			"outils" => "Outils",
			'outils/infos_compte?id_compte='.$this->id_compte => $compte['nom']
		));

	}

	public function action_editer_compte()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		global $DB;
		global $CONF;
		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/editer_compte_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->editer_compte;
		$form = new FBForms($configform);

		$errors = $form->verifreponse($_POST);
		if (is_array($errors))
		{
			$data = $_POST;
			// Ne pas reafficher les valeurs en erreur
			foreach($errors as $k => $v)
			{
				// commenter si l'on veut que les saisies erronées soient reaffichées dans le formulaire
				// if (isset($data[$k])) unset($data[$k]);
			}
			if (isset($data['cmd'])) unset($data['cmd']);
			if (isset($data['submit'])) unset($data['submit']);
			// $_SESSION[IDAPPLI]['data_form'] = $data; // mieux vaut de reprendre les données de la base
			Atomik::flash($errors, 'error');
			Atomik::redirect('outils/editer_compte');
			return;
		}

		// Autorisations OK, enregistre le nouvel ensemble
		$data = $_POST;
		// $id_ensemble = $data['id_ensemble'];
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		$data['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id');
		$DB->update('comptes', $data, "id_compte=".$this->id_compte);
		Atomik::redirect("outils/infos_compte?id_compte=".$this->id_compte);
	}

	public function messages()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
        Atomik::set('scripts/datatab', 'assets/libs/DataTables-1.9.0/media/js/jquery.dataTables.min.js');
        Atomik::set('styles/datatab', 'assets/libs/DataTables-1.9.0/media/css/gestpar_table_1.css');
		// ----------------------------------------------------------------
		global $DB;
		$id_compte = $this->id_compte;
		$sql = "SELECT *, ";
		$sql .= "DATE_FORMAT(date_publication, '%d/%m/%Y') as date_publication, ";
		$sql .= "DATE_FORMAT(date_archivage, '%d/%m/%Y') as date_archivage ";
		$sql .= "FROM messages WHERE id_compte=$id_compte ";
		$stmt = $DB->query($sql);
		$result = $stmt->fetchAll();
		for($i=0; $i < count($result); $i++)
		{
			$r = '';
			if ($result[$i]['dest_user'] != 0) $r .= "utilisateurs ";
			if ($result[$i]['dest_admin'] != 0) $r .= "gestionnaires ";
			if ($result[$i]['dest_superadmin'] != 0) $r .= "administrateurs ";
			$result[$i]['destinataires'] = $r;
		}
		$this->table = $result;
		$this->appurl = Atomik::get('atomik/appurl');

	}
	public function action_supprimer_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		global $DB;
		$id_message = 0;
		if(isset($_GET['id_message']))	$id_message = $_GET['id_message'];
		$DB->delete("messages", "id_compte=$this->id_compte AND id_message=$id_message");

		Atomik::redirect('outils/messages');
	}


	public function ajouter_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-administration-ajouter_message.css');
		$this->ariane = get_fil_ariane(array(
			'outils' => 'Outils',
			'administration/messages' => 'Gestion des messages'
		));
		global $DB;
		$fileini = Atomik::get('atomik/apppath').'/app/views/outils/ajouter_message_form.ini';
		$configform = new Zend_Config_Ini($fileini, 'formulaires');
		$configform = $configform->ajouter_message;
		$form = new FBForms($configform);
		$_SESSION[IDAPPLI]['data_form']['date_publication'] = date("d/m/Y");
		$_SESSION[IDAPPLI]['data_form']['date_archivage'] = "";
		if (isset($_SESSION[IDAPPLI]['data_form'])) $form->setvaleurs($_SESSION[IDAPPLI]['data_form']);
		unset($_SESSION[IDAPPLI]['data_form']);
		$this->appurl = Atomik::get('atomik/appurl');
		$this->form = $form->getform();
		/*if ($error)
		{
			$errors = array(1 => "Erreur...");
			Atomik::flash($errors, 'error');
			Atomik::redirect('personnes');
			return;
		\}*/
	}
	public function action_ajouter_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------

		global $DB;
		if (!isset($_POST['titre']) || $_POST['titre'] == '')
		{
			Atomik::flash(array('1' => "ERREUR : un titre est obligatoire"), 'error');
			Atomik::redirect('outils/ajouter_message');
			return;
		}
		$data = array();
		if (isset($_POST)) $data = $_POST;
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		if (isset($data['date_publication'])) $data['date_publication'] = convert_date_eu_to_mysql($data['date_publication']);
		if (isset($data['date_archivage'])) $data['date_archivage'] = convert_date_eu_to_mysql($data['date_archivage']);
		$data['id_compte'] = $this->id_compte;
		$data['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id')+0;

		$data['dest_user'] = 0;
		$data['dest_admin'] = 0;
		if (isset($_POST['dest_user']) && $_POST['dest_user'] == 'oui') $data['dest_user'] = 1;
		if (isset($_POST['dest_admin']) && $_POST['dest_admin'] == 'oui') $data['dest_admin'] = 1;
		// printr($data);
		$DB->insert("messages", $data);
		Atomik::redirect('outils/messages');
	}


	public function editer_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------
		// Atomik::set('styles/specific', 'assets/css/appli/specif-administration-editer_message.css');
		$this->ariane = get_fil_ariane(array(
			'outils' => 'Outils',
			'outils/messages' => 'Gestion des messages'
		));
		$id_message = 0;
		if (isset($_GET['id_message'])) $id_message = $_GET['id_message'];

		global $DB;
		$sql = "SELECT *, ";
		$sql .= "DATE_FORMAT(date_publication, '%d/%m/%Y') as date_publication, ";
		$sql .= "DATE_FORMAT(date_archivage, '%d/%m/%Y') as date_archivage ";
		$sql .= "FROM messages \n";
		$sql .= "WHERE id_compte=$this->id_compte \n";
		$sql .= "AND id_message = $id_message ";
		// printr($sql);
		$message = $DB->fetchRow($sql);
		if ($message)
		{
			$fileini = Atomik::get('atomik/apppath').'/app/views/outils/editer_message_form.ini';
			$configform = new Zend_Config_Ini($fileini, 'formulaires');
			$configform = $configform->editer_message;
			$form = new FBForms($configform);
			$form->setvaleurs($message);
			$this->form = $form->getform();
		}
		else
		{
			Atomik::flash(array('1' => "ERREUR : le message demandé n'a pas été trouvé"), 'error');
			Atomik::redirect('outils/messages');
		}

	}
	public function action_editer_message()
	{
		// ---- vérification des droits d'accès à cette fonctionnalité ----
		if (!verif_droits_acces('admin')) {Atomik::redirect('fw3/non_autorise'); return;}
		// ----------------------------------------------------------------

		$id_message = 0;
		if (isset($_POST['id_message'])) $id_message = $_POST['id_message'];
		if ($id_message <= 0) return;
		global $DB;
		$data = array();
		// printr($_POST);
		if (isset($_POST)) $data = $_POST;
		if (isset($data['cmd'])) unset($data['cmd']);
		if (isset($data['submit'])) unset($data['submit']);
		// if (isset($data['id_message'])) unset($data['id_message']);
		if (isset($data['date_publication'])) $data['date_publication'] = convert_date_eu_to_mysql($data['date_publication']);
		if (isset($data['date_archivage'])) $data['date_archivage'] = convert_date_eu_to_mysql($data['date_archivage']);
		$data['id_auteur'] = Atomik::get('session/'.IDAPPLI.'/user/id')+0;

		$data['dest_user'] = 0;
		$data['dest_admin'] = 0;
		if (isset($_POST['dest_user']) && $_POST['dest_user'] == 'oui') $data['dest_user'] = 1;
		if (isset($_POST['dest_admin']) && $_POST['dest_admin'] == 'oui') $data['dest_admin'] = 1;

		// printr($data, "DATA");
		$DB->update("messages", $data, "id_message=$id_message AND id_compte=$this->id_compte");
		Atomik::redirect('outils/messages');
	}



}
