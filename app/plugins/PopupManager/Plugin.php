<?php

class PopupManagerPlugin
{
	/**
	 * Plugin's configuration
	 *
	 * @var array
	 */
	public static $config = array(
			
	);
	
	/**
	 * Plugin initialization
	 *
	 * @param array $config Plugin's configuration
	 */
	public static function start($config = array())
	{
		// merging default and user specified configuration 
		self::$config = array_merge(self::$config, $config);
	}
	
	public static function linkPopup($phtmlName, $nom, $largeur, $hauteur, $options = false)
	{
	
		echo "javascript:fenetreCent(\"$phtmlName\",\"$nom\",$largeur,$hauteur,\"resizable=no,scrollbars=no";  
		if($options = false)
			echo "";
		else
		{
			$i = 0;
			if(is_array($options))
			{
				foreach($options as $opt => $value)
				{
					if($i == 0)
						echo $opt."=".$value;
					else
						echo ",".$opt."=".$value;
				}
			}
		
		}
		echo "\")";
	
	}
	
}