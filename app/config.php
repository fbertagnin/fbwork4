<?php
//=====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2011, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Configuration pour le FW Atomik.
// Cette configuration a été personnalisée pour l'application en cours.
//
//=====================================================================

Atomik::set(array (
'app' =>
  array (
	'layout' => '_layout',
	'aide_folder' => 'doc/appli',
	'ajax_layout' => '_layoutajax',
	'export_layout' => '_layoutexport',
	'default_action' => 'index',
	'views' =>
		array (
			'file_extension' => '.phtml',
		),
//	'config' => 'prod'
	'config' => 'develop'
  ),
'atomik' =>
  array (
	'url_rewriting' => true,
    'start_session' => true,
    'class_autoload' => true,
    'trigger' => 'action',
    'catch_errors' => true,
    'display_errors' => true,
    'debug' => true,
	'log' =>
	  array (
		'register_default' => true,
	  ),
	'files' =>
	  array (
		'log' => 'log/application.log',
	  ),
  ),
'styles' =>
  array (
    // 0 => 'assets/css/ui-overcast/jquery-ui-1.8.16.custom.css',
    1 => 'assets/css/appli/layout.css',
    2 => 'assets/css/appli/main.css',
    3 => 'assets/css/appli/tools.css',
    4 => 'assets/css/appli/box.css',
    // 5 => 'assets/css/appli/menu_principal.css',
    5 => 'assets/css/appli/menu_principal_deroulant.css',
    6 => 'assets/css/appli/forms.css',
    7 => 'assets/libs/thickbox/thickbox.css	',
    8 => 'assets/css/appli/boutons-icones.css'
  ),
'styles_smartphone' =>
  array (
    0 => 'assets/css/appli/smartphone/layout.css',
    1 => 'assets/css/appli/smartphone/main.css',
    2 => 'assets/css/appli/smartphone/form.css',
  ),
'ie_styles' =>
  array (
    0 => 'assets/css/appli/ie.css',
  ),
'debug_styles' =>
  array (
    0 => 'assets/css/appli/develop.css',
  ),
'plugins' =>
  array (
    'Ajax',
    'Minifier',
    'PopupManager',
    'Controller',
  ),
'scripts' =>
  array (
	0 => 'assets/js/jquery-1.7.min.js',
	3 => 'assets/js/base.js',
	4 => 'assets/js/menu_deroulant.js',
	5 => 'assets/libs/thickbox/thickbox-fr.js',
	6 => 'assets/js/jquery.tools/jquery.tools.tabs.tooltip.min.js',
  ),
'tracker' =>
  array (
	"type" => array(
		'amélioration',
		'correction',
		'documentation',
		'données',
		'note'
	),
	"priorité" => array(
		'bloquant',
		'urgent',
		'normal',
		'faible'
	),
	"statut" => array(
		'proposé',
		'validé',
		'terminé',
		'réfusé',
		'reporté'
	)
  )
));
