<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2011, est à la version 3 : FBWork3.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork3 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Menu de navigation.
//
// ====================================================================
	// voir http://woork.blogspot.fr/2009/09/how-to-implement-perfect-multi-level.html
	global $CONF;
	echo get_menu_principal($CONF->menu_principal);

function verif_droits($droitsuser, $droitsitem, $nom='')
{
	if (is_object($droitsitem))
	{
		foreach($droitsitem as $droititem)
		{
			if (isset($droititem->name) && $droititem->name == 'all') return true;
			foreach($droitsuser as $droit => $type)
			{
				//	printr("droit=$droit $type item=".$droititem->name);
				if (isset($droititem->name))
				{
					if ($droit == $droititem->name)
					{
						if ($droit == 'webmaster' && $type == '1') return true;
						if ($droit == 'admin' && $type == '1') return true;
						if ($type == $droititem->type) return true;
					}
				}
				else
				{
					if ($droit == $droititem)
					{
						if ($droit == 'webmaster' && $type == '1') return true;
						if ($droit == 'admin' && $type == '1') return true;
						if ($type == $droititem->type) return true;
					}
				}
			}
		}
	}
}

function get_menu_principal ($config)
{
	$DROITS = Atomik::get('session/'.IDAPPLI.'/user/droits');
	$menuselected = 0;
	if (Atomik::get('page/menuselected') != '') $menuselected = Atomik::get('page/menuselected'); 
	// printr($config);
	
	$vtp = new VTemplate;
	$dirpage = realpath(dirname(__FILE__));
	$handle = $vtp->Open($dirpage.DIRECTORY_SEPARATOR."menu_principal_deroulant.tpl.phtml");
	$vtp->NewSession($handle,"MAIN");
	foreach ($config->item as $k => $item)
	{
		$id = 0;
		// printr($item, "ITEM");
		if (!Atomik::get('application/use_authentification') || verif_droits($DROITS, $item->droit,$item->label))
		{
			if (isset($item->id)) $id = $item->id;
			// utiliser l'ID pour gérer les authorisations d'affichage de l'item
			$vtp->AddSession($handle,"LINK_N1");
			if (isset($item->id)) $vtp->SetVar($handle,"LINK_N1.MENUITEM", $item->id); // identificateur
			$classitem = "";
			if (isset($item->class)) $classitem = $item->class;
			if ($menuselected == $id) $classitem .= " selected"; // affichage sélectionné
			$vtp->SetVar($handle,"LINK_N1.CLASSITEM", $classitem); 
			if (isset($item->link)) 
			{
				$vtp->SetVar($handle,"LINK_N1.LINK", Atomik::get('atomik/appurl')."/".$item->link);
			}
			else
			{
				$vtp->SetVar($handle,"LINK_N1.LINK", "#nolink");
			}
			if (isset($item->linktitle)) $vtp->SetVar($handle,"LINK_N1.LINKTITLE", $item->linktitle);
			if (isset($item->label)) $vtp->SetVar($handle,"LINK_N1.LABEL", $item->label);
			if (isset($item->class)) $vtp->SetVar($handle,"LINK_N1.CLASS", $item->class);

			if (isset($item->item))
			{
				$vtp->AddSession($handle,"MENU_DEROULANT");
				foreach ($item->item as $k1 => $item1)
				{
					$id = 0;
					if (verif_droits($DROITS, $item1->droit))
					{
						if (isset($item1->id)) $id = $item1->id;
						// utiliser l'ID pour gérer les authorisations d'affichage de l'item
						$vtp->AddSession($handle,"LINK_N2");
						if (isset($item1->link)) 
						{
							$vtp->SetVar($handle,"LINK_N2.LINK", Atomik::get('atomik/appurl')."/".$item1->link);
						}
						else
						{
							$vtp->SetVar($handle,"LINK_N2.LINK", "#nolink");
						}
						if (isset($item1->linktitle)) $vtp->SetVar($handle,"LINK_N2.LINKTITLE", $item1->linktitle);
						if (isset($item1->label)) $vtp->SetVar($handle,"LINK_N2.LABEL", $item1->label);
						if (isset($item1->class)) $vtp->SetVar($handle,"LINK_N2.CLASS", $item1->class);
						$vtp->CloseSession($handle,"LINK_N2");
					} // END if (verif_droits($DROITS, $item1->droit))
				} 
				$vtp->CloseSession($handle,"MENU_DEROULANT");
			}
			else
			{
				$vtp->AddSession($handle,"NO_MENU_DEROULANT");
				$vtp->CloseSession($handle,"NO_MENU_DEROULANT");
			} // END if (isset($item->item))
			$vtp->CloseSession($handle,"LINK_N1");
		} // END if (verif_droits($DROITS, $item->droit))
	} // END foreach ($config->item as $k => $item)
	$vtp->CloseSession($handle,"MAIN");
	return $vtp->Display($handle,0);
}
