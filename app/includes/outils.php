<?php
// ====================================================================
// FBWork3 PHP Developement Framework
// Version            : 4
// Première version   : 30 mars 2006
// Dernière version   : septembre 2012
// Auteur             : Fabio Bertagnin - FBServices fabio.bertagnin@fbservices.fr
// Licence            : GNU-LGPL v3 (http://www.gnu.org/copyleft/lesser.html)
// --------------------------------------------------------------------
// Copyright (C) 2010-2012  Fabio Bertagnin - FBServices.fr 
//
// Ce fichier fait partie d'un environnement de développement PHP
// appelé FBWork, créé par Fabio Bertagnin comme Framework de base
// pour les développements et les outils de FBServices.
// FBWork, depuis septembre 2012, est à la version 4 : FBWork4.
// FBWork intégre plusieurs bibliothèques et fonctions Open Source,
// notamment le framework Atomik, le générateur de PDF TCPdf, le 
// générateur de templates VTemplate.
// FBWork4 est soumis à la licence GNU-LGPL v3, et peut donc être
// réutilisé et distribué selon les critères propres de cette licence.
// --------------------------------------------------------------------
//
// Description : Différents outils adaptés à l'application.
//
// ====================================================================


// Rend le code pour l'affichage d'un bouton d'aide, selon les standards de FBWork4.
// Le boutton est affiché seulement si le fichier d'aide existe réellement.
// Pour l'administrateur, si le fichier n'existe pas, un bouton spécial d'édition est affiché.
function get_code_button_aide($page, $title="Aide")
{
	$ret = "";
	$appurl = Atomik::get('atomik/appurl');
	$t = encodestringhtml($title);
	
	$file_present = false;
	// Vérification de l'existence de la page
	$path = Atomik::get('atomik/apppath').'/'.Atomik::get('app/aide_folder');
	$aa = explode("-",$page);
	// URL de type ?page=folder-floder-fichier
	// le tableau contient les répertoires, le dernier élément le nom du fichier sans extension
	for ($i=0; $i < count($aa)-1; $i++)
	{
		$path .= '/'.$aa[$i];
		if (!is_dir($path)) mkdir($path);
	}
	$file = $path . '/' . $aa[count($aa)-1].".doc.html";
	if (is_file($file)) 
	{
		$file_present = true;
	}
	
	if (!$file_present && verif_droits_acces('webmaster')) 
	{
		$ret .= "<a class=\"bouton-icone-aide-nofile-32 thickbox avec-tooltip\" title=\"$t\" ";
		$ret .= "href=\"$appurl/ajax_aide/?page=$page\" ></a>";
	}
	if ($file_present)
	{
		$ret .= "<a class=\"bouton-icone-aide-32 thickbox avec-tooltip\" title=\"$t\" ";
		$ret .= "href=\"$appurl/ajax_aide/?page=$page\" ></a>";
	}
	return $ret;
}

// Affiche le texte d'un fichier mis dans le répertoire de la view en cours
function _F($file)
{
	$infos = "";
	$c = Atomik::get('page/controleur');
	$infos_file = Atomik::get('atomik/apppath')."/app/views/$c/$file";
	if (is_file($infos_file))
	{
		$infos = file_get_contents($infos_file);
		$infos = txt2html($infos);
	}
	return $infos;
}

// Affiche un message configuré dans le fichier app_config.xml
function _M($messageid,$controleur="", $view="")
{
	global $MESSAGES;
	
	if ($controleur == "") $controleur = Atomik::get('page/controleur');
	if ($view == "") $view = Atomik::get('page/view');


	// printr($MESSAGES, "MESSAGES");
	$ret = "";
	if (isset($MESSAGES[$controleur][$view][$messageid])) $ret = $MESSAGES[$controleur][$view][$messageid];
	return $ret;
}


define('TRACE(X)', 'TRACE_(X, __FILE__, __LINE__');
function TRACE($message, $file="", $ligne="")
{
	$tracefile = Atomik::get('atomik/apppath')."/log/trace.txt";
	enregistrer_trace($tracefile, $message, $file, $ligne);
}

function erreur_inattendue($msg = '')
{
	$c=Atomik::get('page/controleur');
	$v=Atomik::get('page/view');
	$err[] = "Une erreur inattendue s'est produite";
	if ($msg != '') $err[] = encodehtml($msg);
	$err[] = "CONTR=$c PAGE=$v";
	$err = encodehtml($err);
	Atomik::flash($err, 'error');
	// printr($err);
	Atomik::redirect('/');
}

function verification_doublons_personnes()
{
	global $DB;
	$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
	$doublons = 0;
	$sql = "SELECT nom, prenom, count(id_personne) as cnt ";
	$sql .= "FROM personnes ";
	$sql .= "WHERE id_ensemble=$id_ensemble ";
	$sql .= "GROUP BY nom,prenom ";
	$result = $DB->fetchAll($sql);
	if ($result)
	{
		foreach($result as $r)
		{
			if ($r['cnt'] > 1)
			{
				$doublons++;
			}
		}
	}
	return $doublons;
}




function verifier_config_appurl()
{
	// Vérification de l'URL du site programmée dans la base
	// avec l'URL de connexion réelle. Renvoie un message
	// en cas de discordance

	// test sur les premiers caracères
	$nb = 30;
	$appurl = substr(Atomik::get('atomik/appurl'), 0, $nb);
	$url = substr($_SERVER['HTTP_REFERER'], 0, $nb);

	// $appurl = Atomik::get('atomik/appurl');
	// $url = $_SERVER['HTTP_REFERER'];
	
	// printr("URL programmée : $appurl... URL attendue : $url... ");
	if ($appurl != $url)
	{
		$msg = '';
		// $msg .= "<div style=\"background:red;color:#fff\">";
		$msg .= "ATTENTION : l'URL de base de l'application ne semble pas la même que celle du site. ";
		$msg .= "Ceci peu empécher à l'application de fonctionner correctement. ";
		$msg .= "Une intervention du webmstre est sans doute nécessaire. ";
		$msg .= "URL programmée : $appurl... URL attendue : $url... ";
		// $msg .= "</div>";
		return $msg;
	}
	
}


/* Analyse et conversion d'un numéro de téléphone
 * quand le numéro commence par 00, le 00 est remplacé par + 
 * quand le numéro commence par 0, le zéro est remplacé par +33
 * quand le numéro ne commence pas par 0 un +33 est ajouté 
 * les espaces et les points sont supprimés
 */
function convertir_numero_tel_pour_enregistrement($tel)
{
	$ret = trim($tel);
	$ret = str_replace(' ', '', $ret);
	$ret = str_replace('.', '', $ret);
	if ($ret == '') return $ret;
	if (strlen($ret) < 2) return $ret;
	if (substr($ret, 0, 2) == '00')
	{
		$ret = substr_replace($ret, '+', 0, 2);
	}
	elseif (substr($ret, 0, 1) == '0')
	{
		$ret = substr_replace($ret, '+33', 0, 1);
	}
	elseif (substr($ret, 0, 1) == '+')
	{
		$ret = $ret;
	}
	else
	{
		// $ret = "+33$ret";
	}
	// printr("--$tel--$ret--", "TEL");
	return $ret;
}

/* Convertier le numéro de téléphone (au format standardisé) vers
 * un format facilement lisible */
function convertir_numero_tel_pour_affichage($tel)
{
	$ret = trim($tel);
	if (strlen($ret) < 4) return $ret;
	if (substr($ret, 0, 3) == '+33')
	{
		$str = str_replace('+33', '0',$ret);
		$a = str_split($str, 2);
		$ret = '';
		foreach($a as $e)
		{
			if ($ret != '') $ret .= ' ';
			$ret .= $e;
		}
		// printr($tel, "STR $ret");
	}
	elseif (substr($ret, 0, 1) == '+')
	{
		$ret = $ret;
	}
	else
	{
		$ret = $ret;
	}
	return $ret;
}


function get_config_generale_section($groupe)
{
	global $DB;
	$id_ensemble=Atomik::get('session/'.IDAPPLI.'/user/id_ensemble')+0;
	$ret = array();
	$sql = "SELECT * ";
	$sql .= "FROM parametres_generaux \n";
	$sql .= "WHERE groupe = '$groupe' \n";
	$sql .= "AND ( id_ensemble=$id_ensemble ";
	$sql .= "OR    id_ensemble=0 ) ";
	$sql .= "ORDER BY id_parametre ";
	$result = $DB->fetchAll($sql);
	// printr($result, $sql);
	if ($result)
	{
		foreach($result as $par) $ret[] = encodehtml($par['nom']);
	}	
	return $ret;
}

function get_enum_list($table, $champ)
{
	global $DB;	
	return get_enum_mysql($DB, $table, $champ);
}

function get_enum_config($enumeration)
{
	$ret = array();
	$tmp = array();
	global $CONF;
	$EN = $CONF->enumerations;
	if (!isset($EN->$enumeration)) return $ret;
	$cnt = 0;
	// printr($EN->$enumeration, "ENUMERATION $enumeration");
	foreach ($EN->$enumeration->param as $param)
	{
		$cnt++;
		$ordre = ''.$cnt;
		if (isset($param->ordre) && $param->ordre >= 0) $ordre = $param->ordre.$ordre;
		$tmp[$ordre] = $param;
	}
	// l'énumération est dans le tableau, l'index donne le bon ordre
	ksort($tmp);
	$i = 0;
	foreach($tmp as $param) 
	{	
		$ret[$i]['value'] = $param->data;
		$ret[$i]['text'] = encodehtml($param->label);
		$ret[$i]['defaut'] = '';
		if (isset($param->default)) $ret[$i]['defaut'] = $param->default;
		$i++;
	}
	// printr($ret);
	return $ret;
}

function verif_droits_acces($access, $ecr=false)
{
	$droits = Atomik::get('session/'.IDAPPLI.'/user/droits');
	if (isset($droits['webmaster']) && $droits['webmaster'] == 1) return true;
	if (!isset($droits[$access])) 
	{
		// Atomik::flash('Fonctionnalité non autorisé', 'error');
		// Atomik::redirect('fw3/non_autorise');
		return false;
	}
	if ($access == 'admin' && $droits['admin'] == 1) return true;
	if ($ecr == false) return true;
	if ($droits[$access] == 'ecr') return true;

	// printr($droits); exit;
	return false;
}

function get_fil_ariane($liste)
{
	$ret = '';
	$base = Atomik::get('atomik/appurl');
	if ($base == "") $base = "/";
	if (!is_array($liste) || count($liste) < 0) return $ret;
	$ret .= '<div id="fil-ariane" class="full ariane">';
	// $ret .= "BASE=$base<br />";
	$ret .= "<a href=\"$base\">Accueil</a> >";
	foreach($liste as $k => $v)
	{
		$ret .= " <a href=\"$base/$k\">$v</a> >";
	}
	$ret .= "</div>\n";
	return $ret;
}
function crypt_passwd($login, $passwd)
{
	$crypt = crypt($login, $passwd);
	return $crypt;
}

function get_infos_compte($id_ensemble)
{
	global $DB;	
	$id = $id_ensemble+0;
	$sql = "SELECT * FROM comptes WHERE id_compte = ? ";
	$stmt = $DB->query($sql, $id);
	$ensemble = $stmt->fetch();
	if (!$ensemble)
	{
		return;
	}
	return $ensemble;
}

