/* Création de la base pour un projet d'application avec le framework FBWork4
   La configuration minimale de la base est créée, avec les tables nécessaires
   pour l'autentification et le fonctionnement de base de l'application.
   Insérer les paramètres de connexion à la base dans le fichier de configuration app_config.xml.
 */

/* users : utilisateurs connectés au système */
DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id_user int(11) NOT NULL auto_increment,
  nom varchar(255) NOT NULL default '',
  prenom varchar(255) NOT NULL default '',
  mail varchar(255) NOT NULL default '',
  login varchar(64) NOT NULL default '',
  passwd varchar(64) NOT NULL default '',
  signature TEXT NOT NULL default '',
  notes TEXT NOT NULL default '',

  id_compte int(11) NOT NULL default '1',
  id_auteur int(11) NOT NULL default '0',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  date_validite_debut DATE DEFAULT '0000-00-00',
  date_validite_fin DATE DEFAULT '0000-00-00',
  date_connexion TIMESTAMP DEFAULT '0000-00-00 00:00:00',

  actif ENUM('oui', 'no') NOT NULL default 'oui',
  dr_admin ENUM('oui', 'no') NOT NULL default 'no',
  dr_webmaster ENUM('oui', 'no') NOT NULL default 'no',

  /* affectation de droits génériques, c'est l'application
     qui gère la définition et les accès liés à chaque droit */
  dr_01 ENUM('ecr', 'lec', 'no') NOT NULL default 'lec',
  dr_02 ENUM('ecr', 'lec', 'no') NOT NULL default 'lec',
  dr_03 ENUM('ecr', 'lec', 'no') NOT NULL default 'lec',
  dr_04 ENUM('ecr', 'lec', 'no') NOT NULL default 'lec',
  dr_05 ENUM('ecr', 'lec', 'no') NOT NULL default 'lec',
  dr_06 ENUM('ecr', 'lec', 'no') NOT NULL default 'lec',
  dr_07 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_08 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_09 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_10 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_11 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_12 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_13 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_14 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',
  dr_15 ENUM('ecr', 'lec', 'no') NOT NULL default 'no',

  PRIMARY KEY  (id_user),
  UNIQUE INDEX (login)
) DEFAULT CHARSET=utf8mb4;

/* ALTER TABLE users */
/* ADD date_validite_debut DATE DEFAULT '0000-00-00', */
/* ADD date_validite_fin DATE DEFAULT '0000-00-00', */
/* DROP COLUMN date_validite; */

CREATE TRIGGER users_insert BEFORE INSERT ON users
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER users_update BEFORE UPDATE ON users
FOR EACH ROW SET NEW.date_modif = NOW();

/* Un premier utilisateur avec les droits de webmaster pour
   pouvoir paramétrer l'application. login=admin password=admin */
INSERT INTO users set
	nom='Admin',
	prenom='Administrateur',
	login='admin',
	mail='',
	passwd='d033e22ae348aeb5660fc2140aec35850c4da997',
	id_auteur=1,
	id_compte=1,
	dr_admin=1,
	dr_webmaster=1,
	dr_01 = 'ecr',
	dr_02 = 'ecr',
	dr_03 = 'ecr',
	dr_04 = 'ecr',
	dr_05 = 'ecr',
	dr_06 = 'ecr',
	dr_07 = 'ecr',
	dr_08 = 'ecr',
	dr_09 = 'ecr',
	dr_11 = 'ecr',
	dr_12 = 'ecr',
	dr_13 = 'ecr',
	dr_14 = 'ecr',
	dr_15 = 'ecr',
	actif=1;

/* password : totototo */
INSERT INTO users set
	nom='Toto',
	prenom='TOTO',
	login='toto',
	mail='fabio.bertagnin@fbservices.fr',
	passwd='630547c1fada14c61e876be55ac877e13f5c03d7',
	id_auteur=1,
	id_compte=1,
	dr_admin=1,
	dr_webmaster=0,
	actif=1;


/* *********************************************************************** */
DROP TABLE IF EXISTS tracker;
CREATE TABLE tracker (
  id_tracker int(11) NOT NULL auto_increment,
  titre varchar(255) NOT NULL default '',
  type varchar(32) NOT NULL default '',
  statut varchar(32) NOT NULL default '',
  priorite varchar(32) NOT NULL default '',
  description TEXT NOT NULL default '',
  rapport TEXT NOT NULL default '',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  id_modif int(11) NOT NULL default '0',
  id_auteur int(11) NOT NULL default '0',
  PRIMARY KEY (id_tracker)
)DEFAULT CHARSET=utf8mb4;

CREATE TRIGGER tracker_insert BEFORE INSERT ON tracker
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER tracker_update BEFORE UPDATE ON tracker
FOR EACH ROW SET NEW.date_modif = NOW();

INSERT INTO tracker set
  titre = 'Test tracker',
  description = 'Un ticket ouvert pour tester les fonctionnalités du tracker',
  type = 'correction',
  priorite = 'normal',
  id_auteur = 1,
  id_modif = 1,
  statut = 'proposé';



/* *********************************************************************** */
DROP TABLE IF EXISTS parametres_generaux;
CREATE TABLE parametres_generaux (
  id_parametre int(11) NOT NULL auto_increment,
  id_compte int(11) NOT NULL default '0',
  groupe varchar(255) NOT NULL default '',
  nom varchar(255) NOT NULL default '',
  protege int(4) NOT NULL default '0',
  notes TEXT NOT NULL default '',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  id_auteur int(11) NOT NULL default '0',
  PRIMARY KEY (id_parametre)
)DEFAULT CHARSET=utf8mb4;

CREATE TRIGGER parametres_generaux_insert BEFORE INSERT ON parametres_generaux
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER parametres_generaux_update BEFORE UPDATE ON parametres_generaux
FOR EACH ROW SET NEW.date_modif = NOW();



insert parametres_generaux set
  id_compte = 0,
  protege = 1,
  groupe = 'params-1',
  nom = 'Val 1';

insert parametres_generaux set
  id_compte = 0,
  groupe = 'params-1',
  nom = 'Val 2';


DROP TABLE IF EXISTS parametres_user;
CREATE TABLE parametres_user (
  id_parametre int(11) NOT NULL auto_increment,
  id_compte int(11) NOT NULL default '0',
  id_user int(11) NOT NULL default '0',
  nom varchar(255) NOT NULL default '',
  value varchar(255) NOT NULL default '',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  id_auteur int(11) NOT NULL default '0',
  PRIMARY KEY (id_parametre)
)DEFAULT CHARSET=utf8mb4;

CREATE TRIGGER parametres_user_insert BEFORE INSERT ON parametres_user
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER parametres_user_update BEFORE UPDATE ON parametres_user
FOR EACH ROW SET NEW.date_modif = NOW();



DROP TABLE IF EXISTS messages;
CREATE TABLE messages (
  id_message int(11) NOT NULL auto_increment,
  id_compte int(11) NOT NULL default '0',
  titre varchar(255) NOT NULL default '',
  description varchar(255) NOT NULL default '',
  texte TEXT NOT NULL default '',
  dest_user int(4) NOT NULL default '0',
  dest_admin int(4) NOT NULL default '0',
  dest_superadmin int(4) NOT NULL default '0',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  date_publication DATE DEFAULT '0000-00-00',
  date_archivage DATE DEFAULT '0000-00-00',
  id_auteur int(11) NOT NULL default '0',
  PRIMARY KEY (id_message)
)DEFAULT CHARSET=utf8mb4;

CREATE TRIGGER messages_insert BEFORE INSERT ON messages
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER messages_update BEFORE UPDATE ON messages
FOR EACH ROW SET NEW.date_modif = NOW();

insert into messages set
	id_compte=1,
	dest_user = 1,
	dest_admin = 1,
	titre="Message d'exemple n°1",
	description="Le gestionnaire du compte peut publier et administrer les messages du compte",
	texte="Texte de test. &é{}èçàµ*. Proin faucibus tincidunt sapien sed volutpat. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Sed vitae libero cursus diam venenatis hendrerit. Nam elementum nunc ut mauris tincidunt vitae dapibus erat faucibus. Aenean porttitor rhoncus neque, sit amet rutrum ligula laoreet nec. Pellentesque nec ante id enim pretium ultricies at blandit dolor. Proin euismod mollis dui, vitae faucibus urna semper nec.";

insert into messages set
	id_compte=1,
	dest_user = 1,
	dest_admin = 1,
	dest_superadmin = 1,
	titre="Message d'exemple n°2",
	description="L'administrateur du compte peut publier et administrer les messages pour tous les comptes de l'application",
	texte="Texte de test. &é{}èçàµ*. Donec elit enim, rutrum id dapibus at, iaculis eget ligula. Donec non nulla nec orci hendrerit vulputate. Vestibulum non quam at ante egestas blandit quis et turpis. Nunc ut tortor lacus, vitae auctor eros. Pellentesque dictum dapibus magna; a dictum tortor adipiscing in.";

DROP TABLE IF EXISTS fw4config;
CREATE TABLE fw4config (
  id_param int(11) NOT NULL auto_increment,
  nom varchar(255) NOT NULL default '',
  valeur varchar(255) NOT NULL default '',
  readonly int(4) NOT NULL default '0',

  notes TEXT NOT NULL default '',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY  (id_param)
)DEFAULT CHARSET=utf8mb4;

CREATE TRIGGER fw4config_insert BEFORE INSERT ON fw4config
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER fw4config_update BEFORE UPDATE ON fw4config
FOR EACH ROW SET NEW.date_modif = NOW();

insert into fw4config set
  nom = 'database_version',
  valeur = '1.0',
  readonly = 1,
  notes = 'La version de la base permet de vérifier la compatibilité de la base avec la version de l''application';

insert into fw4config set
  nom = 'appurl',
  valeur = '',
  notes = 'Mettre l''URL exacte de la racine du site pour corriger des éventuelles erreurs du calcul automatique. Ceci permet au site de produire des liens de navigation corrects. Laisser cette valeur vide pour que l''URL soit calculée automatiquement.';

insert into fw4config set
  nom = 'money_format',
  valeur = '%!^.2i',
  readonly = 0,
  notes = 'Formatage des valeurs monétaires, voir la documentation PHP de money_format. Défaut : %!^.2i (pas de regroupement, deux décimaux, pas de nom de la dévise.';


/* *********************************************************************** */
/* comptes : comptes où les utilisateurs sont affectés,
   l'administrateur administre un seul compte
 */
DROP TABLE IF EXISTS comptes;
CREATE TABLE comptes (
  id_compte int(11) NOT NULL auto_increment,
  nom varchar(255) NOT NULL default '',
  notes TEXT NOT NULL default '',
  date_dern_acces TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  date_creation TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  date_modif TIMESTAMP DEFAULT '0000-00-00 00:00:00',
  id_auteur int(11) NOT NULL default '0',
  PRIMARY KEY  (id_compte)
)DEFAULT CHARSET=utf8mb4;
CREATE TRIGGER comptes_insert BEFORE INSERT ON comptes
FOR EACH ROW SET NEW.date_modif = NOW();

CREATE TRIGGER comptes_update BEFORE UPDATE ON comptes
FOR EACH ROW SET NEW.date_modif = NOW();


INSERT INTO comptes set
	nom='Compte de Test',
	notes='Ce compte est un enregistrement de test.',
	id_auteur=1;

